---
layout: post
title: "Security related Operating Systems"
lang: en
categories:
  - security
tags:
  - security
  - linux
  - anonymity
  - pentest
  - privacy
date: 2016/06/09
updated: 2017/09/19
thumbnail: /images/security-265130_640.jpg
authorId: noraj
---

The list of security related OS is now part of [Rawsec's CyberSecurity Inventory](https://inventory.raw.pm/operating_systems.html)!
