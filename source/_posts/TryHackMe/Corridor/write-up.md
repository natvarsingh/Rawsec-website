---
layout: post
title: "Corridor - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - web
  - idor
date: 2022/12/30 23:42:00
thumbnail: /images/TryHackMe/corridor.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Corridor
- **Profile:** [tryhackme.com](https://tryhackme.com/room/corridor)
- **Difficulty:** Easy
- **Description**: Can you escape the Corridor?

![Corridor](/images/TryHackMe/corridor.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap haiti ctf-party ffuf
```

{% chart [width] [height] %}
{
  type: 'polarArea',
  responsive: false,
  data: {
    labels: ['Enumeration', 'Real-Life', 'Well-Known Vulnerabilities', 'Custom Exploitation', 'CTF-Like', 'Scripting'],
    datasets: [{
      label: 'Box rating',
      backgroundColor: [
        'rgba(54, 163, 235, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(255, 205, 86, 0.5)',
        'rgba(68, 205, 58, 0.5)',
        'rgba(244, 81, 30, 0.5)'
      ],
      borderColor: [
        'rgba(54, 163, 235, 1)',
        'rgba(255, 99, 132, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(68, 205, 58, 1)',
        'rgba(244, 81, 30, 1)'
      ],
      data: [
        3,
        1,
        1,
        3,
        5,
        3
      ]
    }]
  },
  options: {
    legend: {
      position: 'right',
      labels: {
        fontSize: 25
      }
    },
    scale: {
      ticks: {
        beginAtZero: true,
        stepSize: 1,
        suggestedMax: 5
      }
    }
  }
};
{% endchart %}

## Network enumeration

Let's add a custom domain:

```
$ grep corridor /etc/hosts
10.10.157.202 corridor.thm
```

Port and service scan with nmap:

```
# Nmap 7.93 scan initiated Fri Dec 30 22:31:00 2022 as: nmap -sSVC -T4 -p- -v --open --reason -oA nmap corridor.thm
Nmap scan report for corridor.thm (10.10.157.202)
Host is up, received reset ttl 63 (0.029s latency).
Not shown: 65528 closed tcp ports (reset), 6 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 62 Werkzeug httpd 2.0.3 (Python 3.10.2)
| http-methods:
|_  Supported Methods: OPTIONS GET HEAD
|_http-title: Corridor

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Dec 30 22:31:22 2022 -- 1 IP address (1 host up) scanned in 21.92 seconds
```

## Web exploration

By looking at the homepage (`curl http://corridor.thm/`), we can see there is a unique URL for each of the 13 doors.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Corridor</title>

    <link rel="stylesheet" href="/static/css/main.css">
</head>

<body>


<img src="/static/img/corridor.png" usemap="#image-map">

    <map name="image-map">
        <area target="" alt="c4ca4238a0b923820dcc509a6f75849b" title="c4ca4238a0b923820dcc509a6f75849b" href="c4ca4238a0b923820dcc509a6f75849b" coords="257,893,258,332,325,351,325,860" shape="poly">
        <area target="" alt="c81e728d9d4c2f636f067f89cc14862c" title="c81e728d9d4c2f636f067f89cc14862c" href="c81e728d9d4c2f636f067f89cc14862c" coords="469,766,503,747,501,405,474,394" shape="poly">
        <area target="" alt="eccbc87e4b5ce2fe28308fd9f2a7baf3" title="eccbc87e4b5ce2fe28308fd9f2a7baf3" href="eccbc87e4b5ce2fe28308fd9f2a7baf3" coords="585,698,598,691,593,429,584,421" shape="poly">
        <area target="" alt="a87ff679a2f3e71d9181a67b7542122c" title="a87ff679a2f3e71d9181a67b7542122c" href="a87ff679a2f3e71d9181a67b7542122c" coords="650,658,644,437,658,652,655,437" shape="poly">
        <area target="" alt="e4da3b7fbbce2345d7772b0674a318d5" title="e4da3b7fbbce2345d7772b0674a318d5" href="e4da3b7fbbce2345d7772b0674a318d5" coords="692,637,690,455,695,628,695,467" shape="poly">
        <area target="" alt="1679091c5a880faf6fb5e6087eb1b2dc" title="1679091c5a880faf6fb5e6087eb1b2dc" href="1679091c5a880faf6fb5e6087eb1b2dc" coords="719,620,719,458,728,471,728,609" shape="poly">
        <area target="" alt="8f14e45fceea167a5a36dedd4bea2543" title="8f14e45fceea167a5a36dedd4bea2543" href="8f14e45fceea167a5a36dedd4bea2543" coords="857,612,933,610,936,456,852,455" shape="poly">
        <area target="" alt="c9f0f895fb98ab9159f51fd0297e236d" title="c9f0f895fb98ab9159f51fd0297e236d" href="c9f0f895fb98ab9159f51fd0297e236d" coords="1475,857,1473,354,1537,335,1541,901" shape="poly">
        <area target="" alt="45c48cce2e2d7fbdea1afc51c7c6ad26" title="45c48cce2e2d7fbdea1afc51c7c6ad26" href="45c48cce2e2d7fbdea1afc51c7c6ad26" coords="1324,766,1300,752,1303,401,1325,397" shape="poly">
        <area target="" alt="d3d9446802a44259755d38e6d163e820" title="d3d9446802a44259755d38e6d163e820" hre f="d3d9446802a44259755d38e6d163e820" coords="1202,695,1217,704,1222,423,1203,423" shape="poly">
        <area target="" alt="6512bd43d9caa6e02c990b0a82652dca" title="6512bd43d9caa6e02c990b0a82652dca" href="6512bd43d9caa6e02c990b0a82652dca" coords="1154,668,1146,661,1144,442,1157,442" shape="poly">
        <area target="" alt="c20ad4d76fe97759aa27a0c99bff6710" title="c20ad4d76fe97759aa27a0c99bff6710" href="c20ad4d76fe97759aa27a0c99bff6710" coords="1105,628,1116,633,1113,447,1102,447" shape="poly">
        <area target="" alt="c51ce410c124a10e0db5e4b97fc2af39" title="c51ce410c124a10e0db5e4b97fc2af39" href="c51ce410c124a10e0db5e4b97fc2af39" coords="1073,609,1081,620,1082,459,1073,463" shape="poly">
    </map>


</body>
</html>
```

But the URL endpoints looks furiously like MD5 hashes, isn't it?

```
$ haiti 'c4ca4238a0b923820dcc509a6f75849b'
MD5 [HC: 0] [JtR: raw-md5]
LM [HC: 3000] [JtR: lm]
NTLM [HC: 1000] [JtR: nt]
MD2 [JtR: md2]
MD4 [HC: 900] [JtR: raw-md4]
Haval-128 (4 rounds) [JtR: haval-128-4]
Lotus Notes/Domino 5 [HC: 8600] [JtR: lotus5]
Skype [HC: 23]
IPB 2.x (Invision Power Board) [HC: 2810]
Keyed MD5: RIPv2, OSPF, BGP, SNMPv2 [JtR: net-md5]
RIPEMD-128 [JtR: ripemd-128]
Snefru-128 [JtR: snefru-128]
Domain Cached Credentials (DCC), MS Cache [HC: 1100] [JtR: mscash]
Domain Cached Credentials 2 (DCC2), MS Cache 2 [HC: 2100] [JtR: mscash2]
DNSSEC (NSEC3) [HC: 8300]
RAdmin v2.x [HC: 9900] [JtR: radmin]
Umbraco HMAC-SHA1 [HC: 24800]
Bitcoin WIF private key (P2PKH), compressed [HC: 28501]
Bitcoin WIF private key (P2PKH), uncompressed [HC: 28502]
```

Those are the md5 hash of the number of the doors.

```
$ for i in {1..13}; do ctf-party "$i" md5; done
c4ca4238a0b923820dcc509a6f75849b
c81e728d9d4c2f636f067f89cc14862c
eccbc87e4b5ce2fe28308fd9f2a7baf3
a87ff679a2f3e71d9181a67b7542122c
e4da3b7fbbce2345d7772b0674a318d5
1679091c5a880faf6fb5e6087eb1b2dc
8f14e45fceea167a5a36dedd4bea2543
c9f0f895fb98ab9159f51fd0297e236d
45c48cce2e2d7fbdea1afc51c7c6ad26
d3d9446802a44259755d38e6d163e820
6512bd43d9caa6e02c990b0a82652dca
c20ad4d76fe97759aa27a0c99bff6710
c51ce410c124a10e0db5e4b97fc2af39
```

## Web exploitation: IDOR

We can try to generate number from 14 to 100 and hash them and use that list as input for ffuf.

```
for i in {14..100}; do ctf-party "$i" md5; done | ffuf -u http://corridor.thm/FUZZ -w -
```

But we have not results.

In fact the good value was zero and then you get the flag.

```
$ curl -s http://corridor.thm/$(ctf-party 0 md5) | grep flag
    flag{edited}
```
