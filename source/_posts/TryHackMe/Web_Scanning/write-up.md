---
layout: post
title: "Web Scanning - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - network
  - thm
  - zaproxy
  - nikto
date: 2020/09/10 22:19:00
updated: 2020/12/17 19:27:00
thumbnail: /images/TryHackMe/Web_Scanning.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Web Scanning
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rpwebscanning)
- **Difficulty:** Easy
- **Description**: Part of the Red Primer series, intro to web scanning.

![Web Scanning](/images/TryHackMe/Web_Scanning.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S nikto	zaproxy
```

**Disclaimer**: answers are very easy, obvious, given in the question or the
description or just requires to search the man/help page so I won't give details here.

## [Task 2] ...I'm supposed to scan with that? 

### #1

> First and foremost, what switch do we use to set the target host?

Answer: {% spoiler -h %}

### #2

> Websites don't always properly redirect to their secure transport port and can sometimes have different issues depending on the manner in which they are scanned. How do we disable secure transport?

Answer: {% spoiler -nossl %}

### #3

>  How about the opposite, how do we force secure transport? 

Answer: {% spoiler -ssl %}

### #4

> What if we want to set a specific port to scan?

Answer: {% spoiler -p %}

### #5

> As the web is constantly evolving, so is Nikto. A database of vulnerabilities represents a core component to this web scanner, how do we verify that this database is working and free from error?

Answer: {% spoiler -dbcheck %}

### #6

> If instructed to, Nitko will attempt to guess and test both files within directories as well as usernames. Which switch and numerical value do we use to set Nikto to enumerate usernames in Apache? Keep in mind, this option is deprecated in favor of plugins, however, it's still a great option to be aware of for situational usage.

Answer: {% spoiler -mutate 3 %}

### #7

> Suppose we know the username and password for a web forum, how do we set Nikto to do a credentialed check? Suppose the username is admin and the password is PrettyAwesomePassword1234

Answer: {% spoiler +id admin:PrettyAwesomePassword1234 %}

### #8

> Let's scan our target machine, what web server do we discover and what version is it?

Answer: {% spoiler Apache/2.4.7 %}

```
$ nikto -h 10.10.151.203
+ Server: Apache/2.4.7 (Ubuntu)
```

### #9

> This box is vulnerable to very poor directory control due to it's web server version, what directory is indexed that really shouldn't be?

Answer: {% spoiler config %}

```
$ nikto -h 10.10.151.203
+ OSVDB-3268: /config/: Directory indexing found.
```

### #10

> Nikto scans can take a while to fully complete, which switch do we set in order to limit the scan to end at a certain time?

Answer: {% spoiler -until %}

### #11

> But wait, there's more! How do we list all of the plugins are available?

Answer: {% spoiler -list-plugins %}

### #12

> On the flip-side of the database, plugins represent another core component to Nikto. Which switch do we use to instruct Nikto to use plugin checks to find out of date software on the target host? Keep in mind that when testing this command we need to specify the host we intend to run this against. For submitting your answer, use only the base command with the out of date option.

Answer: {% spoiler -Plugins outdated %}

### #13

> Finally, what if we'd like to use our plugins to run a series of standard tests against the target host?

Answer: {% spoiler -Plugins tests %}

## [Task 3] Zip ZAP!

### #2

> Launch ZAP, what option to we set in order to specify what we are attacking?

Answer: {% spoiler URL to attack %}

### #4

> ZAP will discover a file that typically contains pages which well-behaved web indexing engines will read in order to know which sections of a site to avoid. What is the name of this file? (Lucky for us, our scanner isn't what we would call 'well-behaved'!)

Answer: {% spoiler robots.txt %}

### #5

> One entry is included in the disallow section of this file, what is it?

Answer: {% spoiler / %}

### #6

> ZAP will find a directory that contains images for our application, what is the path for that directory? (This is what will follows the name/ip of the website)

Answer: {% spoiler /dvwa/images/ %}

### #7

> This website doesn't force a secure connection by default and ZAP isn't pleased with it. Which related cookie is ZAP upset about?

Answer: {% spoiler HttpOnly %}

### #8

> Featured in various rooms on TryHackMe, Cross-Site Scripting is a vicious attack that is becoming ever more common on the open web. What Alert does ZAP produce to let us know that this site is vulnerable to XSS? Note, there are often a couple warnings produced for this, look for one more so directly related to the web client.

Answer: {% spoiler Web Browser XSS Protection Not Enabled %}

### #9

> The ZAP proxy spider represents the component responsible for 'crawling' the site. What site is found to be out of scope?

Answer: {% spoiler http://www.dvwa.co.uk/ %}

### #10

> ZAP will use primarily two methods in order to scan a website, which of these two HTTP methods requests content?

Answer: {% spoiler GET %}

### #

> Which option attempts to submit content to the website?

Answer: {% spoiler POST %}
