---
layout: post
title: "Nmap - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - thm
  - nmap
date: 2020/12/18
thumbnail: /images/TryHackMe/Nmap.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Nmap
- **Profile:** [tryhackme.com](https://tryhackme.com/room/furthernmap)
- **Difficulty:** Easy
- **Description**: An in depth look at scanning with Nmap, a powerful network scanning tool.

![Nmap](/images/TryHackMe/Nmap.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap
```

## Introduction

> What networking constructs are used to direct traffic to the right application on a server?

Answer: {% spoiler ports %}

Read the task material.

> How many of these are available on any network-enabled computer?

Answer: {% spoiler 65535 %}

Read the task material.

> [Research] How many of these are considered "well-known"? (These are the "standard" numbers mentioned in the task)

Answer: {% spoiler 1024 %}

Read this [wikipedia article](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers#Well-known_ports).

## Nmap Switches

**Disclaimer**: I won't detail the task as it's just search through the help/man
page.

> What is the first switch listed in the help menu for a 'Syn Scan' (more on this later!)?

Answer: {% spoiler `-sS` %}

```
$ nmap -h | grep -i syn
```

> Which switch would you use for a "UDP scan"?

Answer: {% spoiler `-sU` %}


```
$ nmap -h | grep -i UDP
```

> If you wanted to detect which operating system the target is running on, which switch would you use?

Answer: {% spoiler `-O` %}

```
$ nmap -h | grep OS
```

> Nmap provides a switch to detect the version of the services running on the target. What is this switch?

Answer: {% spoiler `-sV` %}

```
$ nmap -h | grep -i version
```

> The default output provided by nmap often does not provide enough information for a pentester. How would you increase the verbosity?

Answer: {% spoiler `-v` %}

```
$ nmap -h | grep -i verbosity
```

> Verbosity level one is good, but verbosity level two is better! How would you set the verbosity level to two?

Answer: {% spoiler `-vv` %}

```
$ nmap -h | grep -i verbosity
```

> What switch would you use to save the nmap results in three major formats?

Answer: {% spoiler `-oA` %}

```
$ nmap -h | grep -i output
```

> What switch would you use to save the nmap results in a "normal" format?

Answer: {% spoiler `-oN` %}

```
$ nmap -h | grep -i output
```

> A very useful output format: how would you save results in a "grepable" format?

Answer: {% spoiler `-oG` %}

```
$ nmap -h | grep -i output
```

> Sometimes the results we're getting just aren't enough. If we don't care about how loud we are, we can enable "aggressive" mode. This is a shorthand switch > that activates service detection, operating system detection, a traceroute and common script scanning.
>
> How would you activate this setting?

Answer: {% spoiler `-A` %}

```
$ nmap -h | grep -i traceroute
```

> How would you set the timing template to level 5?

Answer: {% spoiler `-T5` %}

```
$ nmap -h | grep -i timing
```

> How would you tell nmap to only scan port 80?

Answer: {% spoiler `-p 80` %}

```
$ nmap -h | grep -i port | grep -i only
```

> How would you tell nmap to scan ports 1000-1500?

Answer: {% spoiler `-p 1000-1500` %}

> How would you tell nmap to scan all ports?

Answer: {% spoiler `-p-` %}

A shorter version of `-p 1-65535`.

> How would you activate a script from the nmap scripting library (lots more on this later!)?

Answer: {% spoiler `--script` %}

```
$ nmap -h | grep -i script
```

> How would you activate all of the scripts in the "vuln" category?

Answer: {% spoiler `--script=vuln` %}

```
$ nmap -h | grep -i script
```

## [Scan Types] TCP Connect Scans

> Which RFC defines the appropriate behaviour for the TCP protocol?

Answer: {% spoiler `RFC 793` %}

Read the task material.

> If a port is closed, which flag should the server send back to indicate this?

Answer: {% spoiler `RST` %}

Read the task material.

## [Scan Types] SYN Scans

> There are two other names for a SYN scan, what are they?

Answer: {% spoiler `Half-open, stealth` %}

Read the task material.

> Can Nmap use a SYN scan without Sudo permissions (Y/N)?

Answer: {% spoiler `n` %}

Raw socket requires some capabilities:

- `CAP_NET_RAW`
- `CAP_NET_ADMIN`
- `CAP_NET_BIND_SERVICE`

## [Scan Types] UDP Scans

> If a UDP port doesn't respond to an Nmap scan, what will it be marked as?

Answer: {% spoiler `open|filtered` %}

Read the task material.

> When a UDP port is closed, by convention the target should send back a "port unreachable" message. Which protocol would it use to do so?

Answer: {% spoiler `ICMP` %}

Read the task material.

## [Scan Types] NULL, FIN and Xmas

> Which of the three shown scan types uses the URG flag?

Answer: {% spoiler `xmas` %}

Read the task material.

> Why are NULL, FIN and Xmas scans generally used?

Answer: {% spoiler `firewall evasion` %}

Read the task material.

> Which common OS may respond to a NULL, FIN or Xmas scan with a RST for every port?

Answer: {% spoiler `Microsoft Windows` %}

Read the task material.

## [Scan Types] ICMP Network Scanning

> How would you perform a ping sweep on the 172.16.x.x network (Netmask: 255.255.0.0) using Nmap? (CIDR notation)

Answer: {% spoiler `nmap -sn 172.16.0.0/16` %}

## [NSE Scripts] Overview

> What language are NSE scripts written in?

Answer: {% spoiler `lua` %}

Read the task material.

> Which category of scripts would be a very bad idea to run in a production environment?

Answer: {% spoiler `intrusive` %}

Read the task material.

## [NSE Scripts] Working with the NSE

> What optional argument can the `ftp-anon.nse` script take?

Answer: {% spoiler `maxlist` %}

Read the [ftp-anon doc](https://nmap.org/nsedoc/scripts/ftp-anon.html).

## [NSE Scripts] Searching for Scripts

> Search for "smb" scripts in the /usr/share/nmap/scripts/ directory using either of the demonstrated methods.
> What is the filename of the script which determines the underlying OS of the SMB server?

Answer: {% spoiler `smb-os-discovery.nse` %}

Search for `smb` and `os`.

```
$ grep smb /usr/share/nmap/scripts/script.db | grep -e '-os'
```

> Read through this script. What does it depend on?

Answer: {% spoiler `smb-brute` %}

Let's look for dependencies.

```
$ grep dependencies /usr/share/nmap/scripts/smb-os-discovery.nse
```

## Firewall Evasion

> Which simple (and frequently relied upon) protocol is often blocked, requiring the use of the `-Pn` switch?

Answer: {% spoiler `icmp` %}

Read the task material.

> [Research] Which Nmap switch allows you to append an arbitrary length of random data to the end of packets?

Answer: {% spoiler `--data-length` %}

```
$ nmap -h | grep -i 'random data'
```

## Practical

> Does the target (10.10.130.86) respond to ICMP (ping) requests (Y/N)?

Answer: {% spoiler `n` %}

```
$ sudo nmap -PE 10.10.36.6
```

> Perform an Xmas scan on the first 999 ports of the target -- how many ports are shown to be open or filtered?

Answer: {% spoiler `999` %}

```
$ sudo nmap -sX -p 1-999 10.10.36.6 -Pn
```

> There is a reason given for this -- what is it?

Answer: {% spoiler `no responses` %}

```
$ sudo nmap -sX -p 1-999 10.10.36.6 -Pn -vv
```

> Perform a TCP SYN scan on the first 5000 ports of the target -- how many ports are shown to be open?

Answer: {% spoiler `5` %}

```
$ sudo nmap -sS -p 1-5000 --open -Pn 10.10.36.6     
[sudo] password for noraj: 
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 21:00 CET
Nmap scan report for 10.10.36.6
Host is up (0.064s latency).
Not shown: 4995 filtered ports
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT     STATE SERVICE
21/tcp   open  ftp
53/tcp   open  domain
80/tcp   open  http
135/tcp  open  msrpc
3389/tcp open  ms-wbt-server

Nmap done: 1 IP address (1 host up) scanned in 27.47 seconds
```

> Deploy the `ftp-anon` script against the box. Can Nmap login successfully to the FTP server on port 21? (Y/N)

Answer: {% spoiler `xxx` %}

```
$ nmap --script ftp-anon -p 21 10.10.36.6
Starting Nmap 7.91 ( https://nmap.org ) at 2020-12-18 21:04 CET
Nmap scan report for 10.10.36.6
Host is up (0.066s latency).

PORT   STATE SERVICE
21/tcp open  ftp
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: TIMEOUT

Nmap done: 1 IP address (1 host up) scanned in 32.30 seconds
```
