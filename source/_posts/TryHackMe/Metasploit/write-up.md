---
layout: post
title: "Metasploit - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - recon
  - network
  - exploit
  - thm
  - windows
  - metasploit
date: 2020/08/30
thumbnail: /images/TryHackMe/Metasploit.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Metasploit
- **Profile:** [tryhackme.com](https://tryhackme.com/room/rpmetasploit)
- **Difficulty:** Easy
- **OS:** Windows
- **Description**: Part of the Red Primer series, learn to use Metasploit!

![Metasploit](/images/TryHackMe/Metasploit.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pacman -S nmap metasploit msfdb
```

## [Task 2] Initializing...

### #3

> We can start the Metasploit console on the command line without showing the banner or any startup information as well. What switch do we add to msfconsole to start it without showing this information? This will include the '-'

Answer: {% spoiler -q %}

```
$ msfconsole -h
Usage: msfconsole [options]

Common options:
    -E, --environment ENVIRONMENT    Set Rails environment, defaults to RAIL_ENV environment variable or 'production'

Database options:
    -M, --migration-path DIRECTORY   Specify a directory containing additional DB migrations
    -n, --no-database                Disable database support
    -y, --yaml PATH                  Specify a YAML file containing database settings

Framework options:
    -c FILE                          Load the specified configuration file
    -v, -V, --version                Show version

Module options:
        --defer-module-loads         Defer module loading unless explicitly asked
    -m, --module-path DIRECTORY      Load an additional module path

Console options:
    -a, --ask                        Ask before exiting Metasploit or accept 'exit -y'
    -H, --history-file FILE          Save command history to the specified file
    -L, --real-readline              Use the system Readline library instead of RbReadline
    -o, --output FILE                Output to the specified file
    -p, --plugin PLUGIN              Load a plugin on startup
    -q, --quiet                      Do not print the banner on startup
    -r, --resource FILE              Execute the specified resource file (- for stdin)
    -x, --execute-command COMMAND    Execute the specified console commands (use ; for multiples)
    -h, --help                       Show this message
```

### #6

> Cool! We've connected to the database, which type of database does Metasploit 5 use?

Answer: {% spoiler postgresql %}

You'll get the answer by using `db_status` in `msefconsole`.

## [Task 3] Rock 'em to the Core [Commands]

### #2

> The help menu has a very short one-character alias, what is it?

Answer: {% spoiler ? %}

You will get the answer with the `help` command.

### #3

> Finding various modules we have at our disposal within Metasploit is one of the most common commands we will leverage in the framework. What is the base command we use for searching?

Answer: {% spoiler search %}

You will get the answer with the `help` command.

### #4

> Once we've found the module we want to leverage, what command we use to select it as the active module?

Answer: {% spoiler use %}

You will get the answer with the `help` command.

### #5

> How about if we want to view information about either a specific module or just the active one we have selected?

Answer: {% spoiler info %}

You will get the answer with the `help` command.

### #6

> Metasploit has a built-in netcat-like function where we can make a quick connection with a host simply to verify that we can 'talk' to it. What command is this?

Answer: {% spoiler connect %}

You will get the answer with the `help` command.

### #7

> Entirely one of the commands purely utilized for fun, what command displays the motd/ascii art we see when we start msfconsole (without -q flag)?

Answer: {% spoiler banner %}

You will get the answer with the `help` command.

### #8

> We'll revisit these next two commands shortly, however, they're two of the most used commands within Metasploit. First, what command do we use to change the value of a variable?

Answer: {% spoiler set %}

You will get the answer with the `help` command.

### #9

> Metasploit supports the use of global variables, something which is incredibly useful when you're specifically focusing on a single box. What command changes the value of a variable globally?

Answer: {% spoiler setg %}

You will get the answer with the `help` command.

### #10

> Now that we've learned how to change the value of variables, how do we view them? There are technically several answers to this question, however, I'm looking for a specific three-letter command which is used to view the value of single variables.

Answer: {% spoiler get %}

You will get the answer with the `help` command.

### #11

> How about changing the value of a variable to null/no value?

Answer: {% spoiler unset %}

You will get the answer with the `help` command.

### #12

> When performing a penetration test it's quite common to record your screen either for further review or for providing evidence of any actions taken. This is often coupled with the collection of console output to a file as it can be incredibly useful to grep for different pieces of information output to the screen. What command can we use to set our console output to save to a file?

Answer: {% spoiler spool %}

You will get the answer with the `help` command.

### #13

> Leaving a Metasploit console running isn't always convenient and it can be helpful to have all of our previously set values load when starting up Metasploit. What command can we use to store the settings/active datastores from Metasploit to a settings file? This will save within your msf4 (or msf5) directory and can be undone easily by simply removing the created settings file.

Answer: {% spoiler save %}

You will get the answer with the `help` command.

## [Task 4] Modules for Every Occasion!

### #1

> Easily the most common module utilized, which module holds all of the exploit code we will use?

Answer: {% spoiler exploit %}

See the diagram.

### #2

> Used hand in hand with exploits, which module contains the various bits of shellcode we send to have executed following exploitation?

Answer: {% spoiler payload %}

See the diagram.

### #3

> Which module is most commonly used in scanning and verification machines are exploitable? This is not the same as the actual exploitation of course.

Answer: {% spoiler auxiliary %}

See the diagram.

### #4

> One of the most common activities after exploitation is looting and pivoting. Which module provides these capabilities?

Answer: {% spoiler post %}

See the diagram.

### #5

> Commonly utilized in payload obfuscation, which module allows us to modify the 'appearance' of our exploit such that we may avoid signature detection?

Answer: {% spoiler encoder %}

See the diagram.

### #6

> Last but not least, which module is used with buffer overflow and ROP attacks?

Answer: {% spoiler nop %}

See the diagram.

### #7

> Not every module is loaded in by default, what command can we use to load different modules?

Answer: {% spoiler load %}

You will get the answer with the `help` command.

## [Task 5] Move that shell!

### #2

> What service does nmap identify running on port 135?

Answer: {% spoiler msrpc %}

Launch `db_nmap -sV 10.10.57.82`, the answer is in the `SERVICE` column.

### #6

> Now that we've scanned our victim system, let's try connecting to it with a Metasploit payload. First, we'll have to search for the target payload. In Metasploit 5 (the most recent version at the time of writing) you can simply type 'use' followed by a unique string found within only the target exploit. For example, try this out now with the following command 'use icecast'. What is the full path for our exploit that now appears on the msfconsole prompt? *This will include the exploit section at the start

Answer: {% spoiler exploit/windows/http/icecast_header %}

Launch `use icecast` and see where you landed.

### #7

> While that use command with the unique string can be incredibly useful that's not quite the exploit we want here. Let's now run the command 'search multi/handler'. What is the name of the column on the far left side of the console that shows up next to 'Name'? Go ahead and run the command 'use NUMBER_NEXT_TO exploit/multi/handler` wherein the number will be what appears in that far left column (typically this will be 4 or 5). In this way, we can use our search results without typing out the full name/path of the module we want to use.

Answer: {% spoiler # %}

For me it was module 6 in column `#`.

```
exploit/windows/http/icecast_header  2004-09-28       great  No     Icecast Header Overwrite


[*] Using exploit/windows/http/icecast_header
msf5 exploit(windows/http/icecast_header) > search multi/handler

Matching Modules
================

   #  Name                                                 Disclosure Date  Rank       Check  Description
   -  ----                                                 ---------------  ----       -----  -----------
   0  auxiliary/scanner/http/apache_mod_cgi_bash_env       2014-09-24       normal     Yes    Apache mod_cgi Bash Environment Variable Injection (Shellshock) Scanner
   1  exploit/android/local/janus                          2017-07-31       manual     Yes    Android Janus APK Signature bypass
   2  exploit/linux/local/apt_package_manager_persistence  1999-03-09       excellent  No     APT Package Manager Persistence
   3  exploit/linux/local/bash_profile_persistence         1989-06-08       normal     No     Bash Profile Persistence
   4  exploit/linux/local/desktop_privilege_escalation     2014-08-07       excellent  Yes    Desktop Linux Password Stealer and Privilege Escalation
   5  exploit/linux/local/yum_package_manager_persistence  2003-12-17       excellent  No     Yum Package Manager Persistence
   6  exploit/multi/handler                                                 manual     No     Generic Payload Handler
   7  exploit/windows/browser/persits_xupload_traversal    2009-09-29       excellent  No     Persits XUpload ActiveX MakeHttpRequest Directory Traversal
   8  exploit/windows/mssql/mssql_linkcrawler              2000-01-01       great      No     Microsoft SQL Server Database Link Crawling Command Execution
```

## [Task 6] We're in, now what?

### #1

> First things first, our initial shell/process typically isn't very stable. Let's go ahead and attempt to move to a different process. First, let's list the processes using the command 'ps'. What's the name of the spool service?

Answer: {% spoiler spoolsv.exe %}

Run `ps` and check the `Name` column.

### #2

> Let's go ahead and move into the spool process or at least attempt to! What command do we use to transfer ourselves into the process? This won't work at the current time as we don't have sufficient privileges but we can still try!

Answer: {% spoiler migrate %}

You will get the answer with the `help` command.

### #3

> Well that migration didn't work, let's find out some more information about the system so we can try to elevate. What command can we run to find out more information regarding the current user running the process we are in?

Answer: {% spoiler getuid %}

You will get the answer with the `help` command.

```
meterpreter > getuid
Server username: Dark-PC\Dark
```

### #4

> How about finding more information out about the system itself?

Answer: {% spoiler sysinfo %}

You will get the answer with the `help` command.

```
meterpreter > sysinfo
Computer        : DARK-PC
OS              : Windows 7 (6.1 Build 7601, Service Pack 1).
Architecture    : x64
System Language : en_US
Domain          : WORKGROUP
Logged On Users : 2
```

### #5

> This might take a little bit of googling, what do we run to load mimikatz (more specifically the new version of mimikatz) so we can use it?

Answer: {% spoiler load kiwi %}

- `load mimikatz`: load mimikatz v1
- `load kiwi`: load mimikatz v2

```
meterpreter > load kiwi
Loading extension kiwi...
  .#####.   mimikatz 2.2.0 20191125 (x86/windows)
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > http://blog.gentilkiwi.com/mimikatz
 '## v ##'        Vincent LE TOUX            ( vincent.letoux@gmail.com )
  '#####'         > http://pingcastle.com / http://mysmartlogon.com  ***/

[!] Loaded x86 Kiwi on an x64 architecture.

Success.
```

### #6

> Let's go ahead and figure out the privileges of our current user, what command do we run?

Answer: {% spoiler getprivs %}

```
meterpreter > getprivs

Enabled Process Privileges
==========================

Name
----
SeChangeNotifyPrivilege
SeIncreaseWorkingSetPrivilege
SeShutdownPrivilege
SeTimeZonePrivilege
SeUndockPrivilege
```

### #7

> What command do we run to transfer files to our victim computer?

Answer: {% spoiler upload %}

You will get the answer with the `help` command.

### #8

> How about if we want to run a Metasploit module?

Answer: {% spoiler run %}

You will get the answer with the `help` command.

### #9

> A simple question but still quite necessary, what command do we run to figure out the networking information and interfaces on our victim?

Answer: {% spoiler ipconfig %}

You will get the answer with the `help` command.

```
meterpreter > ipconfig

Interface  1
============
Name         : Software Loopback Interface 1
Hardware MAC : 00:00:00:00:00:00
MTU          : 4294967295
IPv4 Address : 127.0.0.1
IPv4 Netmask : 255.0.0.0
IPv6 Address : ::1
IPv6 Netmask : ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff


Interface 12
============
Name         : Microsoft ISATAP Adapter
Hardware MAC : 00:00:00:00:00:00
MTU          : 1280
IPv6 Address : fe80::5efe:a0a:3952
IPv6 Netmask : ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff


Interface 13
============
Name         : AWS PV Network Device #0
Hardware MAC : 02:82:35:b8:e9:13
MTU          : 9001
IPv4 Address : 10.10.57.82
IPv4 Netmask : 255.255.0.0
IPv6 Address : fe80::20a9:b114:1340:6af1
IPv6 Netmask : ffff:ffff:ffff:ffff::
```

### #13

> One quick extra question, what command can we run in our meterpreter session to spawn a normal system shell?

Answer: {% spoiler shell %}

You will get the answer with the `help` command.

## [Task 7] Makin' Cisco Proud

### #1

> Let's go ahead and run the command `run autoroute -h`, this will pull up the help menu for autoroute. What command do we run to add a route to the following subnet: 172.18.1.0/24? Use the -n flag in your answer.

Answer: {% spoiler run autoroute -s 172.18.1.0 -n 255.255.255.0 %}

Run `run autoroute -h` to known the answer.

### #2

> Additionally, we can start a socks4a proxy server out of this session. Background our current meterpreter session and run the command `search server/socks4a`. What is the full path to the socks4a auxiliary module?

Answer: {% spoiler auxiliary/server/socks4a %}

Run `bg` to background our current session, then run `search server/socks4a`.

### #3

> Once we've started a socks server we can modify our /etc/proxychains.conf file to include our new server. What command do we prefix our commands (outside of Metasploit) to run them through our socks4a server with proxychains?

Answer: {% spoiler proxychains %}

The answer is the name of the tool itself.
