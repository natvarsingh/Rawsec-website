---
layout: post
title: "Crack The Hash Level 2 - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - cracking
  - thm
  - hash
date: 2021/05/30 11:53:00
thumbnail: /images/TryHackMe/crackthehashlevel2.jpeg
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Crack The Hash Level 2
- **Profile:** [tryhackme.com](https://tryhackme.com/room/crackthehashlevel2)
- **Difficulty:** Medium
- **Description**: Advanced cracking hashes challenges and wordlist generation

![Crack The Hash Level 2](/images/TryHackMe/crackthehashlevel2.jpeg)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S john wordlistctl mentalist lyricpass cewl haiti ttpassgen
```

John the ripper rules:

```conf
[List.Rules:norajCommon01]
c$[0-9]$[0-9]$[$%&*-_+=#@~!]

[List.Rules:norajCommon02]
c$1$2$3$4$[$%&*-_+=#@~!]

[List.Rules:norajCommon03]
r

[List.Rules:norajCommon04]
d
dd
ddd
dddd
```

## Task 6 - It's time to crack hashes

### Hash 1 - English male name - f76a69033cc94d1fb3b2122becf46828 (MD5)

Border mutation - commonly used combinations of digits and special symbols can be added at the end or at the beginning, or both

```
$ john hash.txt --format=raw-md5 --wordlist=/usr/share/wordlists/misc/top_1000_usa_malenames_english.txt --rules=norajCommon02
```

### Hash 2 - English female name - 01bf0eb071a40ba731ccb3cb47319375 (MD5)

Border mutation - commonly used combinations of digits and special symbols can be added at the end or at the beginning, or both

```
$ john hash.txt --format=raw-md5 --wordlist=/usr/share/wordlists/misc/top_1000_usa_femalenames_english.txt --rules=norajCommon01
```

### Hash 3 - Town name of Mexico - 8ebe45d61ef53c7d748b80c592436e23 (MD5)

Freak mutation - letters are replaced with similarly looking special symbols

```
$ cat /usr/share/wordlists/misc/city-state-country.txt | dos2unix | rg 'Mexico$' | cut -f 1 -d ',' | uniq > mexico.txt
```

Then with `mentalist` or by any other mean, substitute some letters into leet, eg `a` -> `@` or `o` -> `0`.

![](https://i.imgur.com/LIVch0q.png)

Then use the generated dict with John.

```
$ john hash.txt --format=raw-md5 --wordlist=mentalist.txt
```

### Hash 4 - Own name - 1f474c6dadb3cb2370f6cb88d4576ede0db9ff43 (SHA1)

Case mutation - the program checks all variations of uppercase/lowercase letters for any character

Build a short dictionary with the guy name:

```
david
guettapan
davidguettapan
```

Then use john with a case permutation rule:

```
$ john hash.txt --format=raw-sha1 --wordlist=david.txt --rules=NT --fork=3
```

### Hash 5 - Lyrics - 140d61a5ecbe4dc82b61d68c8fc05e42 (MD5)

Order mutation - character order is reversed

Adele

Generate wordlist of her songs.

```
$ lyricpass -a adele
$ cat raw-lyrics-2021-01-13-23.59.48 > adele.txt
```

Then use the lyrics list and a reverse mutation rule:

```
$ john hash.txt --format=raw-md5 --wordlist=adele.txt --rules=norajCommon03 --fork=3
```

### Hash 6 - Phone number - a6319095c2cff7ffb0f3d5e930e902f3 (MD5)

No mutations

https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country

Sint Maarten: `+1` and `721` prefix for mobile phone number

Modify `prefix.txt` and `python pnwgen.py` (https://github.com/toxydose/pnwgen) or do it with your own script.

```
$ python pnwgen.py +1721 '' 7
```

Then use the list with john:

```
$ john hash.txt --format=raw-md5 --wordlist=/tmp/pnwgen/wordlist.txt
```

### Hash 7 - Rockyou - 7f921bcacce131426fc5111e664feb80f509796299a764dfbfb834cd97ce3217ca8015f699bdb517ed913aac3e207cf5b566859470ce7dea33ec5cea30603fb0 (SHA3-512)

No mutations

SHA3

```
$ john hash.txt --format=raw-sha3 --wordlist=/usr/share/wordlists/passwords/rockyou.txt --fork=3
```

### Hash 8 - Web scrapping - 9eb3430dc236ec9d3d5cfc6c3390523fc498037bb3de7d7e58254f7e253307a8d102b374713f425eaf1e71f33d3de9094b6a205b78510c9c3b754107a3b09c53 (blake2)

Repetition (4)

```
$ git clone https://github.com/digininja/CeWL.git
$ cd CeWL
$ bundle install --path vendor
$ bundle exec cewl.rb -d 0 -w $(pwd)/rtfm.txt https://rtfm.re/en/sponsors/index.html
$ john --format=Raw-Blake2 hash.txt --wordlist=/tmp/CeWL/rtfm.txt --rules=norajCommon04
```

### Hash 9 - Rockyou - $6$norajPass$rQe8.6rPhZcivym.tpPacR2u360yUZBKTuXbXIJ/zKrzhC4v4eayiNfW/HlcaFVkmVh7Hps5Rupc3ZINTwhmL1 (SHA512-crypt)

No mutations

```
$ john --format=sha512crypt hash.txt --wordlist=/usr/share/wordlists/passwords/rockyou.txt --show
```
