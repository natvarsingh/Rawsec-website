---
layout: post
title: "Web Fundamentals - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - web
date: 2020/11/14 23:50:00
updated: 2020/12/17 19:11:00
thumbnail: /images/TryHackMe/web_fundamentals.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Web Fundamentals
- **Profile:** [tryhackme.com](https://tryhackme.com/room/webfundamentals)
- **Difficulty:** Easy
- **Description**: Learn how the web works!

![Web Fundamentals](/images/TryHackMe/web_fundamentals.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
$ sudo pacman -S curl
```

**Disclaimer**: all the answer are well explained on the room description so I
won't detail them here.

## How do we load websites?

> What request verb is used to retrieve page content?

Answer: {% spoiler GET %}

> What port do web servers normally listen on?

Answer: {% spoiler 80 %}

> What's responsible for making websites look fancy?

Answer: {% spoiler css %}

## More HTTP - Verbs and request formats

> What verb would be used for a login?

Answer: {% spoiler POST %}

> What verb would be used to see your bank balance once you're logged in?

Answer: {% spoiler GET %}

> Does the body of a GET request matter? Yea/Nay

Answer: {% spoiler nay %}

> What's the status code for "I'm a teapot"?

Answer: {% spoiler 418 %}

> What status code will you get if you need to authenticate to access some content, and you're unauthenticated?

Answer: {% spoiler 401 %}

## Mini CTF

> What's the GET flag?

Answer: {% spoiler thm{162520bec925bd7979e9ae65a725f99f} %}

```
curl -X GET http://10.10.205.207:8081/ctf/get
```

> What's the POST flag?

Answer: {% spoiler thm{3517c902e22def9c6e09b99a9040ba09} %}

```
curl -X POST http://10.10.205.207:8081/ctf/post -d flag_please
```

> What's the "Get a cookie" flag?

Answer: {% spoiler thm{91b1ac2606f36b935f465558213d7ebd} %}

```
curl -X GET http://10.10.205.207:8081/ctf/getcookie --head
```

> What's the "Set a cookie" flag?

Answer: {% spoiler thm{c10b5cb7546f359d19c747db2d0f47b3} %}

```
curl -X GET http://10.10.205.207:8081/ctf/sendcookie -b 'flagpls=flagpls'
```
