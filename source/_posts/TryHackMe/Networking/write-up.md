---
layout: post
title: "Networking - Write-up - TryHackMe"
lang: en
categories:
  - writeups
tags:
  - writeups
  - thm
  - network
date: 2020/09/17 00:31:00
updated: 2020/12/17 19:22:00
thumbnail: /images/TryHackMe/Networking.png
authorId: noraj
toc: true
---
# Information

## Room

- **Name:** Networking
- **Profile:** [tryhackme.com](https://tryhackme.com/room/bpnetworking)
- **Difficulty:** Easy
- **Description**: Part of the Blue Primer series, learn the basics of networking

![Networking](/images/TryHackMe/Networking.png)

# Write-up

## Overview

Install tools used in this WU on BlackArch Linux:

```
pikaur -S radare2
```

**Disclaimer**: The answer essentially requires to search for options in the
man page so it doesn't need a detailed write-up.

## [Task 1] Kinda like a street address, just cooler.

**#1**

> How many categories of IPv4 addresses are there?

Answer: {% spoiler 5 %}

**#2**

> Which type is for research? *Looking for a letter rather than a number here

Answer: {% spoiler E %}

**#3**

> How many private address ranges are there?

Answer: {% spoiler 3 %}

**#4**

> Which private range is typically used by businesses?

Answer: {% spoiler A %}

**#5**

> There are two common default private ranges for home routers, what is the first one?

Answer: {% spoiler 192.168.0.0 %}

**#6**

> How about the second common private home range?

Answer: {% spoiler 192.168.1.0 %}

**#7**

> How many addresses make up a typical class C range? Specifically a /24

Answer: {% spoiler 256 %}

**#8**

> Of these addresses two are reserved, what is the first addresses typically reserved as?

Answer: {% spoiler network %}

**#9**

> The very last address in a range is typically reserved as what address type?

Answer: {% spoiler broadcast %}

**#10**

> A third predominant address type is typically reserved for the router, what is the name of this address type?

Answer: {% spoiler gateway %}

**#11**

> Which address is reserved for testing on individual computers?

Answer: {% spoiler 127.0.0.1 %}

**#12**

> A particularly unique address is reserved for unroutable packets, what is that address? This can also refer to all IPv4 addresses on the local machine.

Answer: {% spoiler 0.0.0.0 %}

## [Task 2] Binary to Decimal

**#1**

> 1001 0010

Answer: {% spoiler 146 %}

```
$ rax2 10010010d
146
```

**#2**

> 0111 0111

Answer: {% spoiler 119 %}

```
$ rax2 01110111d
119
```

**#3**

> 1111 1111

Answer: {% spoiler 255 %}

```
$ rax2 11111111d
255
```

**#4**

> 1100 0101

Answer: {% spoiler 197 %}

```
$ rax2 11000101d
197
```

**#5**

> 1111 0110

Answer: {% spoiler 246 %}

```
$ rax2 11110110d
246
```

**#6**

> 0001 0011

Answer: {% spoiler 19 %}

```
$ rax2 00010011d
19
```

**#7**

> 1000 0001

Answer: {% spoiler 129 %}

```
$ rax2 10000001d
129
```

**#8**

> 0011 0001

Answer: {% spoiler 49 %}

```
rax2 00110001d
49
```

**#9**

> 0111 1000

Answer: {% spoiler 120 %}

```
$ rax2 01111000d
120
```

**#10**

> 1111 0000

Answer: {% spoiler 240 %}

```
$ rax2 11110000d
240
```

**#11**

> 0011 1011

Answer: {% spoiler 59 %}

```
$ rax2 00111011d
59
```

**#12**

> 0000 0111

Answer: {% spoiler 7 %}

```
$ rax2 00000111d
7
```

## [Task 3] Decimal to Binary

**#1**

> 238

Answer: {% spoiler 11101110 %}

```
$ rax2 b238
11101110b
```

**#2**

> 34

Answer: {% spoiler 00100010 %}

```
$ rax2 b34
100010b
```

**#3**

> 123

Answer: {% spoiler 01111011 %}

```
$ rax2 b123
1111011b
```

**#4**

> 50

Answer: {% spoiler 00110010 %}

```
$ rax2 b50
110010b
```

**#5**

> 255

Answer: {% spoiler 11111111 %}

```
$ rax2 b255
11111111b
```

**#6**

> 200

Answer: {% spoiler 11001000 %}

```
$ rax2 b200
11001000b
```

**#7**

> 10

Answer: {% spoiler 00001010 %}

```
$ rax2 b10
1010b
```

**#8**

> 138

Answer: {% spoiler 10001010 %}

```
$ rax2 b138
10001010b
```

**#9**

> 1

Answer: {% spoiler 00000001 %}

**#10**

> 13

Answer: {% spoiler 00001101 %}

```
$ rax2 b13 
1101b
```

**#11**

> 250

Answer: {% spoiler 11111010 %}

```
$ rax2 b250
11111010b
```

**#12**

> 114

Answer: {% spoiler 01110010 %}

```
$ rax2 b114
1110010b
```

## [Task 4] Address Class Identification

**#1**

> 10.240.1.1

Answer: {% spoiler a %}

**#2**

> 150.10.15.0

Answer: {% spoiler b %}

**#3**

> 192.14.2.0

Answer: {% spoiler c %}

**#4**

> 148.17.9.1

Answer: {% spoiler b %}

**#5**

> 193.42.1.1

Answer: {% spoiler c %}

**#6**

> 126.8.156.0

Answer: {% spoiler a %}

**#7**

> 220.200.23.1

Answer: {% spoiler c %}

**#8**

> 230.230.45.58

Answer: {% spoiler d %}

**#9**

> 177.100.18.4

Answer: {% spoiler b %}

**#10**

> 119.18.45.0

Answer: {% spoiler a %}

**#11**

> 117.89.56.45

Answer: {% spoiler a %}

**#12**

> 215.45.45.0

Answer: {% spoiler c %}
