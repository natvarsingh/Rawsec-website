---
layout: post
title: "Variables type size and formaters in C"
date: 2014/08/03
lang: en
categories:
- programming
- c
tags:
- programming
- c
thumbnail: /images/programming.jpg
authorId: noraj
---

## Variables

Type                | Size    | Min                         | Max
---                 | ---     | ---                         | ---
signed char         | 8 bits  | -128                        | +127
unsigned char       | 8 bits  | 0                           | +255
short               | 16 bits | -32,768                     | +32,767
unsigned short      | 16 bits | 0                           | +65,535
int                 | 16 bits | -32,768                     | +32,767
unsigned int        | 16 bits | 0                           | +65,535
long                | 32 bits | -2,147,483,648              | +2,147,483,647
unsigned long       | 32 bits | 0                           | +4,294,967,295
long long           | 64 bits | -9,223,372,036,854,775,808  | +9,223,372,036,854,775,807
unsigned long long  | 64 bits | 0                           | +18446744073709551615

## Control codes

+ `\n` : line feed
+ `\r` : carriage return
+ `\t` : tabulation (horizontal)

## Formaters

Formater    | Description
---         | ---
%i %d       | Integers
%x %X       | Nombre entier hexadécimal
%lf         | Nombre réel double précision
%s          | Chaîne de caractères
%f          | Nombre réel simple précision
%c          | Caractère unique
%e %E       | Nombre réel notation exponentielle
