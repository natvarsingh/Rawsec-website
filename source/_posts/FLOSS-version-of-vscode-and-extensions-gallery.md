---
layout: post
title: 'FLOSS version of VSCode and the extensions gallery'
lang: en
categories:
  - programming
tags:
  - programming
  - extension
date: 2021/01/05 19:07:00
updated: 2021/01/11 19:20:00
thumbnail: /images/programming.jpg
authorId: noraj
toc: true
---
## What is VSCode?

[Visual Studio Code][vscode] (a.k.a. VSCode or VS Code) is a cross-platform, free, libre and open-source (FLOSS) text editor developed by Microsoft and written in JavaScript and TypeScript.

There are two main versions of VSCode:

- The FLOSS version of VSCode (named by Microsoft `Code - OSS`) licensed under the [MIT][mit] license
- The Microsoft branded release of VSCode available at [code.visualstudio.com](https://code.visualstudio.com/) and licensed under the [Microsoft software license][mssoftlicense]

The FLOSS version of VSCode (that we will name Code OSS for now on), is the one
available on the [Github repository][vscode],
and is only primarily available as source code, Microsoft does not offer a
compiled or packaged version of it.

The Microsoft branded release of VSCode (that we will name MS VSCode for now on)
is available as a pre-compiled package
for many OS (Linux, macOS, Windows). This release is free (price) but is nor
libre (the microsoft software license is not an OSI approved license) nor open-source
(the change made by microsoft are not publicly available).

Note: I may use VSCode to speak indifferently of MS VSCode or Code OSS.

## Differences between FLOSS and non-FLOSS versions

So what are the main [differences][diff] between those two versions?

The Microsoft branded is compiled from the FLOSS version but enable telemetry by
default, contains some trademarks assets, code relying on proprietary libraries,
code share with proprietary licensed products (eg. Visual Studio) but also
code that provides access to a service that Microsoft run in its data centers
(e.g. access to the Visual Studio Marketplace).

## Whats is the Marketplace?

[Visual Studio Marketplace][marketplace] is a Microsoft hosted and owned extension gallery
for Visual Studio, Visual Studio Code and Azure DevOps.
And so the code to access this Marketplace is not included in the FLOSS version,
meaning that in theory the FLOSS version couldn't be able to install extension
directly from a gallery but manually.

We will see about this issue more in depth later.

## How it is available?

Most Linux user will avoid the Microsoft branded release because it's non-libre,
contain closed source code and enable unwanted behaviors such as the telemetry.

Hopefully most Linux distribution made available a package with a pre-compiled
version of VSCode so you don't have to compile it yourself. There is also a
community-driven binary distribution of VSCode named [VSCodium][vscodium].

Often the package of the FLOSS version of VSCode is nowadays named code but was
often named vscode, code-oss or visual-studio-code at the beginning.

Among the most well known Linux distribution, only ArchLinux is providing
a package in official repositories.

On all others major Linux distro (including Debian, Ubuntu, Fedora, Centos, openSUSE, RHEL, SLE, etc.)
you have the choice between installing:

- the Microsoft branded version manually
- the Microsoft branded version from a third-party repository (user-based or Microsoft-based)
- VSCodium manually
- VSCodium from a third-party repository (user-based or Microsoft-based)

You can find a package for your distro that by checking one of the following
resources:

- https://repology.org/project/vscode/versions
- https://pkgs.org/download/vscode
- https://vscodium.com/

## The extension gallery issue

I introduced the theoretical issue (no extension gallery available for the FLOSS version)
in the Marketplace section but practically there is the [_Open VSX Registry_][openvsx],
which is open-source registry/gallery for VSCode extensions managed by
Eclipse Foundation. So the FLOSS release of VSCode can embedded Open VSX as
an extension gallery as they [may be forbidden][forbid] to embed the Marketplace.

Problem solved right? Nop.

Here are a few reasons why using Open VSX over the Marketplace is a pain and a
bad idea:

- **Only 3.9% of the extensions are available on Open-VSX**
    - At the time of writing (05/01/2021) there are 912 extensions in Open VSX versus 23262 on the marketplace, and I can tell you as a matter of fact that you won't find extensions for a lot of languages or use cases and won't find many extensions included very famous ones.
- **Huge security concerns: Namespace Access**
    - In the past anyone was able to push extensions nearly anywhere, anyone was able to claim a namespace just by opening an issue, anyone was able to publish an extension without claiming a namespace and even publish extensions on other people namespaces, meaning that overall it was impossible to have any trust in any namespace and you just had to blindly install any extension that could have been corrupted. There was only a little shield icon to tell you when an extension was unverified but you were only able to see this icon on the detail page on the Open VSX website not from the extension panel in VSCode so it was nearly useless.
    - As it was an awful security risk, since 17/12/2020 they made a new rule: only members of a namespace have the authority to publish. So anyone can still claim any unclaimed namespace that is very popular on the Marketplace and spread a malicious extension but if a namespace is already claimed it is now impossible to usurp/spoof it.

So in the most case scenario you have installed a FLOSS version of
VSCode that is embedding Open VSX as an extension gallery be you
would prefer to use the Marketplace instead.

Note: While most of the extensions on the Marketplace are FLOSS, some of them
are distributed with a non-OSI license or are using proprietary code from the
Microsoft branded version of VSCode.

## Replace Open VSX extension gallery with the official Microsoft Marketplace

**Warning**: doing this change may violate the [Microsoft Visual Studio Marketplace Terms of Use](https://cdn.vsassets.io/v/M146_20190123.39/_content/Microsoft-Visual-Studio-Marketplace-Terms-of-Use.pdf) section `1) b.`.

To do that you first have to locate the file `product.json`.
For example on ArchLinux it is located at `/usr/lib/code/product.json`.

The beginning of your config file should looks like this:

```json
{
        "quality": "stable",
        "extensionsGallery": {
                "serviceUrl": "https://open-vsx.org/vscode/gallery",
                "itemUrl": "https://open-vsx.org/vscode/item"
        },
```

And to use the official Microsoft Marketplace instead of Open VSX you should replace those lines by:

```json
{
        "quality": "stable",
        "extensionsGallery": {
                "serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
                "cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
                "itemUrl": "https://marketplace.visualstudio.com/items"
        },
```

or create the `extensionsGallery` node if it is not existing yet.

EZ, now restart Code OSS and enjoy the other 96,1% of extensions in a more secure way.

Note: those changes may be overridden on package update.

## Install extension manually

The best idea is maybe to keep the Open VSX gallery and to ask extension's
authors to upload their extension their too but when an author is unresponsive
and you can't find what you want on Open VSX (it happens a lot) then you can
still manually download an extension from https://marketplace.visualstudio.com/
and install it on the CLI, eg:

```
$ code --install-extension /tmp/bierner.emojisense-0.8.0.vsix
```

This way you don't have to change the gallery in the config.

## Publish extensions on Open VSX

Another alternative is to:

1. Tell your favorite tool maintainer to publish its extension on Open VSX
2. Publish the extension on Open VSX yourself is the project is unmaintained

- For project maintainers: https://github.com/eclipse/openvsx/wiki/Publishing-Extensions
- For abandoned projects: https://github.com/open-vsx/publish-extensions

[vscode]:https://github.com/microsoft/vscode
[mit]:https://github.com/microsoft/vscode/blob/master/LICENSE.txt
[mssoftlicense]:https://code.visualstudio.com/License/
[marketplace]:https://marketplace.visualstudio.com/vscode
[vscodium]:https://vscodium.com/
[diff]:https://github.com/microsoft/vscode/wiki/Differences-between-the-repository-and-Visual-Studio-Code
[forbid]:https://github.com/microsoft/vscode/issues/31168
[openvsx]:https://open-vsx.org/
