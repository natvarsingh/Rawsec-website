---
layout: post
title: "Remove EXIF metadata from pictures with exiftool"
date: 2018/12/03
lang: en
categories:
- misc
tags:
- linux
- metadata
- privacy
thumbnail: /images/bash-161382_640.png
authorId: noraj
---
When taking a photo from your phone you don't want to share your position, right?
After rendering a picture from a tool like Gimp or Photoshop you want it to be as light as possible but to keep a good quality, right?
So why not removing EXIF metadata?

With metadata a picture can looks like this:

```
ExifTool Version Number         : 11.11
File Name                       : IMG_20181203_213126.jpg_original
Directory                       : .
File Size                       : 5.5 MB
File Modification Date/Time     : 2018:12:03 22:27:42+01:00
File Access Date/Time           : 2018:12:03 22:48:42+01:00
File Inode Change Date/Time     : 2018:12:03 22:29:07+01:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Little-endian (Intel, II)
Image Description               : 
Make                            : ulefone
Camera Model Name               : Power_3
Orientation                     : Unknown (0)
X Resolution                    : 72
Y Resolution                    : 72
Resolution Unit                 : inches
Software                        : MediaTek Camera Application
Modify Date                     : 2018:12:03 21:31:26
Y Cb Cr Positioning             : Co-sited
Exposure Time                   : 1/14
F Number                        : 2.0
Exposure Program                : Not Defined
ISO                             : 1158
Exif Version                    : 0220
Date/Time Original              : 2018:12:03 21:31:26
Create Date                     : 2018:12:03 21:31:26
Components Configuration        : Y, Cb, Cr, -
Exposure Compensation           : 0
Metering Mode                   : Center-weighted average
Light Source                    : Other
Flash                           : No Flash
Focal Length                    : 3.5 mm
Sub Sec Time                    : 98
Sub Sec Time Original           : 98
Sub Sec Time Digitized          : 98
Flashpix Version                : 0100
Color Space                     : sRGB
Exif Image Width                : 3984
Exif Image Height               : 5312
Interoperability Index          : R98 - DCF basic file (sRGB)
Interoperability Version        : 0100
Exposure Mode                   : Auto
White Balance                   : Auto
Digital Zoom Ratio              : 1
Scene Capture Type              : Standard
Compression                     : JPEG (old-style)
Thumbnail Offset                : 1280
Thumbnail Length                : 36864
Image Width                     : 3984
Image Height                    : 5312
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:2 (2 1)
Aperture                        : 2.0
Image Size                      : 3984x5312
Megapixels                      : 21.2
Shutter Speed                   : 1/14
Create Date                     : 2018:12:03 21:31:26.98
Date/Time Original              : 2018:12:03 21:31:26.98
Modify Date                     : 2018:12:03 21:31:26.98
Thumbnail Image                 : (Binary data 36864 bytes, use -b option to extract)
Focal Length                    : 3.5 mm
Light Value                     : 2.3
```

To remove all EXIF metadata with `exiftool` we have just to use this simple command:

```
exiftool -all= pic.jpg
```

We can also do the same for several pictures, for example in the current folder:

```bash
for i in *.jpg; do echo "Cleaning $i"; exiftool -all= "$i"; done
```

Now here is the same picture with EXIF metadata cleaned:

```
ExifTool Version Number         : 11.11
File Name                       : IMG_20181203_213126.jpg
Directory                       : .
File Size                       : 5.5 MB
File Modification Date/Time     : 2018:12:03 22:29:07+01:00
File Access Date/Time           : 2018:12:03 22:29:43+01:00
File Inode Change Date/Time     : 2018:12:03 22:29:07+01:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Image Width                     : 3984
Image Height                    : 5312
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:2 (2 1)
Image Size                      : 3984x5312
Megapixels                      : 21.2
```
