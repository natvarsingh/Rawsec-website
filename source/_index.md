# Rawsec's blog

Welcome to the blog of [Rawsec](about/rawsec/).

![](rawsec_logo_dark_355x286.svg)

## Team

[Rawsec](about/rawsec/) is a International CTF team. You can look for more information about the [team](about/rawsec/#team), find our [write-ups](categories/security/writeups/) or discover what is a [CTF](about/rawsec/#ctf).

## Blog

[Rawsec](about/rawsec/) is also a blog talking about [Linux](tags/linux/) and [security](tags/security/). You can also look for more information about the [blog](about/rawsec/#blog) or just [browse it](archives/).

## CyberSecurity Inventory

[Rawsec's CyberSecurity Inventory](https://inventory.raw.pm/) is an inventory of tools and resources about CyberSecurity that aims to help people to find everything related to CyberSecurity.

## noraj

[noraj](about/noraj/): it's me. I'm the founder of the team, the author of the blog and the maintainer of the inventory. I invite you to know more [about me](about/noraj/).
